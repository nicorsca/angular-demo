export interface ChartLegendInterface {
  marker: string;
  value?: string;
  title: string;
}
