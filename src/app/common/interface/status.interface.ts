export interface StatusInterface {
  label: string;
  color: string;
}
