export const FontAwesomeIconList = [
  'fas fa-address-book',
  'fas fa-address-card',
  'fas fa-adjust',
  'fas fa-alarm-clock',
  'fas fa-align-center',
  'fas fa-align-justify',
  'fas fa-align-left',
  'fas fa-align-right',
  'fas fa-allergies',
  'fas fa-ambulance',
  'fas fa-american-sign-language-interpreting',
  'fas fa-anchor',
  'fas fa-angle-double-down',
  'fas fa-angle-double-left',
  'fas fa-angle-double-right',
  'fas fa-angle-double-up',
  'fas fa-angle-down',
  'fas fa-angle-left',
  'fas fa-angle-right',
  'fas fa-angle-up',
  'fas fa-archive',
  'fas fa-arrow-alt-circle-down',
  'fas fa-arrow-alt-circle-left',
  'fas fa-arrow-alt-circle-right',
  'fas fa-arrow-alt-circle-up',
  'fas fa-arrow-alt-down',
  'fas fa-arrow-alt-from-bottom',
  'fas fa-arrow-alt-from-left',
  'fas fa-arrow-alt-from-right',
  'fas fa-arrow-alt-from-top',
  'fas fa-arrow-alt-left',
  'fas fa-arrow-alt-right',
  'fas fa-arrow-alt-square-down',
  'fas fa-arrow-alt-square-left',
  'fas fa-arrow-alt-square-right',
  'fas fa-arrow-alt-square-up',
  'fas fa-arrow-alt-to-bottom',
  'fas fa-arrow-alt-to-left',
  'fas fa-arrow-alt-to-right',
  'fas fa-arrow-alt-to-top',
  'fas fa-arrow-alt-up',
  'fas fa-arrow-circle-down',
  'fas fa-arrow-circle-left',
  'fas fa-arrow-circle-right',
  'fas fa-arrow-circle-up',
  'fas fa-arrow-down',
  'fas fa-arrow-from-bottom',
  'fas fa-arrow-from-left',
  'fas fa-arrow-from-right',
  'fas fa-arrow-from-top',
  'fas fa-arrow-left',
  'fas fa-arrow-right',
  'fas fa-arrow-square-down',
  'fas fa-arrow-square-left',
  'fas fa-arrow-square-right',
  'fas fa-arrow-square-up',
  'fas fa-arrow-to-bottom',
  'fas fa-arrow-to-left',
  'fas fa-arrow-to-right',
  'fas fa-arrow-to-top',
  'fas fa-arrow-up',
  'fas fa-arrows',
  'fas fa-arrows-alt',
  'fas fa-arrows-alt-h',
  'fas fa-arrows-alt-v',
  'fas fa-arrows-h',
  'fas fa-arrows-v',
  'fas fa-assistive-listening-systems',
  'fas fa-asterisk',
  'fas fa-at',
  'fas fa-audio-description',
  'fas fa-backward',
  'fas fa-badge',
  'fas fa-badge-check',
  'fas fa-balance-scale',
  'fas fa-ban',
  'fas fa-band-aid',
  'fas fa-barcode',
  'fas fa-barcode-alt',
  'fas fa-barcode-read',
  'fas fa-barcode-scan',
  'fas fa-bars',
  'fas fa-baseball',
  'fas fa-baseball-ball',
  'fas fa-basketball-ball',
  'fas fa-basketball-hoop',
  'fas fa-bath',
  'fas fa-battery-bolt',
  'fas fa-battery-empty',
  'fas fa-battery-full',
  'fas fa-battery-half',
  'fas fa-battery-quarter',
  'fas fa-battery-slash',
  'fas fa-battery-three-quarters',
  'fas fa-bed',
  'fas fa-beer',
  'fas fa-bell',
  'fas fa-bell-slash',
  'fas fa-bicycle',
  'fas fa-binoculars',
  'fas fa-birthday-cake',
  'fas fa-blanket',
  'fas fa-blind',
  'fas fa-bold',
  'fas fa-bolt',
  'fas fa-bomb',
  'fas fa-book',
  'fas fa-book-heart',
  'fas fa-bookmark',
  'fas fa-bowling-ball',
  'fas fa-bowling-pins',
  'fas fa-box',
  'fas fa-box-alt',
  'fas fa-box-check',
  'fas fa-box-fragile',
  'fas fa-box-full',
  'fas fa-box-heart',
  'fas fa-box-open',
  'fas fa-box-up',
  'fas fa-box-usd',
  'fas fa-boxes',
  'fas fa-boxes-alt',
  'fas fa-boxing-glove',
  'fas fa-braille',
  'fas fa-briefcase',
  'fas fa-briefcase-medical',
  'fas fa-browser',
  'fas fa-bug',
  'fas fa-building',
  'fas fa-bullhorn',
  'fas fa-bullseye',
  'fas fa-burn',
  'fas fa-bus',
  'fas fa-calculator',
  'fas fa-calendar',
  'fas fa-calendar-alt',
  'fas fa-calendar-check',
  'fas fa-calendar-edit',
  'fas fa-calendar-exclamation',
  'fas fa-calendar-minus',
  'fas fa-calendar-plus',
  'fas fa-calendar-times',
  'fas fa-camera',
  'fas fa-camera-alt',
  'fas fa-camera-retro',
  'fas fa-capsules',
  'fas fa-car',
  'fas fa-caret-circle-down',
  'fas fa-caret-circle-left',
  'fas fa-caret-circle-right',
  'fas fa-caret-circle-up',
  'fas fa-caret-down',
  'fas fa-caret-left',
  'fas fa-caret-right',
  'fas fa-caret-square-down',
  'fas fa-caret-square-left',
  'fas fa-caret-square-right',
  'fas fa-caret-square-up',
  'fas fa-caret-up',
  'fas fa-cart-arrow-down',
  'fas fa-cart-plus',
  'fas fa-certificate',
  'fas fa-chart-area',
  'fas fa-chart-bar',
  'fas fa-chart-line',
  'fas fa-chart-pie',
  'fas fa-check',
  'fas fa-check-circle',
  'fas fa-check-square',
  'fas fa-chess',
  'fas fa-chess-bishop',
  'fas fa-chess-bishop-alt',
  'fas fa-chess-board',
  'fas fa-chess-clock',
  'fas fa-chess-clock-alt',
  'fas fa-chess-king',
  'fas fa-chess-king-alt',
  'fas fa-chess-knight',
  'fas fa-chess-knight-alt',
  'fas fa-chess-pawn',
  'fas fa-chess-pawn-alt',
  'fas fa-chess-queen',
  'fas fa-chess-queen-alt',
  'fas fa-chess-rook',
  'fas fa-chess-rook-alt',
  'fas fa-chevron-circle-down',
  'fas fa-chevron-circle-left',
  'fas fa-chevron-circle-right',
  'fas fa-chevron-circle-up',
  'fas fa-chevron-double-down',
  'fas fa-chevron-double-left',
  'fas fa-chevron-double-right',
  'fas fa-chevron-double-up',
  'fas fa-chevron-down',
  'fas fa-chevron-left',
  'fas fa-chevron-right',
  'fas fa-chevron-square-down',
  'fas fa-chevron-square-left',
  'fas fa-chevron-square-right',
  'fas fa-chevron-square-up',
  'fas fa-chevron-up',
  'fas fa-child',
  'fas fa-circle',
  'fas fa-circle-notch',
  'fas fa-clipboard',
  'fas fa-clipboard-check',
  'fas fa-clipboard-list',
  'fas fa-clock',
  'fas fa-clone',
  'fas fa-closed-captioning',
  'fas fa-cloud',
  'fas fa-cloud-download',
  'fas fa-cloud-download-alt',
  'fas fa-cloud-upload',
  'fas fa-cloud-upload-alt',
  'fas fa-club',
  'fas fa-code',
  'fas fa-code-branch',
  'fas fa-code-commit',
  'fas fa-code-merge',
  'fas fa-coffee',
  'fas fa-cog',
  'fas fa-cogs',
  'fas fa-columns',
  'fas fa-comment',
  'fas fa-comment-alt',
  'fas fa-comment-alt-check',
  'fas fa-comment-alt-dots',
  'fas fa-comment-alt-edit',
  'fas fa-comment-alt-exclamation',
  'fas fa-comment-alt-lines',
  'fas fa-comment-alt-minus',
  'fas fa-comment-alt-plus',
  'fas fa-comment-alt-slash',
  'fas fa-comment-alt-smile',
  'fas fa-comment-alt-times',
  'fas fa-comment-check',
  'fas fa-comment-dots',
  'fas fa-comment-edit',
  'fas fa-comment-exclamation',
  'fas fa-comment-lines',
  'fas fa-comment-minus',
  'fas fa-comment-plus',
  'fas fa-comment-slash',
  'fas fa-comment-smile',
  'fas fa-comment-times',
  'fas fa-comments',
  'fas fa-comments-alt',
  'fas fa-compass',
  'fas fa-compress',
  'fas fa-compress-alt',
  'fas fa-compress-wide',
  'fas fa-container-storage',
  'fas fa-conveyor-belt',
  'fas fa-conveyor-belt-alt',
  'fas fa-copy',
  'fas fa-copyright',
  'fas fa-couch',
  'fas fa-credit-card',
  'fas fa-credit-card-blank',
  'fas fa-credit-card-front',
  'fas fa-cricket',
  'fas fa-crop',
  'fas fa-crosshairs',
  'fas fa-cube',
  'fas fa-cubes',
  'fas fa-curling',
  'fas fa-cut',
  'fas fa-database',
  'fas fa-deaf',
  'fas fa-desktop',
  'fas fa-desktop-alt',
  'fas fa-diagnoses',
  'fas fa-diamond',
  'fas fa-dna',
  'fas fa-dollar-sign',
  'fas fa-dolly',
  'fas fa-dolly-empty',
  'fas fa-dolly-flatbed',
  'fas fa-dolly-flatbed-alt',
  'fas fa-dolly-flatbed-empty',
  'fas fa-donate',
  'fas fa-dot-circle',
  'fas fa-dove',
  'fas fa-download',
  'fas fa-dumbbell',
  'fas fa-edit',
  'fas fa-eject',
  'fas fa-ellipsis-h',
  'fas fa-ellipsis-h-alt',
  'fas fa-ellipsis-v',
  'fas fa-ellipsis-v-alt',
  'fas fa-envelope',
  'fas fa-envelope-open',
  'fas fa-envelope-square',
  'fas fa-eraser',
  'fas fa-euro-sign',
  'fas fa-exchange',
  'fas fa-exchange-alt',
  'fas fa-exclamation',
  'fas fa-exclamation-circle',
  'fas fa-exclamation-square',
  'fas fa-exclamation-triangle',
  'fas fa-expand',
  'fas fa-expand-alt',
  'fas fa-expand-arrows',
  'fas fa-expand-arrows-alt',
  'fas fa-expand-wide',
  'fas fa-external-link',
  'fas fa-external-link-alt',
  'fas fa-external-link-square',
  'fas fa-external-link-square-alt',
  'fas fa-eye',
  'fas fa-eye-dropper',
  'fas fa-eye-slash',
  'fas fa-fast-backward',
  'fas fa-fast-forward',
  'fas fa-fax',
  'fas fa-female',
  'fas fa-field-hockey',
  'fas fa-fighter-jet',
  'fas fa-file',
  'fas fa-file-alt',
  'fas fa-file-archive',
  'fas fa-file-audio',
  'fas fa-file-check',
  'fas fa-file-code',
  'fas fa-file-edit',
  'fas fa-file-excel',
  'fas fa-file-exclamation',
  'fas fa-file-image',
  'fas fa-file-medical',
  'fas fa-file-medical-alt',
  'fas fa-file-minus',
  'fas fa-file-pdf',
  'fas fa-file-plus',
  'fas fa-file-powerpoint',
  'fas fa-file-times',
  'fas fa-file-video',
  'fas fa-file-word',
  'fas fa-film',
  'fas fa-film-alt',
  'fas fa-filter',
  'fas fa-fire',
  'fas fa-fire-extinguisher',
  'fas fa-first-aid',
  'fas fa-flag',
  'fas fa-flag-checkered',
  'fas fa-flask',
  'fas fa-folder',
  'fas fa-folder-open',
  'fas fa-font',
  'fas fa-football-ball',
  'fas fa-football-helmet',
  'fas fa-forklift',
  'fas fa-forward',
  'fas fa-fragile',
  'fas fa-frown',
  'fas fa-futbol',
  'fas fa-gamepad',
  'fas fa-gavel',
  'fas fa-gem',
  'fas fa-genderless',
  'fas fa-gift',
  'fas fa-glass-martini',
  'fas fa-globe',
  'fas fa-golf-ball',
  'fas fa-golf-club',
  'fas fa-graduation-cap',
  'fas fa-h-square',
  'fas fa-h1',
  'fas fa-h2',
  'fas fa-h3',
  'fas fa-hand-heart',
  'fas fa-hand-holding',
  'fas fa-hand-holding-box',
  'fas fa-hand-holding-heart',
  'fas fa-hand-holding-seedling',
  'fas fa-hand-holding-usd',
  'fas fa-hand-holding-water',
  'fas fa-hand-lizard',
  'fas fa-hand-paper',
  'fas fa-hand-peace',
  'fas fa-hand-point-down',
  'fas fa-hand-point-left',
  'fas fa-hand-point-right',
  'fas fa-hand-point-up',
  'fas fa-hand-pointer',
  'fas fa-hand-receiving',
  'fas fa-hand-rock',
  'fas fa-hand-scissors',
  'fas fa-hand-spock',
  'fas fa-hands',
  'fas fa-hands-heart',
  'fas fa-hands-helping',
  'fas fa-hands-usd',
  'fas fa-handshake',
  'fas fa-handshake-alt',
  'fas fa-hashtag',
  'fas fa-hdd',
  'fas fa-heading',
  'fas fa-headphones',
  'fas fa-heart',
  'fas fa-heart-circle',
  'fas fa-heart-square',
  'fas fa-heartbeat',
  'fas fa-hexagon',
  'fas fa-attendance-history',
  'fas fa-hockey-puck',
  'fas fa-hockey-sticks',
  'fas fa-home',
  'fas fa-home-heart',
  'fas fa-hospital',
  'fas fa-hospital-alt',
  'fas fa-hospital-symbol',
  'fas fa-hourglass',
  'fas fa-hourglass-end',
  'fas fa-hourglass-half',
  'fas fa-hourglass-start',
  'fas fa-i-cursor',
  'fas fa-id-badge',
  'fas fa-id-card',
  'fas fa-id-card-alt',
  'fas fa-image',
  'fas fa-images',
  'fas fa-inbox',
  'fas fa-inbox-in',
  'fas fa-inbox-out',
  'fas fa-indent',
  'fas fa-industry',
  'fas fa-industry-alt',
  'fas fa-info',
  'fas fa-info-circle',
  'fas fa-info-square',
  'fas fa-inventory',
  'fas fa-italic',
  'fas fa-jack-o-lantern',
  'fas fa-key',
  'fas fa-keyboard',
  'fas fa-lamp',
  'fas fa-language',
  'fas fa-laptop',
  'fas fa-leaf',
  'fas fa-leaf-heart',
  'fas fa-lemon',
  'fas fa-level-down',
  'fas fa-level-down-alt',
  'fas fa-level-up',
  'fas fa-level-up-alt',
  'fas fa-life-ring',
  'fas fa-lightbulb',
  'fas fa-link',
  'fas fa-lira-sign',
  'fas fa-list',
  'fas fa-list-alt',
  'fas fa-list-ol',
  'fas fa-list-ul',
  'fas fa-location-arrow',
  'fas fa-lock',
  'fas fa-lock-alt',
  'fas fa-lock-open',
  'fas fa-lock-open-alt',
  'fas fa-long-arrow-alt-down',
  'fas fa-long-arrow-alt-left',
  'fas fa-long-arrow-alt-right',
  'fas fa-long-arrow-alt-up',
  'fas fa-long-arrow-down',
  'fas fa-long-arrow-left',
  'fas fa-long-arrow-right',
  'fas fa-long-arrow-up',
  'fas fa-loveseat',
  'fas fa-low-vision',
  'fas fa-luchador',
  'fas fa-magic',
  'fas fa-magnet',
  'fas fa-male',
  'fas fa-map',
  'fas fa-map-marker',
  'fas fa-map-marker-alt',
  'fas fa-map-pin',
  'fas fa-map-signs',
  'fas fa-mars',
  'fas fa-mars-double',
  'fas fa-mars-stroke',
  'fas fa-mars-stroke-h',
  'fas fa-mars-stroke-v',
  'fas fa-medkit',
  'fas fa-meh',
  'fas fa-mercury',
  'fas fa-microchip',
  'fas fa-microphone',
  'fas fa-microphone-alt',
  'fas fa-microphone-slash',
  'fas fa-minus',
  'fas fa-minus-circle',
  'fas fa-minus-hexagon',
  'fas fa-minus-octagon',
  'fas fa-minus-square',
  'fas fa-mobile',
  'fas fa-mobile-alt',
  'fas fa-mobile-android',
  'fas fa-mobile-android-alt',
  'fas fa-money-bill',
  'fas fa-money-bill-alt',
  'fas fa-moon',
  'fas fa-motorcycle',
  'fas fa-mouse-pointer',
  'fas fa-music',
  'fas fa-neuter',
  'fas fa-newspaper',
  'fas fa-notes-medical',
  'fas fa-object-group',
  'fas fa-object-ungroup',
  'fas fa-octagon',
  'fas fa-outdent',
  'fas fa-paint-brush',
  'fas fa-pallet',
  'fas fa-pallet-alt',
  'fas fa-paper-plane',
  'fas fa-paperclip',
  'fas fa-parachute-box',
  'fas fa-paragraph',
  'fas fa-paste',
  'fas fa-pause',
  'fas fa-pause-circle',
  'fas fa-paw',
  'fas fa-pen',
  'fas fa-pen-alt',
  'fas fa-pen-square',
  'fas fa-pencil',
  'fas fa-pencil-alt',
  'fas fa-pennant',
  'fas fa-people-carry',
  'fas fa-percent',
  'fas fa-person-carry',
  'fas fa-person-dolly',
  'fas fa-person-dolly-empty',
  'fas fa-phone',
  'fas fa-phone-plus',
  'fas fa-phone-slash',
  'fas fa-phone-square',
  'fas fa-phone-volume',
  'fas fa-piggy-bank',
  'fas fa-pills',
  'fas fa-plane',
  'fas fa-plane-alt',
  'fas fa-play',
  'fas fa-play-circle',
  'fas fa-plug',
  'fas fa-plus',
  'fas fa-plus-circle',
  'fas fa-plus-hexagon',
  'fas fa-plus-octagon',
  'fas fa-plus-square',
  'fas fa-podcast',
  'fas fa-poo',
  'fas fa-portrait',
  'fas fa-pound-sign',
  'fas fa-power-off',
  'fas fa-prescription-bottle',
  'fas fa-prescription-bottle-alt',
  'fas fa-print',
  'fas fa-procedures',
  'fas fa-puzzle-piece',
  'fas fa-qrcode',
  'fas fa-question',
  'fas fa-question-circle',
  'fas fa-question-square',
  'fas fa-quidditch',
  'fas fa-quote-left',
  'fas fa-quote-right',
  'fas fa-racquet',
  'fas fa-ramp-loading',
  'fas fa-random',
  'fas fa-rectangle-landscape',
  'fas fa-rectangle-portrait',
  'fas fa-rectangle-wide',
  'fas fa-recycle',
  'fas fa-redo',
  'fas fa-redo-alt',
  'fas fa-registered',
  'fas fa-repeat',
  'fas fa-repeat-1',
  'fas fa-repeat-1-alt',
  'fas fa-repeat-alt',
  'fas fa-reply',
  'fas fa-reply-all',
  'fas fa-retweet',
  'fas fa-retweet-alt',
  'fas fa-ribbon',
  'fas fa-road',
  'fas fa-rocket',
  'fas fa-route',
  'fas fa-rss',
  'fas fa-rss-square',
  'fas fa-ruble-sign',
  'fas fa-rupee-sign',
  'fas fa-save',
  'fas fa-scanner',
  'fas fa-scanner-keyboard',
  'fas fa-scanner-touchscreen',
  'fas fa-scrubber',
  'fas fa-search',
  'fas fa-search-minus',
  'fas fa-search-plus',
  'fas fa-seedling',
  'fas fa-server',
  'fas fa-share',
  'fas fa-share-all',
  'fas fa-share-alt',
  'fas fa-share-alt-square',
  'fas fa-share-square',
  'fas fa-shekel-sign',
  'fas fa-shield',
  'fas fa-shield-alt',
  'fas fa-shield-check',
  'fas fa-ship',
  'fas fa-shipping-fast',
  'fas fa-shipping-timed',
  'fas fa-shopping-bag',
  'fas fa-shopping-basket',
  'fas fa-shopping-cart',
  'fas fa-shower',
  'fas fa-shuttlecock',
  'fas fa-sign',
  'fas fa-sign-in',
  'fas fa-sign-in-alt',
  'fas fa-sign-language',
  'fas fa-sign-out',
  'fas fa-sign-out-alt',
  'fas fa-signal',
  'fas fa-sitemap',
  'fas fa-sliders-h',
  'fas fa-sliders-h-square',
  'fas fa-sliders-v',
  'fas fa-sliders-v-square',
  'fas fa-smile',
  'fas fa-smile-plus',
  'fas fa-smoking',
  'fas fa-snowflake',
  'fas fa-sort',
  'fas fa-sort-alpha-down',
  'fas fa-sort-alpha-up',
  'fas fa-sort-amount-down',
  'fas fa-sort-amount-up',
  'fas fa-sort-down',
  'fas fa-sort-numeric-down',
  'fas fa-sort-numeric-up',
  'fas fa-sort-up',
  'fas fa-space-shuttle',
  'fas fa-spade',
  'fas fa-spinner',
  'fas fa-spinner-third',
  'fas fa-square',
  'fas fa-square-full',
  'fas fa-star',
  'fas fa-star-exclamation',
  'fas fa-star-half',
  'fas fa-step-backward',
  'fas fa-step-forward',
  'fas fa-stethoscope',
  'fas fa-sticky-note',
  'fas fa-stop',
  'fas fa-stop-circle',
  'fas fa-stopwatch',
  'fas fa-street-view',
  'fas fa-strikethrough',
  'fas fa-subscript',
  'fas fa-subway',
  'fas fa-suitcase',
  'fas fa-sun',
  'fas fa-superscript',
  'fas fa-sync',
  'fas fa-sync-alt',
  'fas fa-syringe',
  'fas fa-table',
  'fas fa-table-tennis',
  'fas fa-tablet',
  'fas fa-tablet-alt',
  'fas fa-tablet-android',
  'fas fa-tablet-android-alt',
  'fas fa-tablet-rugged',
  'fas fa-tablets',
  'fas fa-tachometer',
  'fas fa-tachometer-alt',
  'fas fa-tag',
  'fas fa-tags',
  'fas fa-tape',
  'fas fa-tasks',
  'fas fa-taxi',
  'fas fa-tennis-ball',
  'fas fa-terminal',
  'fas fa-text-height',
  'fas fa-text-width',
  'fas fa-th',
  'fas fa-th-large',
  'fas fa-th-list',
  'fas fa-thermometer',
  'fas fa-thermometer-empty',
  'fas fa-thermometer-full',
  'fas fa-thermometer-half',
  'fas fa-thermometer-quarter',
  'fas fa-thermometer-three-quarters',
  'fas fa-thumbs-down',
  'fas fa-thumbs-up',
  'fas fa-thumbtack',
  'fas fa-ticket',
  'fas fa-ticket-alt',
  'fas fa-times',
  'fas fa-times-circle',
  'fas fa-times-hexagon',
  'fas fa-times-octagon',
  'fas fa-times-square',
  'fas fa-tint',
  'fas fa-toggle-off',
  'fas fa-toggle-on',
  'fas fa-trademark',
  'fas fa-train',
  'fas fa-transgender',
  'fas fa-transgender-alt',
  'fas fa-trash',
  'fas fa-trash-alt',
  'fas fa-tree',
  'fas fa-tree-alt',
  'fas fa-triangle',
  'fas fa-trophy',
  'fas fa-trophy-alt',
  'fas fa-truck',
  'fas fa-truck-container',
  'fas fa-truck-couch',
  'fas fa-truck-loading',
  'fas fa-truck-moving',
  'fas fa-truck-ramp',
  'fas fa-tty',
  'fas fa-tv',
  'fas fa-tv-retro',
  'fas fa-umbrella',
  'fas fa-underline',
  'fas fa-undo',
  'fas fa-undo-alt',
  'fas fa-universal-access',
  'fas fa-university',
  'fas fa-unlink',
  'fas fa-unlock',
  'fas fa-unlock-alt',
  'fas fa-upload',
  'fas fa-usd-circle',
  'fas fa-usd-square',
  'fas fa-user',
  'fas fa-user-alt',
  'fas fa-user-circle',
  'fas fa-user-md',
  'fas fa-user-plus',
  'fas fa-user-secret',
  'fas fa-user-times',
  'fas fa-users',
  'fas fa-utensil-fork',
  'fas fa-utensil-knife',
  'fas fa-utensil-spoon',
  'fas fa-utensils',
  'fas fa-utensils-alt',
  'fas fa-venus',
  'fas fa-venus-double',
  'fas fa-venus-mars',
  'fas fa-vial',
  'fas fa-vials',
  'fas fa-video',
  'fas fa-video-plus',
  'fas fa-video-slash',
  'fas fa-volleyball-ball',
  'fas fa-volume-down',
  'fas fa-volume-mute',
  'fas fa-volume-off',
  'fas fa-volume-up',
  'fas fa-warehouse',
  'fas fa-warehouse-alt',
  'fas fa-watch',
  'fas fa-weight',
  'fas fa-wheelchair',
  'fas fa-whistle',
  'fas fa-wifi',
  'fas fa-window',
  'fas fa-window-alt',
  'fas fa-window-close',
  'fas fa-window-maximize',
  'fas fa-window-minimize',
  'fas fa-window-restore',
  'fas fa-wine-glass',
  'fas fa-won-sign',
  'fas fa-wrench',
  'fas fa-x-ray',
  'fas fa-yen-sign',
  'far fa-address-book',
  'far fa-address-card',
  'far fa-adjust',
  'far fa-alarm-clock',
  'far fa-align-center',
  'far fa-align-justify',
  'far fa-align-left',
  'far fa-align-right',
  'far fa-allergies',
  'far fa-ambulance',
  'far fa-american-sign-language-interpreting',
  'far fa-anchor',
  'far fa-angle-double-down',
  'far fa-angle-double-left',
  'far fa-angle-double-right',
  'far fa-angle-double-up',
  'far fa-angle-down',
  'far fa-angle-left',
  'far fa-angle-right',
  'far fa-angle-up',
  'far fa-archive',
  'far fa-arrow-alt-circle-down',
  'far fa-arrow-alt-circle-left',
  'far fa-arrow-alt-circle-right',
  'far fa-arrow-alt-circle-up',
  'far fa-arrow-alt-down',
  'far fa-arrow-alt-from-bottom',
  'far fa-arrow-alt-from-left',
  'far fa-arrow-alt-from-right',
  'far fa-arrow-alt-from-top',
  'far fa-arrow-alt-left',
  'far fa-arrow-alt-right',
  'far fa-arrow-alt-square-down',
  'far fa-arrow-alt-square-left',
  'far fa-arrow-alt-square-right',
  'far fa-arrow-alt-square-up',
  'far fa-arrow-alt-to-bottom',
  'far fa-arrow-alt-to-left',
  'far fa-arrow-alt-to-right',
  'far fa-arrow-alt-to-top',
  'far fa-arrow-alt-up',
  'far fa-arrow-circle-down',
  'far fa-arrow-circle-left',
  'far fa-arrow-circle-right',
  'far fa-arrow-circle-up',
  'far fa-arrow-down',
  'far fa-arrow-from-bottom',
  'far fa-arrow-from-left',
  'far fa-arrow-from-right',
  'far fa-arrow-from-top',
  'far fa-arrow-left',
  'far fa-arrow-right',
  'far fa-arrow-square-down',
  'far fa-arrow-square-left',
  'far fa-arrow-square-right',
  'far fa-arrow-square-up',
  'far fa-arrow-to-bottom',
  'far fa-arrow-to-left',
  'far fa-arrow-to-right',
  'far fa-arrow-to-top',
  'far fa-arrow-up',
  'far fa-arrows',
  'far fa-arrows-alt',
  'far fa-arrows-alt-h',
  'far fa-arrows-alt-v',
  'far fa-arrows-h',
  'far fa-arrows-v',
  'far fa-assistive-listening-systems',
  'far fa-asterisk',
  'far fa-at',
  'far fa-audio-description',
  'far fa-backward',
  'far fa-badge',
  'far fa-badge-check',
  'far fa-balance-scale',
  'far fa-ban',
  'far fa-band-aid',
  'far fa-barcode',
  'far fa-barcode-alt',
  'far fa-barcode-read',
  'far fa-barcode-scan',
  'far fa-bars',
  'far fa-baseball',
  'far fa-baseball-ball',
  'far fa-basketball-ball',
  'far fa-basketball-hoop',
  'far fa-bath',
  'far fa-battery-bolt',
  'far fa-battery-empty',
  'far fa-battery-full',
  'far fa-battery-half',
  'far fa-battery-quarter',
  'far fa-battery-slash',
  'far fa-battery-three-quarters',
  'far fa-bed',
  'far fa-beer',
  'far fa-bell',
  'far fa-bell-slash',
  'far fa-bicycle',
  'far fa-binoculars',
  'far fa-birthday-cake',
  'far fa-blanket',
  'far fa-blind',
  'far fa-bold',
  'far fa-bolt',
  'far fa-bomb',
  'far fa-book',
  'far fa-book-heart',
  'far fa-bookmark',
  'far fa-bowling-ball',
  'far fa-bowling-pins',
  'far fa-box',
  'far fa-box-alt',
  'far fa-box-check',
  'far fa-box-fragile',
  'far fa-box-full',
  'far fa-box-heart',
  'far fa-box-open',
  'far fa-box-up',
  'far fa-box-usd',
  'far fa-boxes',
  'far fa-boxes-alt',
  'far fa-boxing-glove',
  'far fa-braille',
  'far fa-briefcase',
  'far fa-briefcase-medical',
  'far fa-browser',
  'far fa-bug',
  'far fa-building',
  'far fa-bullhorn',
  'far fa-bullseye',
  'far fa-burn',
  'far fa-bus',
  'far fa-calculator',
  'far fa-calendar',
  'far fa-calendar-alt',
  'far fa-calendar-check',
  'far fa-calendar-edit',
  'far fa-calendar-exclamation',
  'far fa-calendar-minus',
  'far fa-calendar-plus',
  'far fa-calendar-times',
  'far fa-camera',
  'far fa-camera-alt',
  'far fa-camera-retro',
  'far fa-capsules',
  'far fa-car',
  'far fa-caret-circle-down',
  'far fa-caret-circle-left',
  'far fa-caret-circle-right',
  'far fa-caret-circle-up',
  'far fa-caret-down',
  'far fa-caret-left',
  'far fa-caret-right',
  'far fa-caret-square-down',
  'far fa-caret-square-left',
  'far fa-caret-square-right',
  'far fa-caret-square-up',
  'far fa-caret-up',
  'far fa-cart-arrow-down',
  'far fa-cart-plus',
  'far fa-certificate',
  'far fa-chart-area',
  'far fa-chart-bar',
  'far fa-chart-line',
  'far fa-chart-pie',
  'far fa-check',
  'far fa-check-circle',
  'far fa-check-square',
  'far fa-chess',
  'far fa-chess-bishop',
  'far fa-chess-bishop-alt',
  'far fa-chess-board',
  'far fa-chess-clock',
  'far fa-chess-clock-alt',
  'far fa-chess-king',
  'far fa-chess-king-alt',
  'far fa-chess-knight',
  'far fa-chess-knight-alt',
  'far fa-chess-pawn',
  'far fa-chess-pawn-alt',
  'far fa-chess-queen',
  'far fa-chess-queen-alt',
  'far fa-chess-rook',
  'far fa-chess-rook-alt',
  'far fa-chevron-circle-down',
  'far fa-chevron-circle-left',
  'far fa-chevron-circle-right',
  'far fa-chevron-circle-up',
  'far fa-chevron-double-down',
  'far fa-chevron-double-left',
  'far fa-chevron-double-right',
  'far fa-chevron-double-up',
  'far fa-chevron-down',
  'far fa-chevron-left',
  'far fa-chevron-right',
  'far fa-chevron-square-down',
  'far fa-chevron-square-left',
  'far fa-chevron-square-right',
  'far fa-chevron-square-up',
  'far fa-chevron-up',
  'far fa-child',
  'far fa-circle',
  'far fa-circle-notch',
  'far fa-clipboard',
  'far fa-clipboard-check',
  'far fa-clipboard-list',
  'far fa-clock',
  'far fa-clone',
  'far fa-closed-captioning',
  'far fa-cloud',
  'far fa-cloud-download',
  'far fa-cloud-download-alt',
  'far fa-cloud-upload',
  'far fa-cloud-upload-alt',
  'far fa-club',
  'far fa-code',
  'far fa-code-branch',
  'far fa-code-commit',
  'far fa-code-merge',
  'far fa-coffee',
  'far fa-cog',
  'far fa-cogs',
  'far fa-columns',
  'far fa-comment',
  'far fa-comment-alt',
  'far fa-comment-alt-check',
  'far fa-comment-alt-dots',
  'far fa-comment-alt-edit',
  'far fa-comment-alt-exclamation',
  'far fa-comment-alt-lines',
  'far fa-comment-alt-minus',
  'far fa-comment-alt-plus',
  'far fa-comment-alt-slash',
  'far fa-comment-alt-smile',
  'far fa-comment-alt-times',
  'far fa-comment-check',
  'far fa-comment-dots',
  'far fa-comment-edit',
  'far fa-comment-exclamation',
  'far fa-comment-lines',
  'far fa-comment-minus',
  'far fa-comment-plus',
  'far fa-comment-slash',
  'far fa-comment-smile',
  'far fa-comment-times',
  'far fa-comments',
  'far fa-comments-alt',
  'far fa-compass',
  'far fa-compress',
  'far fa-compress-alt',
  'far fa-compress-wide',
  'far fa-container-storage',
  'far fa-conveyor-belt',
  'far fa-conveyor-belt-alt',
  'far fa-copy',
  'far fa-copyright',
  'far fa-couch',
  'far fa-credit-card',
  'far fa-credit-card-blank',
  'far fa-credit-card-front',
  'far fa-cricket',
  'far fa-crop',
  'far fa-crosshairs',
  'far fa-cube',
  'far fa-cubes',
  'far fa-curling',
  'far fa-cut',
  'far fa-database',
  'far fa-deaf',
  'far fa-desktop',
  'far fa-desktop-alt',
  'far fa-diagnoses',
  'far fa-diamond',
  'far fa-dna',
  'far fa-dollar-sign',
  'far fa-dolly',
  'far fa-dolly-empty',
  'far fa-dolly-flatbed',
  'far fa-dolly-flatbed-alt',
  'far fa-dolly-flatbed-empty',
  'far fa-donate',
  'far fa-dot-circle',
  'far fa-dove',
  'far fa-download',
  'far fa-dumbbell',
  'far fa-edit',
  'far fa-eject',
  'far fa-ellipsis-h',
  'far fa-ellipsis-h-alt',
  'far fa-ellipsis-v',
  'far fa-ellipsis-v-alt',
  'far fa-envelope',
  'far fa-envelope-open',
  'far fa-envelope-square',
  'far fa-eraser',
  'far fa-euro-sign',
  'far fa-exchange',
  'far fa-exchange-alt',
  'far fa-exclamation',
  'far fa-exclamation-circle',
  'far fa-exclamation-square',
  'far fa-exclamation-triangle',
  'far fa-expand',
  'far fa-expand-alt',
  'far fa-expand-arrows',
  'far fa-expand-arrows-alt',
  'far fa-expand-wide',
  'far fa-external-link',
  'far fa-external-link-alt',
  'far fa-external-link-square',
  'far fa-external-link-square-alt',
  'far fa-eye',
  'far fa-eye-dropper',
  'far fa-eye-slash',
  'far fa-fast-backward',
  'far fa-fast-forward',
  'far fa-fax',
  'far fa-female',
  'far fa-field-hockey',
  'far fa-fighter-jet',
  'far fa-file',
  'far fa-file-alt',
  'far fa-file-archive',
  'far fa-file-audio',
  'far fa-file-check',
  'far fa-file-code',
  'far fa-file-edit',
  'far fa-file-excel',
  'far fa-file-exclamation',
  'far fa-file-image',
  'far fa-file-medical',
  'far fa-file-medical-alt',
  'far fa-file-minus',
  'far fa-file-pdf',
  'far fa-file-plus',
  'far fa-file-powerpoint',
  'far fa-file-times',
  'far fa-file-video',
  'far fa-file-word',
  'far fa-film',
  'far fa-film-alt',
  'far fa-filter',
  'far fa-fire',
  'far fa-fire-extinguisher',
  'far fa-first-aid',
  'far fa-flag',
  'far fa-flag-checkered',
  'far fa-flask',
  'far fa-folder',
  'far fa-folder-open',
  'far fa-font',
  'far fa-football-ball',
  'far fa-football-helmet',
  'far fa-forklift',
  'far fa-forward',
  'far fa-fragile',
  'far fa-frown',
  'far fa-futbol',
  'far fa-gamepad',
  'far fa-gavel',
  'far fa-gem',
  'far fa-genderless',
  'far fa-gift',
  'far fa-glass-martini',
  'far fa-globe',
  'far fa-golf-ball',
  'far fa-golf-club',
  'far fa-graduation-cap',
  'far fa-h-square',
  'far fa-h1',
  'far fa-h2',
  'far fa-h3',
  'far fa-hand-heart',
  'far fa-hand-holding',
  'far fa-hand-holding-box',
  'far fa-hand-holding-heart',
  'far fa-hand-holding-seedling',
  'far fa-hand-holding-usd',
  'far fa-hand-holding-water',
  'far fa-hand-lizard',
  'far fa-hand-paper',
  'far fa-hand-peace',
  'far fa-hand-point-down',
  'far fa-hand-point-left',
  'far fa-hand-point-right',
  'far fa-hand-point-up',
  'far fa-hand-pointer',
  'far fa-hand-receiving',
  'far fa-hand-rock',
  'far fa-hand-scissors',
  'far fa-hand-spock',
  'far fa-hands',
  'far fa-hands-heart',
  'far fa-hands-helping',
  'far fa-hands-usd',
  'far fa-handshake',
  'far fa-handshake-alt',
  'far fa-hashtag',
  'far fa-hdd',
  'far fa-heading',
  'far fa-headphones',
  'far fa-heart',
  'far fa-heart-circle',
  'far fa-heart-square',
  'far fa-heartbeat',
  'far fa-hexagon',
  'far fa-attendance-history',
  'far fa-hockey-puck',
  'far fa-hockey-sticks',
  'far fa-home',
  'far fa-home-heart',
  'far fa-hospital',
  'far fa-hospital-alt',
  'far fa-hospital-symbol',
  'far fa-hourglass',
  'far fa-hourglass-end',
  'far fa-hourglass-half',
  'far fa-hourglass-start',
  'far fa-i-cursor',
  'far fa-id-badge',
  'far fa-id-card',
  'far fa-id-card-alt',
  'far fa-image',
  'far fa-images',
  'far fa-inbox',
  'far fa-inbox-in',
  'far fa-inbox-out',
  'far fa-indent',
  'far fa-industry',
  'far fa-industry-alt',
  'far fa-info',
  'far fa-info-circle',
  'far fa-info-square',
  'far fa-inventory',
  'far fa-italic',
  'far fa-jack-o-lantern',
  'far fa-key',
  'far fa-keyboard',
  'far fa-lamp',
  'far fa-language',
  'far fa-laptop',
  'far fa-leaf',
  'far fa-leaf-heart',
  'far fa-lemon',
  'far fa-level-down',
  'far fa-level-down-alt',
  'far fa-level-up',
  'far fa-level-up-alt',
  'far fa-life-ring',
  'far fa-lightbulb',
  'far fa-link',
  'far fa-lira-sign',
  'far fa-list',
  'far fa-list-alt',
  'far fa-list-ol',
  'far fa-list-ul',
  'far fa-location-arrow',
  'far fa-lock',
  'far fa-lock-alt',
  'far fa-lock-open',
  'far fa-lock-open-alt',
  'far fa-long-arrow-alt-down',
  'far fa-long-arrow-alt-left',
  'far fa-long-arrow-alt-right',
  'far fa-long-arrow-alt-up',
  'far fa-long-arrow-down',
  'far fa-long-arrow-left',
  'far fa-long-arrow-right',
  'far fa-long-arrow-up',
  'far fa-loveseat',
  'far fa-low-vision',
  'far fa-luchador',
  'far fa-magic',
  'far fa-magnet',
  'far fa-male',
  'far fa-map',
  'far fa-map-marker',
  'far fa-map-marker-alt',
  'far fa-map-pin',
  'far fa-map-signs',
  'far fa-mars',
  'far fa-mars-double',
  'far fa-mars-stroke',
  'far fa-mars-stroke-h',
  'far fa-mars-stroke-v',
  'far fa-medkit',
  'far fa-meh',
  'far fa-mercury',
  'far fa-microchip',
  'far fa-microphone',
  'far fa-microphone-alt',
  'far fa-microphone-slash',
  'far fa-minus',
  'far fa-minus-circle',
  'far fa-minus-hexagon',
  'far fa-minus-octagon',
  'far fa-minus-square',
  'far fa-mobile',
  'far fa-mobile-alt',
  'far fa-mobile-android',
  'far fa-mobile-android-alt',
  'far fa-money-bill',
  'far fa-money-bill-alt',
  'far fa-moon',
  'far fa-motorcycle',
  'far fa-mouse-pointer',
  'far fa-music',
  'far fa-neuter',
  'far fa-newspaper',
  'far fa-notes-medical',
  'far fa-object-group',
  'far fa-object-ungroup',
  'far fa-octagon',
  'far fa-outdent',
  'far fa-paint-brush',
  'far fa-pallet',
  'far fa-pallet-alt',
  'far fa-paper-plane',
  'far fa-paperclip',
  'far fa-parachute-box',
  'far fa-paragraph',
  'far fa-paste',
  'far fa-pause',
  'far fa-pause-circle',
  'far fa-paw',
  'far fa-pen',
  'far fa-pen-alt',
  'far fa-pen-square',
  'far fa-pencil',
  'far fa-pencil-alt',
  'far fa-pennant',
  'far fa-people-carry',
  'far fa-percent',
  'far fa-person-carry',
  'far fa-person-dolly',
  'far fa-person-dolly-empty',
  'far fa-phone',
  'far fa-phone-plus',
  'far fa-phone-slash',
  'far fa-phone-square',
  'far fa-phone-volume',
  'far fa-piggy-bank',
  'far fa-pills',
  'far fa-plane',
  'far fa-plane-alt',
  'far fa-play',
  'far fa-play-circle',
  'far fa-plug',
  'far fa-plus',
  'far fa-plus-circle',
  'far fa-plus-hexagon',
  'far fa-plus-octagon',
  'far fa-plus-square',
  'far fa-podcast',
  'far fa-poo',
  'far fa-portrait',
  'far fa-pound-sign',
  'far fa-power-off',
  'far fa-prescription-bottle',
  'far fa-prescription-bottle-alt',
  'far fa-print',
  'far fa-procedures',
  'far fa-puzzle-piece',
  'far fa-qrcode',
  'far fa-question',
  'far fa-question-circle',
  'far fa-question-square',
  'far fa-quidditch',
  'far fa-quote-left',
  'far fa-quote-right',
  'far fa-racquet',
  'far fa-ramp-loading',
  'far fa-random',
  'far fa-rectangle-landscape',
  'far fa-rectangle-portrait',
  'far fa-rectangle-wide',
  'far fa-recycle',
  'far fa-redo',
  'far fa-redo-alt',
  'far fa-registered',
  'far fa-repeat',
  'far fa-repeat-1',
  'far fa-repeat-1-alt',
  'far fa-repeat-alt',
  'far fa-reply',
  'far fa-reply-all',
  'far fa-retweet',
  'far fa-retweet-alt',
  'far fa-ribbon',
  'far fa-road',
  'far fa-rocket',
  'far fa-route',
  'far fa-rss',
  'far fa-rss-square',
  'far fa-ruble-sign',
  'far fa-rupee-sign',
  'far fa-save',
  'far fa-scanner',
  'far fa-scanner-keyboard',
  'far fa-scanner-touchscreen',
  'far fa-scrubber',
  'far fa-search',
  'far fa-search-minus',
  'far fa-search-plus',
  'far fa-seedling',
  'far fa-server',
  'far fa-share',
  'far fa-share-all',
  'far fa-share-alt',
  'far fa-share-alt-square',
  'far fa-share-square',
  'far fa-shekel-sign',
  'far fa-shield',
  'far fa-shield-alt',
  'far fa-shield-check',
  'far fa-ship',
  'far fa-shipping-fast',
  'far fa-shipping-timed',
  'far fa-shopping-bag',
  'far fa-shopping-basket',
  'far fa-shopping-cart',
  'far fa-shower',
  'far fa-shuttlecock',
  'far fa-sign',
  'far fa-sign-in',
  'far fa-sign-in-alt',
  'far fa-sign-language',
  'far fa-sign-out',
  'far fa-sign-out-alt',
  'far fa-signal',
  'far fa-sitemap',
  'far fa-sliders-h',
  'far fa-sliders-h-square',
  'far fa-sliders-v',
  'far fa-sliders-v-square',
  'far fa-smile',
  'far fa-smile-plus',
  'far fa-smoking',
  'far fa-snowflake',
  'far fa-sort',
  'far fa-sort-alpha-down',
  'far fa-sort-alpha-up',
  'far fa-sort-amount-down',
  'far fa-sort-amount-up',
  'far fa-sort-down',
  'far fa-sort-numeric-down',
  'far fa-sort-numeric-up',
  'far fa-sort-up',
  'far fa-space-shuttle',
  'far fa-spade',
  'far fa-spinner',
  'far fa-spinner-third',
  'far fa-square',
  'far fa-square-full',
  'far fa-star',
  'far fa-star-exclamation',
  'far fa-star-half',
  'far fa-step-backward',
  'far fa-step-forward',
  'far fa-stethoscope',
  'far fa-sticky-note',
  'far fa-stop',
  'far fa-stop-circle',
  'far fa-stopwatch',
  'far fa-street-view',
  'far fa-strikethrough',
  'far fa-subscript',
  'far fa-subway',
  'far fa-suitcase',
  'far fa-sun',
  'far fa-superscript',
  'far fa-sync',
  'far fa-sync-alt',
  'far fa-syringe',
  'far fa-table',
  'far fa-table-tennis',
  'far fa-tablet',
  'far fa-tablet-alt',
  'far fa-tablet-android',
  'far fa-tablet-android-alt',
  'far fa-tablet-rugged',
  'far fa-tablets',
  'far fa-tachometer',
  'far fa-tachometer-alt',
  'far fa-tag',
  'far fa-tags',
  'far fa-tape',
  'far fa-tasks',
  'far fa-taxi',
  'far fa-tennis-ball',
  'far fa-terminal',
  'far fa-text-height',
  'far fa-text-width',
  'far fa-th',
  'far fa-th-large',
  'far fa-th-list',
  'far fa-thermometer',
  'far fa-thermometer-empty',
  'far fa-thermometer-full',
  'far fa-thermometer-half',
  'far fa-thermometer-quarter',
  'far fa-thermometer-three-quarters',
  'far fa-thumbs-down',
  'far fa-thumbs-up',
  'far fa-thumbtack',
  'far fa-ticket',
  'far fa-ticket-alt',
  'far fa-times',
  'far fa-times-circle',
  'far fa-times-hexagon',
  'far fa-times-octagon',
  'far fa-times-square',
  'far fa-tint',
  'far fa-toggle-off',
  'far fa-toggle-on',
  'far fa-trademark',
  'far fa-train',
  'far fa-transgender',
  'far fa-transgender-alt',
  'far fa-trash',
  'far fa-trash-alt',
  'far fa-tree',
  'far fa-tree-alt',
  'far fa-triangle',
  'far fa-trophy',
  'far fa-trophy-alt',
  'far fa-truck',
  'far fa-truck-container',
  'far fa-truck-couch',
  'far fa-truck-loading',
  'far fa-truck-moving',
  'far fa-truck-ramp',
  'far fa-tty',
  'far fa-tv',
  'far fa-tv-retro',
  'far fa-umbrella',
  'far fa-underline',
  'far fa-undo',
  'far fa-undo-alt',
  'far fa-universal-access',
  'far fa-university',
  'far fa-unlink',
  'far fa-unlock',
  'far fa-unlock-alt',
  'far fa-upload',
  'far fa-usd-circle',
  'far fa-usd-square',
  'far fa-user',
  'far fa-user-alt',
  'far fa-user-circle',
  'far fa-user-md',
  'far fa-user-plus',
  'far fa-user-secret',
  'far fa-user-times',
  'far fa-users',
  'far fa-utensil-fork',
  'far fa-utensil-knife',
  'far fa-utensil-spoon',
  'far fa-utensils',
  'far fa-utensils-alt',
  'far fa-venus',
  'far fa-venus-double',
  'far fa-venus-mars',
  'far fa-vial',
  'far fa-vials',
  'far fa-video',
  'far fa-video-plus',
  'far fa-video-slash',
  'far fa-volleyball-ball',
  'far fa-volume-down',
  'far fa-volume-mute',
  'far fa-volume-off',
  'far fa-volume-up',
  'far fa-warehouse',
  'far fa-warehouse-alt',
  'far fa-watch',
  'far fa-weight',
  'far fa-wheelchair',
  'far fa-whistle',
  'far fa-wifi',
  'far fa-window',
  'far fa-window-alt',
  'far fa-window-close',
  'far fa-window-maximize',
  'far fa-window-minimize',
  'far fa-window-restore',
  'far fa-wine-glass',
  'far fa-won-sign',
  'far fa-wrench',
  'far fa-x-ray',
  'far fa-yen-sign',
];

