import {Component, Input, OnInit} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-badge',
  template: `
    <span class="badge badge-{{color}} {{isElevated ? 'elevated' : ''}}">
       {{label}}
     </span>
  `,
  styles: [`:host {
    display: inline-block;
  }`]
})
export class BadgeComponent implements OnInit {
  /**
   * Label for badge
   */
  @Input() label: string;
  /**
   * Elevated or Flat
   */
  @Input() isElevated: boolean;
  /**
   * Color option for badge
   */
  @Input() color: 'primary' | 'accent' | 'success' | 'danger' | 'warning' | 'info';

  constructor() {
  }

  ngOnInit(): void {
    this.color = NicoUtils.isNullOrUndefined(this.color) ? 'info' : this.color;
    this.isElevated = NicoUtils.isNullOrUndefined(this.isElevated) ? false : this.isElevated;
  }
}
