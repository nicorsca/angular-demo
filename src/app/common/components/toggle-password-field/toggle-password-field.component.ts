import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-toggle-password-field',
  template: `
    <div class="input-group password-input-group">
      <input [disabled]="isDisabled"
             [type]="viewPassword ? 'text' :'password'" [id]="id"
             (blur)="touched()"
             [value]="password"
             (keyup)="onInputValueChange($event)"
             class="form-control form-control-lg"
             [placeholder]="placeholder | translate">
      <span class="icon-btn btn-link-light btn-sm icon-btn-circular view-btn"
             (click)="togglePasswordView($event)">
        <i class="font-icon far fa-eye" *ngIf="viewPassword"></i>
        <i class="font-icon far fa-eye-slash" *ngIf="!viewPassword"></i>
      </span>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(
      () => TogglePasswordFieldComponent
    ),
    multi: true
     }
  ]
})
export class TogglePasswordFieldComponent implements OnInit, ControlValueAccessor {

  public password: string;

  public viewPassword: boolean;

  public changed: (value: string) => {};

  public touched: () => {};

  public isDisabled: boolean;

  /**
   * Inputs for placeholder, field id
   */

  @Input() id: string;

  @Input() placeholder: string;

  constructor() { }

  ngOnInit(): void {
    this.viewPassword = false;
  }

  public togglePasswordView($event): void {
    $event.stopPropagation();
    this.viewPassword = !this.viewPassword;
  }

  /**
   * ControlValueAccessor methods
   */

  writeValue(password: string): void {
    this.password = password;
  }

  registerOnChange(onChange: any): void {
    this.changed = onChange;
  }

  registerOnTouched(onTouched: any): void {
    this.touched = onTouched;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  /**
   * Call changed function to register change and set value on password
   */

  public onInputValueChange($event): void {
    this.changed($event.target.value);
    this.password = $event.target.value;
  }

}
