import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HodPillComponent } from './hod-pill.component';
import {
  TranslateModule

} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';

describe('HodPillComponent', () => {
  let component: HodPillComponent;
  let fixture: ComponentFixture<HodPillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HodPillComponent ],
      providers: [
          [...MOCK_PROVIDERS]
      ],
      imports: [
        TranslateModule,
        CommonModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HodPillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
