import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-hod-pill',
  templateUrl: './hod-pill.component.html',
  styles: [`
    :host {
      display: inline-flex;
    }
  `]
})
export class HodPillComponent implements OnInit {

  @HostBinding('class.app-hod-pill-element') add = true;

  @Input() department: any;

  constructor() { }

  ngOnInit(): void {

  }
}
