import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {NicoUtils} from '@system/utilities';
import {DrawerAnimation} from '@common/animations/app.animations';

@Component({
  selector: 'app-drawer-container',
  templateUrl: './drawer-container.component.html',
  styleUrls: ['./drawer-container.component.css'],
  animations: [DrawerAnimation]
})
export class DrawerContainerComponent implements OnInit, OnChanges {

  /**
   * Determines the position for the drawerContainer (start:left, end: right)
   */
  @Input() position: 'start' | 'end';
  /**
   * Determines weather the drawerContainer should have overlay or not
   */
  @Input() hasOverlay: boolean;
  /**
   * Determines the state of drawerContainer visibility
   */
  @Input() isOpen: boolean;
  /**
   * Custom className for drawerContainer
   */
  @Input() drawerContainerClass: string;
  /**
   * Event triggered on drawer close
   * Returns an observable when the drawer is finished closing.
   */
  @Output() closeDrawerEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit(): void {
    this.position = NicoUtils.isNullOrUndefined(this.position) ? 'start' : this.position;
    this.hasOverlay = NicoUtils.isNullOrUndefined(this.hasOverlay) ? true : this.hasOverlay;
    this.isOpen = NicoUtils.isNullOrUndefined(this.isOpen) ? false : this.isOpen;
    this.drawerContainerClass = NicoUtils.isNullOrUndefined(this.drawerContainerClass) ? null : this.drawerContainerClass;
  }

  ngOnChanges(simpleChanges: SimpleChanges): void {
    if (!NicoUtils.isNullOrUndefined(simpleChanges.isOpen?.currentValue)) {
      if (simpleChanges?.isOpen?.currentValue &&
        document.documentElement.scrollHeight > document.documentElement.clientHeight) {
        if (this.position === 'start') {
          document.documentElement.classList.add('no-scroll');
        } else {
          document.documentElement.classList.add('hide-scroll');
        }
      } else {
        if (document.documentElement.classList.contains('hide-scroll')) {
          document.documentElement.classList.remove('hide-scroll');
        }
        if (document.documentElement.classList.contains('no-scroll')) {
          document.documentElement.classList.remove('no-scroll');
        }
      }
    }
  }

  public closeDrawer($event): void {
    $event.stopPropagation();
    this.isOpen = false;
    this.closeDrawerEvent.emit({isOpen: this.isOpen});
  }
}
