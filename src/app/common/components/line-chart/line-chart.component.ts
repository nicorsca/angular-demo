import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {

  @Input() chartOption: any;
  @Input() stacked: boolean;

  constructor() {
    this.stacked = false;
  }

  ngOnInit(): void {
  }

}
