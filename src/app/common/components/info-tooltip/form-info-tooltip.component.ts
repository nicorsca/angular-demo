import {Component, Input, OnInit} from '@angular/core';
import {TooltipPosition} from '@angular/material/tooltip';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-form-info-tooltip',
  template: `
    <div class="form-info-tooltip" [matTooltip]="tooltipData | translate" [matTooltipPosition]="matTooltipPosition">
      <i class="font-icon far fa-info-circle"></i>
    </div>
  `
})
export class FormInfoTooltipComponent implements OnInit {

  @Input() tooltipData: string;

  @Input() matTooltipPosition: TooltipPosition;

  constructor() { }

  ngOnInit(): void {
    this.matTooltipPosition = NicoUtils.isNullOrUndefined(this.matTooltipPosition) ? 'above' : this.matTooltipPosition;
  }

}
