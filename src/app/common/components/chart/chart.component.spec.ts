import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ChartComponent} from './chart.component';
import {NicoHttpClient} from '@system/http-client';
import {
  MockAjaxSpinnerService,
  MockFormBuilder, MockMatDialog, MockMatSnackBar,
  MockNicoHttpClient,
  MockNicoPageTitle, MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder} from '@angular/forms';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';

describe('ChartComponent', () => {
  let component: ChartComponent;
  let fixture: ComponentFixture<ChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartComponent ],
      providers: [
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormBuilder, useClass: MockFormBuilder},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: AjaxSpinnerService, useClass: MockAjaxSpinnerService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useValue: MockMatDialog},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
