import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  @Input() fillValue: any;

  @Input() value: any;

  @Input() startValue: any;

  @Input() endValue: any;

  @Input() color: string;

  @Input() attachLabel: string;



  constructor() { }

  ngOnInit(): void {
  }

}
