import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from '@angular/material/snack-bar';

@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss']
})

export class SnackBarComponent implements OnInit {

  constructor(
    private snackBarRef: MatSnackBarRef<any>,
    @Inject(MAT_SNACK_BAR_DATA) public data: SnackBarAlertInterface,
  ) {
  }

  ngOnInit(): void {
  }

  public dismiss(): void {
    this.snackBarRef.dismiss();
  }
}

export interface SnackBarAlertInterface {
  type: 'danger' | 'default' | 'info' | 'success' | 'warning';
  message: string;
  title?: string;
  withSolidBackground?: boolean;
}
