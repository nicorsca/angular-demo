import {Component, EventEmitter, Injector, OnInit, Output} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {AccordionAnimation} from '@common/animations/app.animations';
import {Router} from '@angular/router';
import {AbstractBaseComponent} from '../../../AbstractBaseComponent';
import {EmployeeRoleEnum} from '@common/enums/employeeRole.enum';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css'],
  animations: [AccordionAnimation]
})

export class SideNavComponent extends AbstractBaseComponent implements OnInit {

  public isOpen = false;
  public isPinned = false;
  @Output() sidenavCollapse: EventEmitter<boolean> = new EventEmitter<boolean>();

  public role: string;

  public roleEnum = EmployeeRoleEnum;

  public sideNavLinks: any;

  public screenWidth;

  constructor(public router: Router, protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    /* Web Components are only available for development environment */
    super.ngOnInit();
    if (this.authUserItems) {
      this.role = JSON.parse(this.authUserItems).role_name;
    }
    this.sideNavLinks = [
      {link: '/manage/dashboard', label: 'Dashboard', icon: 'far fa-home-lg-alt'},
      {link: '/manage/employees', label: 'Employees', icon: 'far fa-user', role: this.role},
      {link: '/manage/departments', label: 'Departments', icon: 'far fa-briefcase', role: this.role},
      {
        link: '', label: 'Leaves Management', icon: 'far fa-house-leave', isOpen: false,
        children: [
          {
            link: '/manage/my-leaves',
            label: 'My leaves',
            icon: 'far fa-user',
            isOpen: null,
            children: null
          },
          {
            link: '/manage/admin-leaves', label: 'Admin', icon: 'far fa-user-tie', isOpen: null, role: this.role,
            children: null
          },
        ]
      },
      {
        link: '', label: 'Attendance management', icon: 'far fa-id-badge', isOpen: false,
        children: [
          {
            link: '/manage/personal-attendance',
            label: 'My attendance',
            icon: 'far fa-user',
            isOpen: false,
            children: [
              {link: '/manage/personal-attendance/my-dashboard', label: 'Dashboard', icon: 'far fa-tachometer-alt-slowest'},
              // {link: '/manage/personal-attendance/attendance-history', label: 'History', icon: 'far fa-history'}
            ]
          },
          {
            link: '/manage/admin-attendance', label: 'Admin', icon: 'far fa-user-tie', isOpen: null, role: this.role,
            children: [
              {link: '/manage/admin-attendance/dashboard', label: 'Dashboard', icon: 'far fa-tachometer-alt-slowest'},
              {link: '/manage/admin-attendance/report', label: 'Report', icon: 'fas fa-file-chart-line'},
            ]
          }
        ]
      }
    ];
    if (!environment.production) {
      this.sideNavLinks = [...this.sideNavLinks, {link: '/web-components', label: 'Components', icon: 'far fa-cube'}];
    }

    this.screenWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
  }

  public toggleSideNavPinnedState($event): void {
    $event.stopPropagation();
    this.isPinned = !this.isPinned;
    this.sidenavCollapse.emit(this.isPinned);
  }

  public toggleSideNav($event): void {
    $event.stopPropagation();
    this.isOpen = !this.isOpen;
  }

  public onMouseOver($event): void {
    $event.stopPropagation();
    if(this.screenWidth > 600) {
      this.isOpen = true;
    }
  }

  public onMouseOut($event): void {
    $event.stopPropagation();
    if(this.screenWidth > 600) {
      this.isOpen = false;
    }
  }

  public toggleChildrenNavItems($event, navItem): void {
    if (navItem.children && navItem.children.length > 0) {
      $event.preventDefault();
      $event.stopPropagation();
      navItem.isOpen = !navItem.isOpen;
    }
  }

  public isLinkActive(navItem): any {
    const url = this.router.url.split('?')[0];
    if (navItem.children) {
      const found = navItem.children.find((child) => child.link === url);
      return !!found;
    } else {
      return url === navItem.link;
    }
  }
}
