export interface PunchInAttendance {
  type: string;
  browser: string;
  device: string;
  location: string;
  ip: string;
  fingerprint: string;
}
