import {Component, Input, OnInit} from '@angular/core';
import {ChartLegendInterface} from '@common/interface/chart-legend.interface';

@Component({
  selector: 'app-chart-legend',
  templateUrl: './chart-legend.component.html',
  styleUrls: ['./chart-legend.component.scss']
})
export class ChartLegendComponent implements OnInit {

  @Input() chartTitle: string;

  @Input() chartLegend: ChartLegendInterface[];

  constructor() { }

  ngOnInit(): void {
  }

  public assignMarkerColor(legend: ChartLegendInterface): string {
    if (legend.value) {
      return `color: ${legend.marker}`;
    }
    return `background-color: ${legend.marker};`;
  }

}
