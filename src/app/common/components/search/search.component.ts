import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {NicoUtils} from '@system/utilities';
import {NicoStorageService} from '@system/services';
import {ActivatedRoute} from '@angular/router';
import {SearchNavEnum} from '@common/enums/searchNav.enum';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-search-nav',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit, OnChanges {

  @HostBinding('class.page-header-right') add = true;

  @Input() config: SearchNavConfigInterface = {
    enableSearchInput: true,
    // enableStatus: true,
    enableToggle: true,
    enableSortOrder: true,
    enableSortBy: true,
    enableStatus: true,
    enableCheckLeave: false
  };

  @Input() viewToggleIcon: string;
  @Input() dropdownOptions: any[];
  @Input() dropdownValue: any;

  @Input() searchPlaceholderText: string;

  @Input() statusDropdownValues: any[];
  @Input() sortByDropdownValues: any[];
  @Input() checkLeaveDropdownValues: any[];

  @Output() searchChange: EventEmitter<string>;
  @Output() viewToggleClick: EventEmitter<MouseEvent>;
  @Output() dropdownChange: EventEmitter<any>;
  @Output() sortOrderChange: EventEmitter<string>;
  @Output() sortByChange: EventEmitter<string>;
  @Output() statusChange: EventEmitter<boolean | number>;
  @Output() checkLeaveChange: EventEmitter<boolean | number>;
  @Output() viewToggleChange: EventEmitter<boolean>;

  @ViewChild('dropDownItemTemplate') dropdownItemTemplate: TemplateRef<any>;

  public searchText = '';

  public sortOrder: SearchNavEnum.Ascending | SearchNavEnum.Descending;

  public sortBy: string;

  public status: number;

  public onLeave: number;

  public toggleView: boolean;

  public selectedIcon = 'far fa-circle';

  public searchNavEnum = SearchNavEnum;

  constructor(protected storageService: NicoStorageService, protected activatedRoute: ActivatedRoute, protected translateService: TranslateService) {
    this.viewToggleClick = new EventEmitter<MouseEvent>();
    this.searchChange = new EventEmitter<string>();
    this.dropdownChange = new EventEmitter<any>();
    this.sortOrderChange = new EventEmitter<string>();
    this.sortByChange = new EventEmitter<string>();
    this.statusChange = new EventEmitter<boolean | number>();
    this.checkLeaveChange = new EventEmitter<boolean | number>();
    this.viewToggleChange = new EventEmitter<boolean>();
  }

  public initConfig(): void {
    this.config.enableSearchInput = NicoUtils.isNullOrUndefined(this.config.enableSearchInput) ? true : this.config.enableSearchInput;
    this.config.enableToggle = NicoUtils.isNullOrUndefined(this.config.enableToggle) ? true : this.config.enableToggle;
    this.config.enableStatus = NicoUtils.isNullOrUndefined(this.config.enableStatus) ? true : this.config.enableStatus;
  }

  private initDropdownOptions(): void {
    if (!this.dropdownOptions) {
      this.dropdownOptions = [this.translateService.instant('MOD_COMMON.MOD_SEARCH.ACTIVE_DROPDOWN_OPTION'), this.translateService.instant('MOD_COMMON.MOD_SEARCH.INACTIVE_DROPDOWN_OPTION')];
    }
  }

  ngOnChanges(): void {
    this.initDropdownOptions();
  }

  public ngOnInit(): void {
    this.initDropdownOptions();
    this.initConfig();
    this.activatedRoute.queryParams?.subscribe(params => {
      this.sortOrder = NicoUtils.isNullOrUndefined(params?.sort_order) ?
        SearchNavEnum.Ascending : params.sort_order;
      if (this.sortByDropdownValues) {
        this.sortBy = NicoUtils.isNullOrUndefined(params?.sort_by) ?
          this.sortByDropdownValues[0].value : params.sort_by;
      }
      if (this.checkLeaveDropdownValues) {
        this.onLeave = (NicoUtils.isNullOrUndefined(params?.on_leave) || params?.on_leave === '') ?
          this.checkLeaveDropdownValues[0].value : params.on_leave;
      }
      if (this.statusDropdownValues){
            this.status = (
              NicoUtils.isNullOrUndefined(params?.status) || params?.status === '') ?
              this.statusDropdownValues[0]?.value : params?.status;
          }
    });
  }

  public toggleCollapsedView(evt: MouseEvent): void {
    this.viewToggleClick.emit(evt);
  }

  public onSelectItemPick(evt: any): void {
    this.dropdownChange.emit(evt);
  }

  public onSearchValueChange(): void {
    this.searchChange.emit(this.searchText);
  }

  public onSearchTextClear(): void {
    this.searchText = '';
    this.onSearchValueChange();
  }

  public onSortByChange(): void {
    this.sortByChange.emit(this.sortBy);
  }

  public onCheckLeaveChange(): void {
    this.checkLeaveChange.emit(this.onLeave);
  }

  public onStatusChange(): void {
    // switch (Number(this.status)) {
    //   case EmployeeStatus.Inactive: {
    //     this.selectedIcon = 'far fa-eye-slash';
    //     break;
    //   }
    //   case EmployeeStatus.Active: {
    //     this.selectedIcon = 'far fa-eye';
    //     break;
    //   }
    //   default: {
    //     this.selectedIcon = 'far fa-circle';
    //     break;
    //   }
    // }
    this.selectedIcon = 'far fa-circle';
    this.statusChange.emit(this.status);
  }

  public toggleSortOrder($event, order: SearchNavEnum.Ascending | SearchNavEnum.Descending): void {
    $event.stopPropagation();
    this.sortOrder = order;
    this.sortOrderChange.emit(this.sortOrder);
  }

  public onToggleView($event): void {
    $event.stopPropagation();
    this.toggleView = !this.toggleView;
    this.storageService.setItem('view', String(this.toggleView));
    this.viewToggleChange.emit(this.toggleView);
  }
}

export interface SearchNavConfigInterface {
  enableSearchInput?: boolean;
  // enableStatus?: boolean;
  enableToggle?: boolean;
  enableSortOrder?: boolean;
  enableSortBy?: boolean;
  enableStatus?: boolean;
  enableCheckLeave?: boolean;
}
