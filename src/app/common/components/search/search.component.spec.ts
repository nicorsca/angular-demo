import {SearchComponent} from './search.component';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {DepartmentService} from '@common/services/department.service';
import {MockDepartmentService} from '@common/testing-resources/mock-services/MockDepartmentService';
import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup, FormsModule} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler, TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchComponent ],
      providers: [
      {provide: DepartmentService, useClass: MockDepartmentService},
      {provide: NicoHttpClient, useClass: MockNicoHttpClient},
      {provide: FormGroup, useClass: MockFormGroup},
      {provide: NicoStorageService, useClass: MockNicoStorageService},
      {provide: NicoPageTitle, useClass: MockNicoPageTitle},
      {provide: NicoSessionService, useClass: MockNicoSessionService},
      {provide: MatSnackBar, useClass: MockMatSnackBar},
      {provide: MatDialog, useValue: MockMatDialog},
      {provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
      {provide: ActivatedRoute, useValue:  { snapshot: {queryParams: {page: 1, sort_order: 'asc', per_page: 16, status: 'active'}}}},
      {provide: MatDialogRef, useValue: component},
      {provide: MAT_DIALOG_DATA, useValue: undefined},
      FormBuilder,
      AjaxSpinnerService,
      TranslateService,
      TranslateStore,
      {provide: TranslateLoader, useClass: TranslateFakeLoader},
      {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
      TranslateParser,
      {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
      {provide: USE_DEFAULT_LANG, useValue: undefined},
      {provide: USE_STORE, useValue: undefined},
      {provide: USE_EXTEND, useValue: undefined},
      {provide: DEFAULT_LANGUAGE, useValue: undefined},
    ],
      imports: [
      RouterTestingModule,
      TranslateModule,
      MatDialogModule,
        FormsModule
    ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create search nav component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize values', () => {
    component.initConfig();
    expect(component.config.enableStatus).toBeDefined();
  });

  it('should toggle collapsed view', () => {
    spyOn(component.viewToggleClick, 'emit').and.callThrough();
    const $event = new MouseEvent('click');
    component.toggleCollapsedView($event);
    expect(component.viewToggleClick.emit).toHaveBeenCalled();
  });

  it('should emit search text', () => {
    spyOn(component.dropdownChange, 'emit').and.callThrough();
    const $event = new Event('keypress');
    component.onSelectItemPick($event);
    expect(component.dropdownChange.emit).toHaveBeenCalled();
  });

  it('should change value of search change', () => {
    spyOn(component.searchChange, 'emit').and.callThrough();
    component.onSearchValueChange();
    expect(component.searchChange.emit).toHaveBeenCalled();
  });

  it('should clear search text', () => {
    component.statusDropdownValues = [
      {
        label: 'All',
        value: 'all'
      },
      {
        label: 'Active',
        value: 'active'
      }, {
        label: 'Inactive',
        value: 'inactive'
      }];
    fixture.detectChanges();
    fixture.whenStable();
    component.searchText = 'Test';
    component.onSearchTextClear();
    expect(component.searchText).toEqual('');
  });
});

