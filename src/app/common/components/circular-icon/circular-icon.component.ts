import {Component, Input, OnInit} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-circular-icon',
  template: `
    <div class="circular-icon-container {{config.containerClassName}}">
      <div class="circular-icon circular-icon-{{config?.color}} {{config.className}}">
        <span class="indicator" *ngIf="config?.withIndicator"></span>
        <div class="font-icon">
          <i class="{{config.iconName}}"></i>
        </div>
      </div>
    </div>
  `,
  styles: [`:host {
    display: inline-block;
  }`]
})

export class CircularIconComponent implements OnInit {
  /**
   * Config: CircularIconConfig
   * Note: This component's style is dependent on external style (scss) files <circular-icon.scss>
   */
  @Input() config: CircularIconConfig = {};

  constructor() {
  }

  ngOnInit(): void {
    this.initConfig();
  }

  public initConfig(): void {
    this.config.containerClassName = NicoUtils.isNullOrUndefined(this.config.containerClassName) ? '' : this.config.containerClassName;
    this.config.className = NicoUtils.isNullOrUndefined(this.config.className) ? '' : this.config.className;
    this.config.iconName = NicoUtils.isNullOrUndefined(this.config.iconName) ? 'far fa-search' : this.config.iconName;
    this.config.color = NicoUtils.isNullOrUndefined(this.config.color) ? 'accent' : this.config.color;
    this.config.withIndicator = NicoUtils.isNullOrUndefined(this.config.withIndicator) ? false : this.config.withIndicator;
  }
}

export interface CircularIconConfig {
  /**
   * Defines external custom classname for parent container
   */
  containerClassName?: string;
  /**
   * Defines custom classname for circular icon
   */
  className?: string;
  /**
   * Defines icon to be used on component
   */
  iconName?: string;
  /**
   * Defines which color (background and icon color)
   */
  color?: 'primary' | 'accent' | 'success' | 'danger' | 'warning' | 'info';
  /**
   * Toggles the visibility status of indicator
   */
  withIndicator?: boolean;
}
