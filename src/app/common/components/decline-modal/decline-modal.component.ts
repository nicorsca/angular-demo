import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AbstractBaseComponent} from '../../../AbstractBaseComponent';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AjaxSpinnerPositionEnum, NicoStorageService} from '@system/services';
import {LeaveService} from '@common/services/leave.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-decline-modal',
  templateUrl: './decline-modal.component.html',
  styleUrls: ['./decline-modal.component.scss']
})
export class DeclineModalComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public declineForm: FormGroup;
  public declineSubscription: Subscription;
  public declineFormID = 'declineFormID';
  public pageTitle = '';
  public userDetail: any;
  constructor(injector: Injector, @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<DeclineModalComponent>,
              public formBuilder: FormBuilder, public leaveService: LeaveService,
              public storageService: NicoStorageService
  ) {
    super(injector);
    if (this.storageService.getItem('user_details')) {
      this.userDetail = JSON.parse(this.storageService.getItem('user_details'));
    }
  }

  onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_LEAVE.LEAVE_DETAIL_LABEL');
    super.onComponentReady();
    this.onCreateForm();
  }

  public onCreateForm(): void {
    this.declineForm = this.formBuilder.group({
      reason: [null, Validators.required]
    });
  }
  public onConfirmDecline(): void{
    this.declineSubscription = this.leaveService.onCancelDeclineLeave(this.data.model, this.declineForm.value,
      this.getActiveUserState()).subscribe(response => {
      this.dialogRef.close();
      if (this.getActiveUserState()){
        this.showSuccessSnackBar(this.translateService.instant('MOD_LEAVE.LEAVE_CANCELLED_LABEL'));
      }
      else {
        this.showSuccessSnackBar(this.translateService.instant('MOD_LEAVE.LEAVE_DECLINED_LABEL'));
      }
    }, error => {
      this.onFormSubmissionError(error, this.declineFormID);
    });
  }

  public getActiveUserState(): boolean {
    return this.data.model?.employee.id === this.userDetail?.id;
  }
  public ngOnDestroy(): void {
    if (this.declineSubscription != null){
      this.declineSubscription.unsubscribe();
    }
  }
}
