import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {NicoUtils} from '@system/utilities';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'nico-paginator',
  templateUrl: 'paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})

export class PaginatorComponent implements OnInit, OnChanges {

  @Input() offset: number;
  /**
   * Per page limit
   */
  @Input() limit: number;
  /**
   * Total data
   */
  @Input() size: number;

  @Input() range = 1;
  /**
   * Current page
   */
  @Input() currentPage: number;

  /**
   * lazy loading option
   */
  @Input() lazyLoading = false;
  /**
   * Show "all" option
   */
  @Input() showAllOption = false;

  protected pageWindowSize = 4;

  public startIndex = 1;

  /**
   * Total pages
   */
  public totalPages: number;

  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  @Output() perPageLimitChange: EventEmitter<number> = new EventEmitter<number>();

  pages: number[];

  private isInitialLoad = true;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    // let the BaseComponent handle setting correct page in initialLoad
    // then when params change send event to change page
    // use self variable because the value of this is not the component inside the subscribe callback

    /*const self = this;
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.page && !self.isInitialLoad) {
        // reset the current page and re do pagination calculation
        const page = parseInt(params.page);
        self.currentPage = page;
        self.pageChange.emit(page);
        self.getPages();
      }
      self.isInitialLoad = false;
    });*/
    this.getPages();
  }

  ngOnChanges(): void {
    this.getPages();
  }

  getPages(): void {
    this.totalPages = Math.ceil(this.size / this.limit);
    if (isNaN(this.totalPages)) {
      this.totalPages = 0;
    }
    if (this.totalPages < this.pageWindowSize) {
      // don't reset the StartIndex when the lists is not loaded yet and paginator doesn't know about page size and items,
      // will cause a NAN error if it is reset without this check
      if (this.totalPages !== 0) {
        // set the startIndex back to 1 when pages are less than the maximumPagesAllowed (this.pageWindowSize)
        // to make sure all the page numbers are shown
        this.startIndex = 1;
      }
      this.pages = new Array(this.totalPages);
    } else {
      this.pages = new Array(this.pageWindowSize + 1);
      this.startIndex = Math.floor(this.currentPage / this.pageWindowSize) * (this.pageWindowSize);
      if (this.totalPages - this.currentPage <= this.pageWindowSize) {
        this.startIndex = this.totalPages - this.pageWindowSize;
      }
    }
  }

  selectPage(page: number): void {
    if (this.isValidPageNumber(page, this.totalPages)) {
      this.pageChange.emit(page);
    }
  }

  isValidPageNumber(page: number, totalPages: number): boolean {
    return page > 0 && page <= totalPages;
  }

  public onPageLimitChange(value): void {
    this.limit = value;
    this.perPageLimitChange.emit(value);
  }
}
