import {ComponentFixture, TestBed} from '@angular/core/testing';

import {NotificationDrawerComponent} from './notification-drawer.component';

describe('NotificationDrawerComponent', () => {
  let component: NotificationDrawerComponent;
  let fixture: ComponentFixture<NotificationDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificationDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create notification drawer component', () => {
    expect(component).toBeTruthy();
  });

  it('should call close drawer on event', () => {
    spyOn(component.drawerOpenEvent, 'emit');
    const $event = new MouseEvent('click');
    component.closeDrawer($event);
    expect(component.drawerOpenEvent.emit).toHaveBeenCalled();
  });
});
