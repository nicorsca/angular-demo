import { Router } from '@angular/router';
import { NicoSessionService, NicoStorageService } from '@system/services';
import { Injectable } from '@angular/core';
import { NicoHttpErrorInterceptorInterface } from '@system/http-client';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarService } from '@common/services/snackbar.service';
import { ErrorStatusCodeEnum } from '@common/enums/errorStatusCode.enum';

@Injectable()
export class HttpErrorInterceptor implements NicoHttpErrorInterceptorInterface {
  protected snackbar: MatSnackBar;

  constructor(
    protected router: Router,
    protected session: NicoSessionService,
    protected storage: NicoStorageService,
    protected dialog: MatDialog,
    protected snackBar: SnackbarService
  ) {}

  onError(error: HttpErrorResponse): void {
    switch (error.status) {
      case ErrorStatusCodeEnum.MultipleChoice: {
        this.snackBar.showInfoSnackBar(error.error.message);
        this.router.navigate(['auth/change-password-first-login']).then();
        break;
      }
      case ErrorStatusCodeEnum.Unauthorized: {
        this.dialog.closeAll();
        this.snackBar.showErrorSnackBar(error.error.message);
        this.storage.clear();
        this.router.navigate(['auth/login']).then();
        break;
      }
      default: {
        this.snackBar.showErrorSnackBar(error.error.message);
        break;
      }
    }
  }
}
