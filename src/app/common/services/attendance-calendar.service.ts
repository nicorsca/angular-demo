import {Injectable} from '@angular/core';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {BaseResource} from '@system/datamodels';
import {Observable} from 'rxjs';
import {NicoHttpClient} from '@system/http-client';
import {AttendanceCalendarModel} from '@common/models';
import {HttpParams} from '@angular/common/http';
import {NicoCollection} from '@system/utilities';

@Injectable({
  providedIn: 'root'
})
export class AttendanceCalendarService extends AbstractBaseService {
  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new AttendanceCalendarModel();
  }

  getResourceName(): string {
    return 'attendances/reports/employees';
  }

  public getCalendarDetails(params: HttpParams, employeeId: any): Observable<NicoCollection<AttendanceCalendarModel>> {
    return this.get(params, this.resourceUrl + `/${employeeId}` + '/calendar');
  }
}
