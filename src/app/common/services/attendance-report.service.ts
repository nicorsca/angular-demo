import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AttendanceReportModel} from '@common/models/attendance-report.model';

@Injectable({
  providedIn: 'root'
})
export class AttendanceReportService extends AbstractBaseService {
  protected baseModel: BaseResource;



  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new AttendanceReportModel();
  }

  getResourceName(): string {
    return 'attendances/reports';
  }

  public getAttendanceTodaySummary(params?: HttpParams): Observable<AttendanceReportModel> {
    return this.get(params, `${this.resourceUrl}/today/summary`);
  }
  public getAttendanceTodayAttendance(params?: HttpParams): Observable<AttendanceReportModel> {
    return this.get(params, `${this.resourceUrl}/today/attendance`);
  }
  public getReportSummary(params?: HttpParams): Observable<AttendanceReportModel> {
    return this.get(params, `${this.resourceUrl}/summary`);
  }


  public getChartData(params?: HttpParams): Observable<AttendanceReportModel> {
    return this.get(params , `${this.resourceUrl}/average`, true);
  }

  public getSeriesData(checkIn: any, checkOut: any, startThreshold: number, endTimeThreshold: number): Array<any>{
    const seriesData = [
        {
          name: 'Check in',
          type: 'line',
          data: this.getCalculatedCheckIn(checkIn, startThreshold),
          emphasis: {
            focus: 'series'
          },
          smooth: true,
          animationDelay: (idx) => idx * 10,
          symbolSize: 7.5,
          symbol: 'circle',
          itemStyle: {
            color: '#E84118',
            borderColor: '#E84118',
          },
          lineStyle: {
            width: 0.5,
            color: '#000000',

          },
        },
        {
          name: 'Check out',
          type: 'line',
          data: this.getCalculatedCheckOut(checkOut, endTimeThreshold),
          smooth: true,
          emphasis: {
            focus: 'series'
          },
          symbolSize: 7.5,
          symbol: 'circle',
          itemStyle: {
            color: '#855CF8',
            borderColor: '#855CF8'
          },
          lineStyle: {
            width: 0.5,
            color: '#122f1e',
          },
          animationDelay: (idx) => idx * 10 + 100,
        },
      ];
    return seriesData;
  }

  public getCalculatedCheckIn(data: any, threshold: number): any {
    const calculated = [];
    data.map(d => {
      if (d < threshold) {
        calculated.push({value: d,
          name: 'Late',
          itemStyle: {
            color: '#855CF8',
            borderColor: '#855CF8'
          }});
      }
      else {
        calculated.push({value: d, name: 'On-time', stack: 'Total'});
      }
    });
    return calculated;
  }

  public getCalculatedCheckOut(data: any, threshold: number): any {
    const calculated = [];
    data.map(d => {
      if (d < threshold) {
        calculated.push({value: d,
          name: 'Early',
          itemStyle: {
            color: '#E84118',
            borderColor: '#E84118'
          }});
      }
      else {
        calculated.push({value: d, name: 'On-time', stack: 'Total'});
      }
    });
    return calculated;
  }

  public getEmployeeAttendanceReport(params?: HttpParams): Observable<AttendanceReportModel> {
    return this.get(params , `${this.resourceUrl}/calendar`, true);
  }


 public getSeriesBarData(data: any): any {
    return  {
      data: this.getCalculatedBarChartData(data),
      type: 'bar'
    };
 }

 public getCalculatedBarChartData(data: any): any {
    const calculated = [];
    data.map(d => {
    if (d.holiday || d.weekend) {
      calculated.push({value: '1800',
        name: d.holiday ? 'Holiday' : 'Weekend',
        itemStyle: d.holiday ? {
          color: '#E84118'
        } : {
          color:  '#B3B3B3'
        }});
    }
    else{
      calculated.push({
        value: d.avg_work_time,
        name: 'Present',
        itemStyle: {
          color: '#32A321'
        }
      });
    }
    });
    return calculated;
 }
}
