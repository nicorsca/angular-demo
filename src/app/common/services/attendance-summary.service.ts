import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PaginatedNicoCollection} from '@system/utilities';
import {AttendanceSummaryModel} from '@common/models';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AttendanceSummaryService extends AbstractBaseService {

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new AttendanceSummaryModel();
  }

  getResourceName(): string {
    return 'attendances/reports/summary';
  }
  public getAttendanceSummary(params: HttpParams): Observable<PaginatedNicoCollection<AttendanceSummaryModel>> {
    this.baseModel = new AttendanceSummaryModel();
    return this.get(params, this.resourceUrl);
  }
}
