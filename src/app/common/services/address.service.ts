import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AddressModel} from '@common/models/address.model';
import {PaginatedNicoCollection} from '@system/utilities';

@Injectable({
  providedIn: 'root'
})
export class AddressService extends AbstractBaseService{

  protected baseModel: BaseResource;
  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new AddressModel();
  }


  getResourceName(): string {
    return `employees`;
  }

  public saveAddress(id: string, data: AddressModel, model?: AddressModel, params?: HttpParams): Observable<AddressModel> {
    if (model){
      return this.put(model, data, params, `${this.resourceUrl}/${id}/addresses/${model.id}` );
    }
    else {
      return this.post( data,  params, `${this.resourceUrl}/${id}/addresses`);
    }
  }
  public getAddress(id: string, params?: HttpParams): Observable<PaginatedNicoCollection<AddressModel>> {
    return this.get(params, `${this.resourceUrl}/${id}/addresses`);
  }

  public deleteAddress(id: string, model: AddressModel, params?: HttpParams): Observable<any> {
    return this.delete(model, params, `${this.resourceUrl}/${id}/addresses/${model.id}`);
  }

}
