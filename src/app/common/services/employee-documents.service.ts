import {AbstractBaseService} from '@common/services/abstract-base.service';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {EmployeeDocumentsModel} from '@common/models';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PaginatedNicoCollection} from '@system/utilities';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeDocumentsService extends AbstractBaseService {
  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new EmployeeDocumentsModel();
  }

  getResourceName(): string {
    return 'employees';
  }

  public getDocuments(id: any, params?: HttpParams): Observable<PaginatedNicoCollection<EmployeeDocumentsModel>> {
    return this.get(params, this.resourceUrl + `/${id}` + '/documents');
  }

  public saveDocument(body: EmployeeDocumentsModel, id: any, documentModel?: any): Observable<PaginatedNicoCollection<EmployeeDocumentsModel>> {
    if (documentModel) {
      return this.put(documentModel, body, null, this.resourceUrl + `/${id}` + '/documents' + `/${documentModel.id}`);
    } else {
      return this.post(body, null, this.resourceUrl + `/${id}` + '/documents');
    }
  }

  public deleteDocument(id: any, documentModel: any): Observable<PaginatedNicoCollection<EmployeeDocumentsModel>> {
    return this.delete(documentModel, null, this.resourceUrl + `/${id}` + '/documents' + `/${documentModel.id}`);
  }

  public toggleDocumentStatus(id: any, documentModel: any): Observable<EmployeeDocumentsModel> {
    return this.put(documentModel, null, null, this.resourceUrl + `/${id}` + '/documents' + `/${documentModel.id}` + '/status');
  }
}
