 import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {AttendanceDetailModel} from '@common/models/attendance-detail.model';
import {HttpParams} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {PaginatedNicoCollection} from '@system/utilities';
import {AttendanceModel, AttendanceSummaryModel} from '@common/models';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService extends AbstractBaseService {
  protected baseModel: BaseResource;


  public attendanceChange: Subject<string> = new Subject<string>();

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new AttendanceModel();
  }

  getResourceName(): string {
    return 'attendances';
  }

  // public getEmployeeAttendance(params: HttpParams): Observable<PaginatedNicoCollection<AttendanceDetailModel>> {
  //   this.baseModel = new AttendanceDetailModel();
  //   return this.get(params, this.resourceUrl + '/details');
  // }

  // public editTimeLog(body: AttendanceDetailModel, id: any, attendanceModel: AttendanceDetailModel): Observable<PaginatedNicoCollection<AttendanceDetailModel>> {
  //   this.baseModel = new AttendanceDetailModel();
  //   return this.put(attendanceModel, body, null, this.resourceUrl + `/${id}`);
  // }

  public setattendanceChange(): void {
    this.attendanceChange.next();
  }

  public watchObservableChange(): Observable<any> {
    return this.attendanceChange.asObservable();
  }


}
