import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {NicoHttpClient} from '@system/http-client';
import {UserModel} from '@common/models';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseResource} from '@system/datamodels';

@Injectable({
  providedIn: 'root'
})
export class AuthUserChangePasswordService extends AbstractBaseService {

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new UserModel();
  }

  getResourceName(): string {
    return 'auth/change-password';
  }

  public put(body, params?: HttpParams): Observable<any> {
    return this.httpClient.put(this.resourceUrl, body, {params, });
  }

}
