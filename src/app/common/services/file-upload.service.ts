import {AbstractBaseService} from '@common/services/abstract-base.service';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {FileUploadModel} from '@common/models/file-upload.model';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService extends AbstractBaseService {
  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new FileUploadModel();
  }

  getResourceName(): string {
    return 'files/upload';
  }
}
