import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {NicoUtils} from '@system/utilities';
import {NicoStorageService} from '@system/services';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private storageService: NicoStorageService, private router: Router) {
  }

  canActivate(): boolean {
    if (!NicoUtils.isNullOrUndefined(this.storageService.getItem('access_token'))) {
      this.router.navigate(['/manage/dashboard']);
      return false;
    }
    return true;
  }
}
