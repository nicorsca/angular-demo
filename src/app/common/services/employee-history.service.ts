import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {EmployeeHistoriesModel} from '@common/models/employee-histories.model';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PaginatedNicoCollection} from '@system/utilities';

@Injectable({
  providedIn: 'root'
})
export class EmployeeHistoryService extends AbstractBaseService {
  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new EmployeeHistoriesModel();
  }

  getResourceName(): string {
    return 'employees';
  }

  public getHistories(id: any, params?: HttpParams): Observable<PaginatedNicoCollection<EmployeeHistoriesModel>> {
    return this.get(params, this.resourceUrl + `/${id}` + '/histories');
  }

  public toggleEmployeeStatus(body: EmployeeHistoriesModel, id: any, activateEmployee: boolean, params?: HttpParams): Observable<EmployeeHistoriesModel> {
    if (activateEmployee) {
      return this.put(body, body, null, this.resourceUrl + `/${id}` + '/activate');
    } else {
      return this.put(body, body, null, this.resourceUrl + `/${id}` + '/deactivate');
    }
  }

}
