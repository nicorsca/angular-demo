import { Injectable } from '@angular/core';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {NicoHttpClient} from '@system/http-client';
import {BaseResource} from '@system/datamodels';
import {SummaryModel} from '@common/models/summary.model';

@Injectable({
  providedIn: 'root'
})
export class SummaryService extends AbstractBaseService{

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new SummaryModel();
  }

  getResourceName(): string {
    return 'leaves/summary';
  }
}
