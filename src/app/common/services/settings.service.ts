import { Injectable } from '@angular/core';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {NicoHttpClient} from '@system/http-client';
import {BaseResource} from '@system/datamodels';
import {SettingModel} from '@common/models/settings.model';

@Injectable({
  providedIn: 'root'
})
export class SettingService extends AbstractBaseService{

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new SettingModel();
  }

  getResourceName(): string {
    return 'settings';
  }
}
