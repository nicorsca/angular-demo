import { Injectable } from '@angular/core';

import {HttpInterceptor, HttpRequest, HttpHandler} from '@angular/common/http';

import { finalize } from 'rxjs/operators';
import {LoaderService} from '@common/services/loader.service';
@Injectable({
  providedIn: 'root'
})
export class LoaderInterceptor implements HttpInterceptor  {

  public urlNoLoader: Array<string> = [''];
  constructor(private loader: LoaderService) {
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): any {
    if (!req.params.keys().includes('skip')) {

      this.loader.show();

      return next.handle(req).pipe(finalize(() => this.loader.hide()));

    } else {
      return next.handle(req);
    }
  }
}
