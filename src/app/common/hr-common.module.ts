import {NgModule} from '@angular/core';
import {AvatarComponent} from './components/avatar/avatar.component';
import {BadgeComponent} from './components/badge/badge.component';
import {CircularIconComponent} from './components/circular-icon/circular-icon.component';
import {HeaderComponent} from './components/header/header.component';
import {PillComponent} from './components/pill/pill.component';
import {SideNavComponent} from './components/side-nav/side-nav.component';
import {SwitchComponent} from './components/switch/switch.component';
import {AuthMiddleware, GuestMiddleware} from './middlewares';
import {
  HttpErrorInterceptor, LoginService, SnackbarService,
} from './services';
import {CommonModule} from '@angular/common';
import {NotificationRowComponent} from './components/notification-row/notification-row.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NicoComponentsModule} from '@system/components';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EmptyStateComponent} from './components/empty-state/empty-state.component';
import {MatDialogModule} from '@angular/material/dialog';
import {DrawerContainerComponent} from './components/drawer-container/drawer-container.component';
import {NotificationDrawerComponent} from './components/notification-drawer/notification-drawer.component';
import {ConfirmationDialogComponent} from '@common/components/confirmation-dialog/confirmation-dialog.component';
import {SearchComponent} from '@common/components/search/search.component';
import {SnackBarComponent} from '@common/components/snack-bar/snack-bar.component';
import {TranslateModule} from '@ngx-translate/core';
import {PaginatorComponent} from '@common/components/pagination/paginator.component';
import {StaticSpinnerComponent} from '@common/components/static-spinner/static-spinner.component';
import {IconPickerComponent} from '@common/components/icon-picker/icon-picker.component';
import {ShortenPipe} from '@common/pipes/shorten.pipes';
import {TeamPipe} from '@common/pipes/team.pipes';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatMomentDateModule, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_LOCALE} from '@angular/material/core';
import {TogglePasswordFieldComponent} from '@common/components/toggle-password-field/toggle-password-field.component';
import {FormInfoTooltipComponent} from '@common/components/info-tooltip/form-info-tooltip.component';
import {BreadcrumbModule} from 'xng-breadcrumb';
import {SentenceCasePipe} from '@common/pipes/sentenceCase.pipe';
import {MAT_DATE_FORMATS} from '@common/constants/ngxCustomDateTimeFormat';
import {
  NGX_MAT_DATE_FORMATS,
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule
} from '@angular-material-components/datetime-picker';
import {UtcToLocalPipe} from '@common/pipes/utc-to-local.pipe';
import {DeclineModalComponent} from '@common/components/decline-modal/decline-modal.component';
import {PunchInOutComponent} from '@common/components/punch-in-out/punch-in-out.component';
import {OnlyNumberDirective} from './directives/only-number.directive';
import {DoughnutChartComponent} from './components/doughnut-chart/doughnut-chart.component';
import {UserPillComponent} from '@common/components/user-pill/user-pill.component';
import {StatusPillComponent} from './components/status-pill/status-pill.component';
import {HodPillComponent} from './components/hod-pill/hod-pill.component';

import { LineChartComponent } from './components/line-chart/line-chart.component';

import { InfoBlockComponent } from './components/info-block/info-block.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {LoaderInterceptor} from '@common/interceptors/loader.interceptor';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import {NgxEchartsModule} from 'ngx-echarts';
import { ChartLegendComponent } from './components/chart-legend/chart-legend.component';
import { DashboardInfoCardComponent } from './components/dashboard-info-card/dashboard-info-card.component';
import {ToggleDirective} from "@common/directives/toggle.directive";

// mbsc date time picker
/*** Items within declarations, exports, imports etc. should be in a separate line ****/

@NgModule({
  declarations: [
    AvatarComponent,
    BadgeComponent,
    CircularIconComponent,
    HeaderComponent,
    PillComponent,
    SideNavComponent,
    SwitchComponent,
    NotificationRowComponent,
    EmptyStateComponent,
    DrawerContainerComponent,
    NotificationDrawerComponent,
    ConfirmationDialogComponent,
    SearchComponent,
    StaticSpinnerComponent,
    IconPickerComponent,
    SnackBarComponent,
    PaginatorComponent,
    TogglePasswordFieldComponent,
    FormInfoTooltipComponent,
    UserPillComponent,
    DoughnutChartComponent,
    DeclineModalComponent,
    PunchInOutComponent,
    HodPillComponent,
    StatusPillComponent,
    InfoBlockComponent,

    /*** Directives ***/
    OnlyNumberDirective,
    ToggleDirective,

    /*** Pipes ***/
    ShortenPipe,
    TeamPipe,
    SentenceCasePipe,
    UtcToLocalPipe,
    ProgressBarComponent,
    LineChartComponent,
    ChartLegendComponent,
    DashboardInfoCardComponent,
  ],

    exports: [
        CommonModule,
        ShortenPipe,
        TeamPipe,
        FormsModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatDialogModule,
        TranslateModule,
        MatNativeDateModule,
        MatDatepickerModule,
        BreadcrumbModule,
        NgxMatDatetimePickerModule,
        NgxEchartsModule,

        /*** put pages export above and components export below this line ****/

        AvatarComponent,
        BadgeComponent,
        CircularIconComponent,
        HeaderComponent,
        PillComponent,
        SideNavComponent,
        SwitchComponent,
        NotificationRowComponent,
        EmptyStateComponent,
        DrawerContainerComponent,
        NotificationDrawerComponent,
        ConfirmationDialogComponent,
        SearchComponent,
        StaticSpinnerComponent,
        IconPickerComponent,
        PaginatorComponent,
        TogglePasswordFieldComponent,
        FormInfoTooltipComponent,
        SentenceCasePipe,
        OnlyNumberDirective,
        ToggleDirective,
        UtcToLocalPipe,
        DoughnutChartComponent,
        PunchInOutComponent,
        UserPillComponent,
        HodPillComponent,
        StatusPillComponent,
        ProgressBarComponent,
        LineChartComponent,
        InfoBlockComponent,
        ProgressBarComponent,
        ChartLegendComponent,
      DashboardInfoCardComponent
    ],
  providers: [
    AuthMiddleware,
    GuestMiddleware,
    HttpErrorInterceptor,
    LoginService,
    {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    {provide: NGX_MAT_DATE_FORMATS, useValue: MAT_DATE_FORMATS},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'DD-MM-YYYY',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY'
        },
      },
    },
    SnackbarService
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    MatSnackBarModule,
    NicoComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    TranslateModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatDatepickerModule,
    NgxMatDatetimePickerModule,
    MatMomentDateModule,
    NgxMatNativeDateModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ],
})

export class HrCommonModule {

}
