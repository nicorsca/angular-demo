import {BaseResource} from '@system/datamodels';

export class LeaveModel extends BaseResource{
  public creatableAttributes = [
    'title',
    'created_at',
    'days',
    'employee',
    'end_at',
    'is_emergency',
    'requested',
    'avatar',
    'code',
    'department',
    'email',
    'id',
    'name',
    'start_at',
    'status',
    'title',
    'type',
    'updated_at',
    'description',
    'reason'
  ];
}
