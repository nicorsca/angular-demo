import {BaseResource} from '@system/datamodels';

export class FileUploadModel extends BaseResource {
  public creatableAttributes = [
    'file',
    'path',
    'thumb_path'
  ];
}
