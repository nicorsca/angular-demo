import {BaseResource} from '@system/datamodels';

export class AttendanceSummaryModel extends BaseResource {
  public creatableAttributes = [
    'employee_id',
    'present_days',
    'leave_days',
    'avg_check_in',
    'avg_check_out',
    'total_office_time',
    'total_work_time',
    'avg_work_time',
    'total_break_time',
    'avg_break_time',
    'employee',
  ];
}
