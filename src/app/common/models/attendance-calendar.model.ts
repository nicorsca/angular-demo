import {BaseResource, BaseResourceInterface} from '@system/datamodels';

export class AttendanceCalendarModel extends BaseResource {
  public create(obj: any): BaseResourceInterface {
    return super.create(obj, true);
  }
}
