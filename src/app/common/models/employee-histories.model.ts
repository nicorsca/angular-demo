import {BaseResource} from '@system/datamodels';

export class EmployeeHistoriesModel extends BaseResource {
  public creatableAttributes = [
    'id',
    'date',
    'status',
    'comment',
    'employee_id',
    'url',
    'document',
    'thumbnail',
    'created_at',
    'updated_at'
  ];
}
