import {BaseResource} from '@system/datamodels';

export class AllocatedLeaveModel extends BaseResource {
  public creatableAttributes = [
    'id',
    'title',
    'days',
    'start_date',
    'end_date',
    'type',
    'status',
    'created_at',
    'updated_at'
  ];
}
