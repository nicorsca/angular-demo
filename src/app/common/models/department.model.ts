import {BaseResource} from '@system/datamodels';

export class DepartmentModel extends BaseResource {
  // public creatableAttributes = [
  //   'current_page',
  //   'data',
  //   'first_page_url',
  //   'from',
  //   'last_page',
  //   'last_page_url',
  //   'links',
  //   'next_page_url',
  //   'path',
  //   'per_page',
  //   'prev_page_url',
  //   'to',
  //   'total',
  // ];


  public creatableAttributes = [
    'id',
    'title',
    'description',
    'status',
    'created_at',
    'updated_at',
    'employees_count',
    'hod',
  ];

}
