import {BaseResource} from '@system/datamodels';
import {EmployeeDocument} from '@common/enums/employeeView.enums';

export class EmployeeDocumentsModel extends BaseResource {
  public fileType = EmployeeDocument;
  public creatableAttributes = [
    'id',
    'title',
    'url',
    'thumbnail',
    'type',
    'status',
    'created_at',
    'updated_at'
  ];
  public documentIcon: string;

  public create(obj: any): EmployeeDocumentsModel {
    const model = super.create(obj) as EmployeeDocumentsModel;
    model.getIconForDocument();
    return model;
  }

  // @ts-ignore
  public getIconForDocument(): string {
    switch (this.type) {
      case this.fileType.Docx:
        this.documentIcon = 'far fa-file-word';
        break;
      case this.fileType.Pdf:
        this.documentIcon = 'far fa-file-pdf';
        break;
      case this.fileType.Doc:
        this.documentIcon = 'far fa-image-word';
        break;
      case this.fileType.Png:
        this.documentIcon = 'far fa-file-image';
        break;
      case this.fileType.Jpeg:
        this.documentIcon = 'far fa-file-image';
        break;
      case this.fileType.Jpg:
        this.documentIcon = 'far fa-file-image';
        break;
      case this.fileType.Xlsx:
        this.documentIcon = 'far fa-file-excel';
        break;
      case this.fileType.Xls:
        this.documentIcon = 'far fa-file-excel';
        break;
      case this.fileType.Gif:
        this.documentIcon = 'far fa-file-video';
        break;
      case this.fileType.Svg:
        this.documentIcon = 'far fa-file-image';
        break;
      case this.fileType.OctetStream:
        this.documentIcon = 'far fa-file-word';
        break;
    }
  }
}
