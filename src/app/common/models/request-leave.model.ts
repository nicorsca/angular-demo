import {BaseResource} from '@system/datamodels';

export class RequestLeaveModel extends BaseResource{
  public creatableAttributes = [
    'title',
    'description',
    'start_at',
    'end_at',
    'type',
    'is_emergency',
    'requested_to',
    'id',
    'days',
    'status',
    'reason',
    'created_at',
    'updated_at'
];
}
