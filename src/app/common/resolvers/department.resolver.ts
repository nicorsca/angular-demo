import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';

import { Observable, of } from 'rxjs';
import {DepartmentService} from '@common/services';

@Injectable({
  providedIn: 'root'
})
export class DepartmentResolver implements Resolve<Observable<string>> {
  constructor(public departmentService: DepartmentService) {
  }
  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.departmentService.getDetail(route.params);
  }
}
