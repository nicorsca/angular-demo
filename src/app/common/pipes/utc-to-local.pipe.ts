import { Pipe, PipeTransform } from '@angular/core';
import {DateUtility} from '@common/utilities/date-utility';

@Pipe({
  name: 'utcToLocal'
})
export class UtcToLocalPipe implements PipeTransform{

  // tslint:disable-next-line:typedef
  transform(date: any, format?: string){
      if (format) {
        return DateUtility.setDateFormat(DateUtility.utcToLocalFormat(date), format);
      }
      return DateUtility.utcToLocalFormat(date);
  }
}
