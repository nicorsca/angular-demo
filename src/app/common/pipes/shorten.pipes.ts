import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform{

  private  limit = 20;
  // tslint:disable-next-line:typedef
  transform(value: any){
    if (value.length > this.limit){
      return value.substr(0, this.limit) + '......';
    }
    return value;
  }
}
