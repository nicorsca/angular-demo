import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {NicoSessionService} from '@system/services';
import {Injectable} from '@angular/core';

@Injectable()
export class AuthMiddleware implements CanActivate {

  constructor(protected router: Router, protected session: NicoSessionService) {
  }

  // tslint:disable-next-line:max-line-length
  // @ts-ignore
  // tslint:disable-next-line:max-line-length
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.session.getUser() != null) {
      return true;
    }
    this.router.navigate(['auth/login']).then();
    return false;
  }
}
