import {ActivatedRoute, Router} from '@angular/router';
import {of} from 'rxjs';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {
  MockAjaxSpinnerService, MockLoginService, MockMatDialog, MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService, MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {NicoHttpClient} from '@system/http-client';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {AppService, LoginService} from '@common/services';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler, TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {ChangePasswordService} from '@common/services/change-password.service';
import {HttpErrorResponse} from '@angular/common/http';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {BreadcrumbComponent, BreadcrumbService} from 'xng-breadcrumb';

export const MOCK_PROVIDERS = [
  {provide: ActivatedRoute, useValue: {
      snapshot: {
        queryParams: {
          page: '1'
        }
      },
      queryParams: of({
        access_token: 'test',
      })

    }},
  {provide: NicoPageTitle, useClass: MockNicoPageTitle},
  {provide: NicoHttpClient, useClass: MockNicoHttpClient},
  {provide: NicoSessionService, useClass: MockNicoSessionService},
  {provide: AjaxSpinnerService, useClass: MockAjaxSpinnerService},
  {provide: NicoStorageService, useClass: MockNicoStorageService},
  {provide: MatSnackBar, useClass: MockMatSnackBar},
  {provide: MatDialog, useClass: MockMatDialog},
  {provide: NicoStorageService, useClass: MockNicoStorageService},
  {provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
  AppService,
  FormBuilder,
  AjaxSpinnerService,
  TranslateService,
  TranslateStore,
  {provide: TranslateLoader, useClass: TranslateFakeLoader},
  {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
  TranslateParser,
  {provide: HttpErrorResponse, useValue: {headers: {}, error: {message: 'Test', status: '417'}}},
  {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
  {provide: USE_DEFAULT_LANG, useValue: undefined},
  {provide: USE_STORE, useValue: undefined},
  {provide: USE_EXTEND, useValue: undefined},
  {provide: FormGroup, useClass: MockFormGroup},
  {provide: LoginService, useClass: MockLoginService},
  {provide: DEFAULT_LANGUAGE, useValue: undefined},
  {provide: ChangePasswordService,
    useValue: {put(params?: any): (defer) => any {return defer => Promise.resolve({message: 'success'}); }}},
  {provide: BreadcrumbService, useValue: undefined}
];
