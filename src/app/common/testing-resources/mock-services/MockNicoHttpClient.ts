import {defer, Observable} from 'rxjs';

export class MockNicoHttpClient {
  public get(params?: any): Observable<any> {
    return defer(() => Promise.resolve({}));
  }

  public put(params?: any): Observable<any> {
    return defer(() => Promise.resolve({}));
  }

  public post(params?: any): Observable<any> {
    return defer(() => Promise.resolve({}));
  }
}
