import {AuthenticatableInterface} from '../../../../system/interfaces';

export class MockNicoSessionService {
  protected user = {
    name: 'Test User',
    email: 'pallavi.ghimire@ensue.us',
    getName: () => 'Test User'
  };
  public setUser(user: AuthenticatableInterface): void {
    this.user = user as any;
  }


  public getUser(): AuthenticatableInterface {
    return this.user as any;
  }


  public clearAuth(): void {
    this.user = null;
  }

  public setAuthToken(value): void {

  }

  public getAuthToken(): string {
    return 'mocktoken';
  }
}
