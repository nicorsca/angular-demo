import {defer, Observable} from 'rxjs';
import {DepartmentModel} from '@common/models';

export class MockDepartmentService {
  mockData = [{
    created_at: '2021-07-15T16:25:17.000000Z',
    description: 'Finance Department',
    employees_count: 0,
    hod: null,
    id: 4,
    status: 1,
    title: 'Finance department',
    updated_at: '2021-07-16T07:39:38.000000Z',
  }] as unknown as DepartmentModel;

  public post(data: any, params?: any): Observable<any> {
    if (data) {
      return defer(() => Promise.resolve(this.mockData[0]));
    }
    else {
      return defer(() => Promise.resolve(
        {error:
            {message: 'Test', status: '417'}
        }));
    }
  }
  public saveDepartment(data: any, params?: any): Observable<any> {
    if (data) {
      return defer(() => Promise.resolve(this.mockData[0]));
    }
    else {
      return defer(() => Promise.resolve(
        {error:
            {message: 'Test', status: '417'}
        }));
    }
  }

  public watchUserChange(): Observable<any> {
    return defer(() => Promise.resolve({
    }));
  }

  public setUserDetailChange(): void {
  }


  public get(): Observable<any>{
    return defer(() => Promise.resolve(this.mockData));
  }
  public put(params?: any): Observable<any> {
    return defer(() => Promise.resolve( {
      user: 'present'
    }));
  }
  public delete(occasion: any): Observable<any>{
    if (occasion.employees_count <= 0) {
      return defer(() => Promise.resolve({
        body: 'success'
      }));
    }
    else{
      return defer(() => Promise.resolve({
        error: {
          code: 'err_employee_exists_exception',
          code_text: 'Failed to perform operation, employees exists in this department',
          message: 'Employee exists in the department. Unable to perform this operation.'
        }
      }));
    }
  }
  public getDetail(occasion: any): Observable<any>{
    return defer( () => Promise.resolve(this.mockData));
  }
  public changeHOD(occasion: any, hodId: any): Observable<any>{
    if (occasion.employees_count <= 0) {
      return defer(() => Promise.resolve({
        body: 'success'
      }));
    }
    else{
      return defer(() => Promise.resolve({
      error: {
          code: 'err_employee_exists_exception',
          code_text: 'Please assign HOD to your department first.',
          message: 'Employee exists in the department. Unable to perform this operation.'
      }
      }));
    }
  }
  public changeStatus(occasion: any): Observable<any>{
    return defer( () => Promise.resolve({
      status: 2
    }));
  }
}
