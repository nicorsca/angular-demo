import {defer, Observable} from 'rxjs';
import {PaginatedNicoCollection} from '@system/utilities';

export class MockUserDetailInfoService {
  public get(params?: any): Observable<any> {
    return defer(() => Promise.resolve({test: 'test'} as unknown as PaginatedNicoCollection<any>));
  }
}
