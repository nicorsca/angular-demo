import {defer, Observable} from 'rxjs';

export class MockPasswordEmailService {
  public post(params?: any): Observable<any> {
    if (params.email === 'a@a.com') {
      return defer(() => Promise.resolve({
        status: '200'
      }));
    } else {
      return defer(() => Promise.reject({}));
    }
  }
}
