import {of} from 'rxjs';
export class MockMatDialog {

  public open(): any {
    return {
      afterClosed: () => of(true)
    };
  }

  public afterClosed(): any{
      return{
        afterClosed: () => of(true)
      };
  }
  public close(params?: any): any {
    return;
  }
}
