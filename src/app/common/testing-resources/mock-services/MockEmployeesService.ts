import {defer, Observable} from 'rxjs';
import {DepartmentModel} from '@common/models';
import {PaginatedNicoCollection} from '@system/utilities';
import {EmployeeHistoriesModel} from '@common/models/employee-histories.model';

export class MockEmployeesService {
  mockData = [{
    created_at: '2021-07-15T16:25:17.000000Z',
    description: 'Finance Department',
    employees_count: 0,
    hod: null,
    id: 4,
    status: 1,
    title: 'Finance department',
    updated_at: '2021-07-16T07:39:38.000000Z',
  }] as unknown as DepartmentModel;

  public post(body: any, params?: any): Observable<any> {
    if (body.pan_number.length < 4){
      return defer(() => Promise.resolve({error: {message: 'Test', status: '417'}}));
    }
    else {
      this.mockData.push(body);
      return defer(() => Promise.resolve(this.mockData[0]));
    }
  }

  public get(): Observable<any>{
    return defer(() => Promise.resolve(this.mockData));
  }
  public put(params?: any): Observable<any> {
    return defer(() => Promise.resolve( {
      user: 'present'
    }));
  }
  public delete(occasion: any): Observable<any>{
    return defer(() => Promise.resolve({
      body: 'success'
    }));
  }
  public getDetail(occasion: any): Observable<any>{
    return defer( () => Promise.resolve(this.mockData));
  }
  public changeHOD(occasion: any, hodId: any): Observable<any>{
    return defer(() => Promise.resolve({
      body: 'success'
    }));
  }

  public watchUserChange(): Observable<any> {
    return defer(() => Promise.resolve({
    }));
  }

  public setUserDetailChange(): void {
  }

  /**
   * Contacts
   */
  public getContacts(params?: any): Observable<any> {
    if (params === 1) {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }

  public saveContact(params?: any): Observable<any> {
    if (params.status === 1) {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    } else {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    }
  }

  public putContact(params?: any): Observable<any> {
    if (params.status === 1) {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }

  public postContacts(params?: any): Observable<any> {
    if (params.status === 1) {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }

  public deleteContact(id?: any, modelId?: any): Observable<any> {
    if (id === 1) {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }

  public toggleContactStatus(id?: any, contactId?: any, params?: any): Observable<any> {
    if (id === 1) {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    } else {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    }
  }

  /**
   * Banks
   */

  public getBanks(params?: any): Observable<any> {
    if (params === 1) {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }

  public saveBank(params?: any): Observable<any> {
    if (params.status === 2) {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }

  public toggleBankStatus(params: any, bankModel: any): Observable<any> {
    if (bankModel.id === 1) {
      return defer(() => Promise.resolve({id: 1, name: 'Jane Doe', branch: 'Battisputali', account_name: 'Jane Doe', account_number: '111000222'}));
    } else {
      return defer(() => Promise.reject({}));
    }
  }

  public deleteBank(id?: any, modelId?: any): Observable<any> {
    if (id === 1) {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    } else {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    }
  }
  public postBanks(params?: any): Observable<any> {
    if (params.status === 1) {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }

  /**
   * Documents
   */

  public getDocuments(params?: any): Observable<any> {
    if (params === 1) {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }

  public saveDocument(params?: any): Observable<any> {
    if (params.status === 1) {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    } else {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    }
  }

  public toggleDocumentStatus(params: any, bankModel: any): Observable<any> {
    if (bankModel.id === 1) {
      return defer(() => Promise.resolve({id: 1, name: 'Jane Doe', branch: 'Battisputali', account_name: 'Jane Doe', account_number: '111000222'}));
    } else {
      return defer(() => Promise.reject({}));
    }
  }

  public deleteDocument(id?: any, modelId?: any): Observable<any> {
    if (id === 1) {
      return defer(() => Promise.resolve({type: 1, number: '9800000000'}));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }

  public saveEmployee(body, params): Observable<any> {
    if (body.pan_number.length < 4) {
      return defer(() => Promise.resolve({error: {message: 'Test', status: '417'}}));
    }
    else {
      this.mockData.push(body);
      return defer(() => Promise.resolve(this.mockData[0]));
    }
  }

  /**
   * Histories
   */
  public toggleEmployeeStatus(body: any, id: any, activateEmployee: boolean, params?: any): Observable<any> {
    if (body.url === 'test') {
      return defer(() => Promise.resolve({}));
    } else {
      return defer(() => Promise.reject({error: {message: 'this is an error'}}));
    }
  }

  public getHistories(params?: any): Observable<any> {
    if (params === 1) {
      return defer(() => Promise.resolve({} as unknown as PaginatedNicoCollection<EmployeeHistoriesModel>));
    } else {
      return defer(() => Promise.reject({error: {message: 'Test'}}));
    }
  }
}
