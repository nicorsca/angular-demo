import {defer, Observable} from 'rxjs';

export class MockResetPasswordService {
  public post(params?: any): Observable<any> {
    if (params.password === 'validPassword') {
      return defer(() => Promise.resolve({
        status: '200'
      }));
    } else {
      return defer (() => Promise.reject({}));
    }
  }

  public put(params?: any): Observable<any> {
    if (params.old_password === 'validPassword') {
      return defer(() => Promise.resolve({
        status: '200'
      }));
    } else {
      return defer (() => Promise.reject({}));
    }
  }
}
