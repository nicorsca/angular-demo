import {defer, Observable} from 'rxjs';

export class MockUserInfoService {
  public get(params?: any): Observable<any> {
    return defer(() => Promise.resolve({first_name: 'Jane'}));
  }
}
