import {defer, Observable} from 'rxjs';
import {DepartmentModel} from '@common/models';
import {AddressModel} from '@common/models/address.model';

export class MockAddressService {
  mockData = [{
    id : 1,
    country: 'Nepal',
    state: 3,
    city: 'ktm',
    street: 'bijeshwori',
    zip_code: 44600,
    type: 2,
    created_at: '',
    updated_at: ''
  }] as unknown as DepartmentModel;

  public post(body: any, params?: any): Observable<any> {
    this.mockData.push(body);
    return defer(() => Promise.resolve(this.mockData[0]));
  }

  public saveAddress(body: any, params?: any): Observable<any>{
    if (body.type) {
      return defer(() => Promise.resolve({
        body: this.mockData
      }));
    }
    else{
      return defer(() => Promise.resolve({
        error: {message: 'Test', status: '417'}
      }));
    }
  }
  public getAddress(): Observable<any>{
    return defer(() => Promise.resolve(this.mockData));
  }
  public put(params?: any): Observable<any> {
    return defer(() => Promise.resolve( {
      user: 'present'
    }));
  }
  public delete(occasion: any): Observable<any>{
    return defer(() => Promise.resolve({
      body: 'success'
    }));
  }
  public postAddress(id: string, body: AddressModel): Observable<any> {
    if (body.type) {
      return defer(() => Promise.resolve({
        body: this.mockData
      }));
    }
    else{
      return defer(() => Promise.resolve({
        error: {message: 'Test', status: '417'}
      }));
    }
  }
}
