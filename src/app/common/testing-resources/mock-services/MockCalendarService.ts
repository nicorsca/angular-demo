import {defer, Observable} from 'rxjs';

export class MockCalendarService {
  public getCalendarDetails(params: any, empId: any): Observable<any> {
    return defer(() => Promise.reject({error: {message: 'undefined'}}));
  }
}
