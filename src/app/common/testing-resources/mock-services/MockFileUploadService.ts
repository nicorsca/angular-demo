import {defer, Observable} from 'rxjs';

export class MockFileUploadService {
  public get(params?: any): Observable<any> {
    return defer(() => Promise.resolve({path: 'test.png', thumb_path: 'test_thumb.png'}));
  }
}
