import {defer, Observable} from 'rxjs';
import {AllocatedLeaveModel} from '@common/models/allocated-leaves.model';
import {AttendanceDetailModel} from "@common/models";
import {AttendanceReportModel} from "@common/models/attendance-report.model";
import {PaginatedNicoCollection} from "@system/utilities";

export class MockAdminReportService {
  mockData = {
    attend_date: null,
    avatar: 'https://static.toiimg.com/thumb/resizemode-4,msid-76729750,imgsize-249247,width-720/76729750.jpg',
    break_time: null,
    check_in: null,
    check_out: null,
    code: 'EN202101',
    email: 'manish.shakya@ensue.us',
    employee_id: null,
    id: 1,
    leave_in: null,
    leave_out: null,
    leave_time: null,
    name: 'Manish Ratna Shakya',
    office_time: null,
    position: 'Junior frontend developer',
    work_time: null} as unknown as PaginatedNicoCollection<AttendanceReportModel>;

  public getAttendanceTodayAttendance(params?: any): Observable<PaginatedNicoCollection<AttendanceReportModel>>{
    return defer(() => Promise.resolve(this.mockData));
  }
  public getAttendanceTodaySummary(): Observable<any>{
    return defer(() => Promise.resolve(this.mockData));
  }


}
