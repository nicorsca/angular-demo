import {defer, Observable} from 'rxjs';
import {DepartmentModel} from '@common/models';
import {SocialSecurityModel} from '@common/models/social-security.model';

export class MockSocialSecurity {
  mockData = [{
    created_at: '2021-07-15T16:25:17.000000Z',
    description: 'Finance Department',
    employees_count: 0,
    hod: null,
    id: 4,
    status: 1,
    title: 'Finance department',
    updated_at: '2021-07-16T07:39:38.000000Z',
  }] as unknown as DepartmentModel;

  public postSocialSecurity(id: string, body: SocialSecurityModel, params?: any): Observable<any> {
    return defer(() => Promise.resolve({
      body: true
    }));
  }

  public get(): Observable<any>{
    return defer(() => Promise.resolve(this.mockData));
  }

}
