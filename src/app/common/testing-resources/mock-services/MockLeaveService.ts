import {defer, Observable} from 'rxjs';

export class MockLeaveService {
  public mockData = {
    avatar: undefined,
    code: undefined,
    created_at: '2021-09-17T06:54:35.000000Z',
    days: 3,
    department: undefined,
    description: 'mero khutta bacyo',
    email: undefined,
    employee: {id: 1, name: 'Manish Ratna Shakya', email: 'manish.shakya@ensue.us', avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7w-VUkvVobuC_P3TImDJalLsfOGGijkNRmA&usqp=CAU', code: 'EN200701'},
    end_at: '2021-11-23 18:14:59',
    id: '9',
    is_emergency: false,
    name: undefined,
    requested: {id: 20, name: 'Angel Ratna Roman', email: 'angel@gmail.com', avatar: null, code: 'EN202106'},
    start_at: '2021-11-20 18:15:00',
    status: 1,
    title: 'khutta bhachyo',
    type: 'sick',
    updated_at: '2021-09-17T06:54:35.000000Z',
  };

  public post(data: any, params?: any): Observable<any> {
    if (data) {
      return defer(() => Promise.resolve(this.mockData[0]));
    }
    else {
      return defer(() => Promise.resolve(
        {error:
            {message: 'Test', status: '417'}
        }));
    }
  }
  public saveRequestLeave(data: any, params?: any): Observable<any> {
    if (data) {
      return defer(() => Promise.resolve(this.mockData));
    }
    else {
      return defer(() => Promise.resolve(
        {error:
            {message: 'Test', status: '417'}
        }));
    }
  }
  public onCancellationLeave(id: number, data: any, params?: any): Observable<any> {
    if (data) {
      return defer(() => Promise.resolve(this.mockData));
    }
    else {
      return defer(() => Promise.resolve(
        {error:
            {message: 'Test', status: '417'}
        }));
    }
  }
  public onCancelDeclineLeave(model: any, data: any, state: boolean, params?: any): Observable<any> {
    if (data) {
      return defer(() => Promise.resolve({message: data}));
    }
    else {
      return defer(() => Promise.resolve(
        {error:
            {message: 'Test', status: '417'}
        }));
    }
  }

  public watchUserChange(): Observable<any> {
    return defer(() => Promise.resolve({
    }));
  }

  public setUserDetailChange(): void {
  }


  public getLeaves(): Observable<any>{
    return defer(() => Promise.resolve(this.mockData));
  }
  public put(params?: any): Observable<any> {
    return defer(() => Promise.resolve( {
      user: 'present'
    }));
  }
  public delete(occasion: any): Observable<any>{
    if (occasion.employees_count <= 0) {
      return defer(() => Promise.resolve({
        body: 'success'
      }));
    }
    else{
      return defer(() => Promise.resolve({
        error: {
          code: 'err_employee_exists_exception',
          code_text: 'Failed to perform operation, employees exists in this department',
          message: 'Employee exists in the department. Unable to perform this operation.'
        }
      }));
    }
  }
  public getDetail(occasion: any): Observable<any>{
    return defer( () => Promise.resolve(this.mockData));
  }
  public changeHOD(occasion: any, hodId: any): Observable<any>{
    if (occasion.employees_count <= 0) {
      return defer(() => Promise.resolve({
        body: 'success'
      }));
    }
    else{
      return defer(() => Promise.resolve({
        error: {
          code: 'err_employee_exists_exception',
          code_text: 'Please assign HOD to your department first.',
          message: 'Employee exists in the department. Unable to perform this operation.'
        }
      }));
    }
  }
  public changeStatus(occasion: any): Observable<any>{
    return defer( () => Promise.resolve({
      status: 2
    }));
  }
}
