export interface FilterQueryParamInterface {
  keyword?: string;
  date?: string;
  page?: number;
  per_page?: number;
  sort_by?: any;
  sort_order?: 'asc' | 'desc';
  on_leave?: '' | number;
  status?: number | boolean | null | string;
  [key: string]: any;
}
