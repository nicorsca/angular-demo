export enum AppRouteConstants{
  EMPLOYEE_LEAVE = '/manage/leaves/employees-leave',
  LEAVE = '/manage/leaves'
}
