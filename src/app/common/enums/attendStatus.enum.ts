export enum AttendStatusEnum {
  Present = 'P',
  Leave = 'L',
  Absent = 'A',
  HalfLeave = 'HL',
  Weekend = 'W',
  Holiday = 'H',
}
