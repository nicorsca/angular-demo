export enum EmployeeView {
  TableView = 'table-view',
  CardView = 'card-view',
}

export enum EmployeeContact {
  Personal = 1,
  Home = 2,
  Office = 3
}

export enum EmployeeDocument {
  Png = 'image/png',
  Pdf = 'application/pdf',
  OctetStream = 'application/octet-stream',
  Jpeg = 'image/jpeg',
  Jpg = 'image/jpeg',
  Doc = 'application/vnd.oasis.opendocument.presentation',
  Xls = 'application/vnd.ms-excel',
  Xlsx = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  Gif = 'image/gif',
  Svg = 'image/svg+xml',
  Docx = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
}
