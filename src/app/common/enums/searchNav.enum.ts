export enum SearchNavEnum {
  Ascending = 'asc',
  Descending = 'desc',
  OnLeave = 1,
  All = 'all',
  AttendDate = 'attend_date',
  CheckIn = 'check_in',
}
