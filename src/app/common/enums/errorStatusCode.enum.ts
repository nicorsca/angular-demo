export enum ErrorStatusCodeEnum {
  MultipleChoice = 300,
  Unauthorized = 401
}
