export enum EmployeeRoleEnum {
  Manager = 'manager',
  Employee = 'employee'
}
