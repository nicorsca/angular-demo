import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appToggle]',
})
export class ToggleDirective {
  private shown = false;

  constructor(private el: ElementRef) {
    this.setup();
    this.el.nativeElement.setAttribute('type', 'password');
  }

  setup(): void {
    const parent = this.el.nativeElement.parentNode;
    const span = document.createElement('span');
    span.classList.add(
      'icon-btn',
      'btn-link-light',
      'btn-sm',
      'icon-btn-circular',
      'view-btn'
    );
    const italicIcon = document.createElement('i');

    italicIcon.setAttribute('class', 'font-icon far fa-eye');
    span.appendChild(italicIcon);

    span.addEventListener('click', (event) => {
      this.toggle(italicIcon);
    });
    parent.appendChild(span);
  }

  toggle(italicIcon: HTMLElement): void {
    this.shown = !this.shown;
    if (this.shown) {
      this.el.nativeElement.setAttribute('type', 'text');
      italicIcon.setAttribute('class', 'font-icon far fa-eye-slash');
    } else {
      this.el.nativeElement.setAttribute('type', 'password');
      italicIcon.setAttribute('class', 'font-icon far fa-eye');
    }
  }
}
