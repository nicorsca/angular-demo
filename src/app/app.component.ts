import {Component, Injector, OnInit} from '@angular/core';
import {AbstractBaseComponent} from './AbstractBaseComponent';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {LoaderService} from '@common/services/loader.service';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends AbstractBaseComponent implements OnInit {

  public isLoading: boolean;

  constructor(injector: Injector, translateService: TranslateService, private loader: LoaderService) {
    super(injector);
    translateService.setDefaultLang('en');
    translateService.use('en');
    this.isLoading = false;
  }

  public title = 'ensue-hr';

  public ngOnInit(): void {
    this.appService.setThemeModeFromStorage();
    this.loadSpinner();
  }

  public loadSpinner(): void {
    this.loader.isLoading.pipe(delay(0)).subscribe((loading) => {
      this.isLoading = loading;
    });
  }
}
