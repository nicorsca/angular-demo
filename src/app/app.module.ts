import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HrCommonModule} from '@common/hr-common.module';
import {SystemModule} from '@system/system.module';
import {NicoSessionService, NicoStorageService, PageTitleInterface} from '@system/services';
import {NICO_AUTH_TOKEN_PROVIDER, NICO_HTTP_ERROR_INTERCEPTOR_PROVIDER, NicoHttpClient} from '@system/http-client';
import {AuthModule} from './modules/auth/auth.module';
import {AdminModule} from './modules/admin-module/admin.module';
import {NicoAuthProvider} from './app-utils';
import {HttpErrorInterceptor, SnackbarService} from '@common/services';
import {AuthGuardService} from '@common/services/auth-guard.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialog} from '@angular/material/dialog';


const PAGE_TITLE_CONF: PageTitleInterface = {
  addOnShown: true,
  separator: '-',
  titleAddOn: 'Ensue Bot',
  titlePrefixed: true
};

export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HrCommonModule,
        AuthModule,
        AdminModule,
        SystemModule.forRoot(PAGE_TITLE_CONF),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient],
            }
        }),
        MatProgressSpinnerModule
    ],
  providers: [NicoHttpClient,
    {provide: NICO_AUTH_TOKEN_PROVIDER, deps: [NicoSessionService], useClass: NicoAuthProvider},
    {provide: NICO_HTTP_ERROR_INTERCEPTOR_PROVIDER, deps: [Router, NicoSessionService, NicoStorageService, MatDialog, SnackbarService], useClass: HttpErrorInterceptor},
    AuthGuardService
  ],
  bootstrap: [AppComponent],
})

export class AppModule {
}
