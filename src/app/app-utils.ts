import {NicoAuthorizationProviderInterface, NicoDefaultRequestOptionsInterface} from '@system/http-client';
import {NicoSessionService} from '@system/services';
import {Injectable} from '@angular/core';
import {HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable()
export class NicoAuthProvider implements NicoAuthorizationProviderInterface {

  constructor(protected nicoSession: NicoSessionService) {
  }

  getAuthToken(): string {
    return this.nicoSession.getAuthToken();
  }

  useAuthTokenForRequest(): boolean {
    return true;
  }

  public setAuthToken(str: string): void {
    this.nicoSession.setAuthToken(str);
  }

}

export class NicoRequestOptions implements NicoDefaultRequestOptionsInterface {
  getDefaultHeaders(): HttpHeaders {
    return new HttpHeaders();
  }

  getDefaultParams(): HttpParams {
    return new HttpParams();
  }

}
