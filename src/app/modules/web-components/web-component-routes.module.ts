import {Routes} from '@angular/router';
import {WebComponentsComponent} from './web-components.component';
import {WebListAvatarComponent} from './components/web-list-avatar/web-list-avatar.component';
import {WebListPillBadgeComponent} from './components/web-list-pill-badge/web-list-pill-badge.component';
import {WebListButtonsComponent} from './components/web-list-buttons/web-list-buttons.component';
import {WebListCircularIconComponent} from './components/web-list-circular-icon/web-list-circular-icon.component';
import {WebListDrawerComponent} from './components/web-list-drawer/web-list-drawer.component';
import {WebListDropdownComponent} from './components/web-list-dropdown/web-list-dropdown.component';
import {WebListEmptyStateComponent} from './components/web-list-empty-state/web-list-empty-state.component';
import {WebListFormsComponent} from './components/web-list-forms/web-list-forms.component';
import {WebListFlexTableComponent} from './components/web-list-flex-table/web-list-flex-table.component';
import {WebListGridComponent} from './components/web-list-grid/web-list-grid.component';
import {WebListInputComponent} from './components/web-list-input/web-list-input.component';
import {WebListModalComponent} from './components/web-list-modal/web-list-modal.component';
import {WebListNotificationRowComponent} from './components/web-list-notification-row/web-list-notification-row.component';
import {WebListSkeletonComponent} from './components/web-list-skeleton/web-list-skeleton.component';
import {WebListTabsComponent} from './components/web-list-tabs/web-list-tabs.component';
import {WebListTypographyComponent} from './components/web-list-typography/web-list-typography.component';
import {WebListSwitchComponent} from './components/web-list-switch/web-list-switch.component';
import {WebListSnackbarComponent} from './components/web-list-snackbar/web-list-snackbar.component';
import {WebListPaginationComponent} from './components/web-list-pagination/web-list-pagination.component';
import {WebListChartsComponent} from './components/web-list-charts/web-list-charts.component';

export const WEB_COMPONENT_ROUTES: Routes = [
  {
    path: '',
    component: WebComponentsComponent,
    children: [
      {
        path: '',
        redirectTo: 'avatar',
        pathMatch: 'full'
      },
      {
        path: 'avatar',
        component: WebListAvatarComponent
      },
      {
        path: 'badge-pills',
        component: WebListPillBadgeComponent
      },
      {
        path: 'buttons',
        component: WebListButtonsComponent
      },
      {
        path: 'charts',
        component: WebListChartsComponent
      },
      {
        path: 'circular-icon',
        component: WebListCircularIconComponent
      },
      {
        path: 'drawer',
        component: WebListDrawerComponent
      },
      {
        path: 'dropdown',
        component: WebListDropdownComponent
      },
      {
        path: 'empty-state',
        component: WebListEmptyStateComponent
      },
      {
        path: 'form',
        component: WebListFormsComponent
      },
      {
        path: 'flex-table',
        component: WebListFlexTableComponent
      },
      {
        path: 'grid',
        component: WebListGridComponent
      },
      {
        path: 'input',
        component: WebListInputComponent
      },
      {
        path: 'modal',
        component: WebListModalComponent
      },
      {
        path: 'notification',
        component: WebListNotificationRowComponent
      },
      {
        path: 'pagination',
        component: WebListPaginationComponent
      },
      {
        path: 'buttons',
        component: WebListButtonsComponent
      },
      {
        path: 'skeleton',
        component: WebListSkeletonComponent
      },
      {
        path: 'snackbar',
        component: WebListSnackbarComponent,
      },
      {
        path: 'switch',
        component: WebListSwitchComponent
      },
      {
        path: 'tab',
        component: WebListTabsComponent
      },
      {
        path: 'typography',
        component: WebListTypographyComponent
      },

    ]
  }
];
