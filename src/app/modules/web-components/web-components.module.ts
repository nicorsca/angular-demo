import {NgModule} from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';
import {WebComponentsComponent} from './web-components.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NicoComponentsModule} from '@system/components';
import {TranslateModule} from '@ngx-translate/core';
import {WEB_COMPONENT_ROUTES} from './web-component-routes.module';
import {WebListGridComponent} from './components/web-list-grid/web-list-grid.component';
import {WebListButtonsComponent} from './components/web-list-buttons/web-list-buttons.component';
import {WebListTypographyComponent} from './components/web-list-typography/web-list-typography.component';
import {WebListInputComponent} from './components/web-list-input/web-list-input.component';
import {WebListFormsComponent} from './components/web-list-forms/web-list-forms.component';
import {WebListSnackbarComponent} from './components/web-list-snackbar/web-list-snackbar.component';
import {WebListTabsComponent} from './components/web-list-tabs/web-list-tabs.component';
import {WebListFlexTableComponent} from './components/web-list-flex-table/web-list-flex-table.component';
import {WebListDropdownComponent} from './components/web-list-dropdown/web-list-dropdown.component';
import {WebListAvatarComponent} from './components/web-list-avatar/web-list-avatar.component';
import {WebListSwitchComponent} from './components/web-list-switch/web-list-switch.component';
import {WebListDrawerComponent} from './components/web-list-drawer/web-list-drawer.component';
import {WebListModalComponent} from './components/web-list-modal/web-list-modal.component';
import {WebListCircularIconComponent} from './components/web-list-circular-icon/web-list-circular-icon.component';
import {WebListEmptyStateComponent} from './components/web-list-empty-state/web-list-empty-state.component';
import {WebListNotificationRowComponent} from './components/web-list-notification-row/web-list-notification-row.component';
import {WebListPillBadgeComponent} from './components/web-list-pill-badge/web-list-pill-badge.component';
import {WebListSkeletonComponent} from './components/web-list-skeleton/web-list-skeleton.component';
import {WebListPaginationComponent} from './components/web-list-pagination/web-list-pagination.component';
import {WebListTestChildComponent} from './components/web-list-drawer/web-list-test-child.component';
import {WebListChartsComponent} from './components/web-list-charts/web-list-charts.component';
import {NgxEchartsModule} from 'ngx-echarts';

@NgModule({
  declarations: [
    WebComponentsComponent,
    WebListTypographyComponent,
    WebListButtonsComponent,
    WebListInputComponent,
    WebListFormsComponent,
    WebListSnackbarComponent,
    WebListTabsComponent,
    WebListFlexTableComponent,
    WebListDropdownComponent,
    WebListGridComponent,
    WebListAvatarComponent,
    WebListSwitchComponent,
    WebListDrawerComponent,
    WebListModalComponent,
    WebListCircularIconComponent,
    WebListEmptyStateComponent,
    WebListNotificationRowComponent,
    WebListPillBadgeComponent,
    WebListSkeletonComponent,
    WebListPaginationComponent,
    WebListTestChildComponent,
    WebListChartsComponent
  ],
    imports: [
        HrCommonModule,
        CommonModule,
        RouterModule,
        NicoComponentsModule,
        RouterModule.forChild(WEB_COMPONENT_ROUTES),
        TranslateModule.forChild({
            extend: true
        }),
    ],
  providers: [],
})
export class WebComponentsModule {
}
