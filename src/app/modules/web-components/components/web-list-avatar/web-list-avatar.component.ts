import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-web-list-avatar',
  templateUrl: './web-list-avatar.component.html',
})
export class WebListAvatarComponent implements OnInit {

  public tableData = [
    {
      id: '60c8acde5c0d47b2ef36b0ba',
      isActive: true,
      picture: 'http://placehold.it/32x32',
      name: 'Woods Holder',
      company: 'BRAINQUIL',
      email: 'woodsholder@brainquil.com',
      phone: '+1 (973) 582-2191'
    },
    {
      id: '60c8acdebb92f283537c8eb7',
      isActive: false,
      picture: 'http://placehold.it/32x32',
      name: 'Carissa Mcguire',
      company: 'COMSTAR',
      email: 'carissamcguire@comstar.com',
      phone: '+1 (993) 414-3468'
    },
    {
      id: '60c8acded9f50c8234ebd541',
      isActive: true,
      picture: 'http://placehold.it/32x32',
      name: 'Bertie Mendoza',
      company: 'UNISURE',
      email: 'bertiemendoza@unisure.com',
      phone: '+1 (957) 483-3536'
    },
    {
      id: '60c8acde6557639c29e1e977',
      isActive: true,
      picture: 'http://placehold.it/32x32',
      name: 'Anthony Solis',
      company: 'XELEGYL',
      email: 'anthonysolis@xelegyl.com',
      phone: '+1 (965) 416-3385'
    },
    {
      id: '60c8acde20110037e56f4849',
      isActive: false,
      picture: 'http://placehold.it/32x32',
      name: 'Holly Slater',
      company: 'ZENSOR',
      email: 'hollyslater@zensor.com',
      phone: '+1 (803) 412-2862'
    },
    {
      id: '60c8acdeadd6542d534cdefa',
      isActive: true,
      picture: 'http://placehold.it/32x32',
      name: 'Floyd Williams',
      company: 'EGYPTO',
      email: 'floydwilliams@egypto.com',
      phone: '+1 (956) 425-2506'
    },
    {
      id: '60c8acde32a0fb8095ccdc88',
      isActive: false,
      picture: 'http://placehold.it/32x32',
      name: 'Fanny Benjamin',
      company: 'TECHMANIA',
      email: 'fannybenjamin@techmania.com',
      phone: '+1 (994) 472-2600'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
