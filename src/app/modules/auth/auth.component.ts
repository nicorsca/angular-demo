import {Component, OnInit} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {RouteAnimations} from '@common/animations/app.animations';

@Component({
  selector: 'app-auth',
  template: `
    <main class="auth-main-container" [@routeAnimations]="prepareRoute(outlet)">
      <router-outlet #outlet="outlet"></router-outlet>
    </main>
  `,
  animations: [RouteAnimations]
})
export class AuthComponent implements OnInit {

  constructor() {
  }

  public ngOnInit(): void {
  }

  public prepareRoute(outlet: RouterOutlet): any {
    return outlet && outlet.activatedRouteData;
  }
}
