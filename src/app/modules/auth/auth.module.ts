import { NgModule } from '@angular/core';
import {LoginComponent} from './pages/login/login.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import {HrCommonModule} from '@common/hr-common.module';
import { LogoutComponent } from './pages/logout/logout.component';
import {AuthRoutesModule} from './auth-routes.module';
import { CallbackComponent } from './pages/callback/callback.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { PasswordResetConfirmComponent } from './pages/password-reset-confirm/password-reset-confirm.component';
import {AuthComponent} from './auth.component';
import { ChangePasswordFirstloginComponent } from './pages/change-password-firstlogin/change-password-firstlogin.component';

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    ForgotPasswordComponent,
    LogoutComponent,
    CallbackComponent,
    ResetPasswordComponent,
    PasswordResetConfirmComponent,
    ChangePasswordFirstloginComponent,
  ],
  imports: [
    HrCommonModule,
    AuthRoutesModule,
  ],
  providers: [],
})
export class AuthModule { }
