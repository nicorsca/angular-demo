import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserModel} from '@common/models';
import {LoginService} from '@common/services/login.service';
import {Subscription} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {SettingService} from '@common/services/settings.service';
import {NicoStorageService} from '@system/services';
import {UserDetailService} from '@common/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Form
   */
  public loginFormGroup: FormGroup;

  public loginFormId = 'login-form';

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * Environment
   */
  public env: any;

  /**
   * Subscriptions
   */
  public loginSubscription: Subscription;
  public settingInfoSubscription: Subscription;

  public isLoading = false;

  /**
   * Constructor
   */
  public constructor(
    injector: Injector,
    public formBuilder: FormBuilder,
    public loginService: LoginService,
    public settingService: SettingService,
    public nicoStorageService: NicoStorageService,
    public userDetailService: UserDetailService
  ) {
    super(injector);
    this.env = environment;
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_AUTH.MOD_LOGIN.HTML_TITLE');
    super.onComponentReady();
    this.createForm();
  }

  /**
   * To create the login form
   */
  public createForm(): void {
    this.loginFormGroup = this.formBuilder.group({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  /**
   * To log user in and set the localstorage with user data
   */
  public onFormSubmit(): void {
    if (!this.loginFormGroup.valid) {
      Object.keys(this.loginFormGroup.controls).forEach(field => {
        this.loginFormGroup.get(field).markAsTouched({onlySelf: true});
      });
      return;
    }
    this.isLoading = true;
    this.loginSubscription = this.loginService.post(this.loginFormGroup.getRawValue()).subscribe((userInfo: UserModel) => {
      this.setUserToSessionAndRedirect(userInfo);
      this.setMyDetails();
      this.setSettingInfo();
      this.isLoading = false;
    }, err => {
      this.onFormSubmissionError(err, this.loginFormId);
      this.isLoading = false;
    });
  }

  /**
   * To log the user in and set user to session
   */
  public setUserToSessionAndRedirect(userInfo: UserModel): void {
    this.session.setUser(userInfo);
    const settings = {};
    this.settingService.get().subscribe(response => {
      for (const data of response.all()){
        settings[data.key] = data.value;
      }
      this.nicoStorageService.setItem('settings', JSON.stringify(settings));
    });
    this.router.navigate(['']);
  }

  public setSettingInfo(): void {
    this.settingInfoSubscription = this.settingService.get().subscribe(response => {
      const settings = {};
      for (const setting of response.all()) {
        settings[setting.key] = setting.value;
      }
      this.nicoStorageService.setItem('settings', JSON.stringify(settings));
    });
  }

  public setMyDetails(): void{
    this.userDetailService.get().subscribe(model => {
      this.nicoStorageService.setItem('user_details', JSON.stringify(model));
    });
  }
  /**
   * Destroy Method
   */
  public ngOnDestroy(): void {
    if (this.loginSubscription != null) {
      this.loginSubscription.unsubscribe();
    }
  }
}
