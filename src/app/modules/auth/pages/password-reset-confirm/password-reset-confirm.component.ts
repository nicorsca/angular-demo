import {Component, Injector, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';

@Component({
  selector: 'app-password-reset-confirm',
  templateUrl: './password-reset-confirm.component.html',
})

export class PasswordResetConfirmComponent extends AbstractBaseComponent implements OnInit {

  /**
   * Page Title
   */
  public pageTitle = '';

  constructor(protected injector: Injector) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_AUTH.MOD_PASSWORD_RESET_CONFIRM.HTML_TITLE');
    super.onComponentReady();
  }

}
