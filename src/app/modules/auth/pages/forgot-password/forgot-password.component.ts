import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {PasswordEmailService} from '@common/services/password-email.service';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
})
export class ForgotPasswordComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Form
   */
  public formGroup: FormGroup;

  public forgotPasswordFormId = 'forgot-password-form';

  /**
   * Subscription
   */
  public forgotPassSubscription: Subscription;

  public isLoading = false;

  public isEmailSent = false;

  constructor(private formBuilder: FormBuilder, public passEmailService: PasswordEmailService, protected injector: Injector) {
    super(injector);
  }

  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_AUTH.MOD_FORGOT_PASSWORD.HTML_TITLE');
    super.onComponentReady();
    this.createForm();
  }

  /**
   * To create the login form
   */
  public createForm(): void {
    this.formGroup = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email])
    });
  }

  /**
   * To send email reset link
   */
  public sendResetPasswordLink(): void {
    if (!this.formGroup.valid) {
      Object.keys(this.formGroup.controls).forEach(field => {
        this.formGroup.get(field).markAsTouched({onlySelf: true});
      });
      return;
    }
    this.isLoading = true;
    this.forgotPassSubscription = this.passEmailService.post(this.formGroup.getRawValue()).subscribe(resetPass => {
      this.isLoading = false;
      this.showSuccessSnackBar(this.translateService.instant('MOD_AUTH.MOD_FORGOT_PASSWORD.SUCCESS_MESSAGE_PASSWORD_RESET'));
      this.isEmailSent = true;
      // this.router.navigate(['auth/login']);
    }, error => {
      this.isLoading = false;
      this.onFormSubmissionError(error, this.forgotPasswordFormId);
    });
  }

  public ngOnDestroy(): void {
    if (this.forgotPassSubscription != null) {
      this.forgotPassSubscription.unsubscribe();
    }
  }

}
