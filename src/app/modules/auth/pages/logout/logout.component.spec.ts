import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';
import { LogoutComponent } from './logout.component';
import {throwError} from 'rxjs';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogoutComponent ],
      providers: [
        ...MOCK_PROVIDERS
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create Logout Component', () => {
    expect(component).toBeTruthy();
  });

  it ('should log the user out', () => {
    spyOn(component, 'logoutUserAndClearAuth');
    component.onComponentReady();
    expect(component.logoutUserAndClearAuth).toHaveBeenCalled();
  });

  it('should not show error snack bar in case of error', fakeAsync(async () => {
    // @ts-ignore
    spyOn(component, 'showErrorSnackBar').and.returnValue(throwError({error: {status: 417}}));
    component.logoutUserAndClearAuth();
    expect(component.showErrorSnackBar).not.toHaveBeenCalled();
    flush();
  }));
});
