import {ComponentFixture, fakeAsync, flush, TestBed, tick} from '@angular/core/testing';

import { CallbackComponent } from './callback.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {SettingService} from '@common/services/settings.service';
import {MockSettingService} from '@common/testing-resources/mock-services';

describe('CallbackComponent', () => {
  let component: CallbackComponent;
  let fixture: ComponentFixture<CallbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallbackComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: SettingService, useClass: MockSettingService}
      ],
      imports: [
        RouterTestingModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CallbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create callback component', () => {
    expect(component).toBeTruthy();
  });

  it('should call route activation function', () => {
    spyOn(component, 'extractToken');
    component.onComponentReady();
    expect(component.extractToken).toHaveBeenCalled();
  });

  it('should redirect to dashboard in case of success', fakeAsync(async () => {
    // @ts-ignore
    spyOn(component, 'showErrorSnackBar').and.returnValue({});
    await component.extractToken();
    await component.setSettingInfo();
    fixture.whenStable();
    expect(component.showErrorSnackBar).not.toHaveBeenCalled();
    flush();
  }));

  // TODO: Error case, because there is no public function that can be spied upon.

});
