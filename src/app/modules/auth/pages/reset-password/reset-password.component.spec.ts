import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import { ResetPasswordComponent } from './reset-password.component';

import {ActivatedRoute} from '@angular/router';
import {TranslateModule
} from '@ngx-translate/core';
import {of} from 'rxjs';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {MatDialogRef} from '@angular/material/dialog';
import {ResetPasswordService} from '@common/services';
import {MockResetPasswordService} from '@common/testing-resources/mock-services/MockResetPasswordService';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('ResetPasswordComponent', () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;

  const mockActivatedRoute = {
    queryParams: of({ email: 'test' })
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResetPasswordComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: ResetPasswordService, useClass: MockResetPasswordService},
        {provide: MatDialogRef, useValue: {}},
        {provide: ActivatedRoute, useValue: {snapshot: {queryParams: {page: '1'}}, queryParams: of({email: '', token: ''})}},
      ],
      imports: [TranslateModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create Reset Password Component', () => {
    expect(component).toBeTruthy();
  });

  it('should call set token and email', () => {
    spyOn(component, 'setTokenAndEmail');
    component.ngOnInit();
    expect(component.setTokenAndEmail).toHaveBeenCalled();
  });

  it('should create form', () => {
    spyOn(component, 'createForm');
    component.onComponentReady();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should not show error message in case of success', fakeAsync(async () => {
    spyOn(component, 'onFormSubmissionError').and.returnValue();
    component.resetPassword();
    expect(component.onFormSubmissionError).not.toHaveBeenCalled();
  }));

  it('should set page title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_AUTH.MOD_RESET_PASSWORD.HTML_TITLE');
  });

  it('should check initial form values', () => {
    const loginFormGroup = component.formGroup;
    const loginFormValues = {
      password: '',
      password_confirmation: '',
      email: '',
      token: ''
    };
    expect(loginFormGroup.value).toEqual(loginFormValues);
  });


  it('should disable submission until conditions are met', () => {
    component.createForm();
    const userNameValueFormGroup = component.formGroup.get('password');
    expect(userNameValueFormGroup.value).toEqual('');
    expect(userNameValueFormGroup.errors.required).toBeTruthy();
  });

  // it('should activate button after conditions are met', () => {
  //   component.onComponentReady();
  //   const button = fixture.debugElement.nativeElement.querySelectorAll('button');
  //   expect(button.getAttribute('disabled')).toBeDefined();
  // });

  it('should redirect to success page on success', fakeAsync(async () => {
    spyOn(component, 'onFormSubmissionError');
    component.onComponentReady();
    component.formGroup.setValue({password: 'test', password_confirmation: 'test', email: 'a@a.com', token: 'test'});
    await component.resetPassword();
    await fixture.whenStable();
    expect(component.onFormSubmissionError).not.toHaveBeenCalled();
  }));

});
