import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {LogoutComponent} from './pages/logout/logout.component';
import {CallbackComponent} from './pages/callback/callback.component';
import {ForgotPasswordComponent} from './pages/forgot-password/forgot-password.component';
import {ResetPasswordComponent} from './pages/reset-password/reset-password.component';
import {PasswordResetConfirmComponent} from './pages/password-reset-confirm/password-reset-confirm.component';
import {AuthComponent} from './auth.component';
import {ChangePasswordFirstloginComponent} from './pages/change-password-firstlogin/change-password-firstlogin.component';
import {AuthGuardService, HttpErrorInterceptor} from '@common/services';

/**
 * App Routes
 */
const routes: Routes = [
  {
    path: '', component: AuthComponent, children: [
      {path: 'login', component: LoginComponent, canActivate: [AuthGuardService]},
      {path: 'logout', component: LogoutComponent},
      {path: 'callback', component: CallbackComponent, canActivate: [AuthGuardService]},
      {path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [AuthGuardService]},
      {path: 'reset-password', component: ResetPasswordComponent, canActivate: [AuthGuardService]},
      {path: 'reset-password-confirmation', component: PasswordResetConfirmComponent, canActivate: [AuthGuardService]},
      {path: 'change-password-first-login', component: ChangePasswordFirstloginComponent},
      {path: '', redirectTo: 'login', pathMatch: 'full'},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutesModule {

}
