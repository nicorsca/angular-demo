import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AdminModuleComponent} from './admin-module.component';
import {WebComponentsComponent} from '../web-components/web-components.component';


@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '', component: AdminModuleComponent, children: [
        {
          path: 'dashboard',
          loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule),
          data: {breadcrumb: {label: 'Dashboard'}}
        },
        {
          path: 'account',
          loadChildren: () => import('./pages/user-account/user-account.module').then(m => m.UserAccountModule),
          data: {breadcrumb: {label: 'Account'}}
        },
        {
          path: 'departments',
          loadChildren: () => import('./pages/departments/departments.module').then(m => m.DepartmentsModule),
          data: {breadcrumb: {label: 'Departments', alias: 'department'}}
        },
        {
          path: 'employees',
          loadChildren: () => import('./pages/employees/employees.module').then(m => m.EmployeesModule),
          data: {breadcrumb: {label: 'Employees', alias: 'employee'}}
        },
        {
          path: 'my-leaves',
          loadChildren: () => import('./pages/personal-leave/personal-leave.module').then(m => m.PersonalLeaveModule),
          data: {breadcrumb: {label: 'My Leaves'}}
        },
        {
          path: 'admin-leaves',
          loadChildren: () => import('./pages/employee-leave/employee-leave.module').then(m => m.EmployeeLeaveModule),
          data: {breadcrumb: {label: 'Employees Leaves'}}
        },
        {
          path: 'personal-attendance',
          loadChildren: () => import('./pages/attendance/attendance.module').then(m => m.AttendanceModule),
          data: {breadcrumb: {label: 'Personal'}}
        },
        {
          path: 'admin-attendance',
          loadChildren: () => import('./pages/admin-attendance/admin-attendance.module').then(m => m.AdminAttendanceModule),
          data: {breadcrumb: {label: 'Admin'}}
        },
        {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
        {
          path: 'attendance', component: WebComponentsComponent, children: [
            {
              path: 'my-attendance', component: WebComponentsComponent, children: [
                {path: 'dashboard', component: WebComponentsComponent},
                {path: 'history', component: WebComponentsComponent},
              ]
            },
            {
              path: 'admin-view', component: WebComponentsComponent, children: [
                {path: 'dashboard', component: WebComponentsComponent},
                {path: 'history', component: WebComponentsComponent},
                {path: 'report', component: WebComponentsComponent},
              ]
            },
          ]
        },
      ]
    }
  ])]
})

export class AdminRoutesModule {
}
