import {NgModule} from '@angular/core';
import {AdminModuleComponent} from './admin-module.component';
import {AdminRoutesModule} from './admin-routes.module';
import {HrCommonModule} from '@common/hr-common.module';
import {RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    AdminModuleComponent,
  ],
  imports: [
     AdminRoutesModule,
     FormsModule,
     HrCommonModule,
     CommonModule,
     RouterModule,
    TranslateModule.forChild({
      extend: true
    }),
  ],
})
export class AdminModule { }
