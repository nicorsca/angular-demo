import {Component, Injector, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../AbstractBaseComponent';

@Component({
  selector: 'app-admin-module',
  templateUrl: './admin-module.component.html',
})
export class AdminModuleComponent extends AbstractBaseComponent implements OnInit {
  title = 'ensue-hr';

  public isSidenavPinned: boolean;

  constructor(injector: Injector) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    super.onComponentReady();
  }


  /**
   * Sidenav Collapse Event
   */
  public onSidenavCollapse($event): void {
    this.isSidenavPinned = $event;
  }
}
