import {Component, Injector, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.scss']
})
export class AttendanceComponent extends AbstractBaseComponent implements OnInit {

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * Constructor
   */
  constructor(protected injector: Injector) {
    super(injector);
  }

  /**
   * On Component ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DASHBOARD.HTML_TITLE');
    super.onComponentReady();
  }

}
