import {Component, Injector, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AttendanceCalendarModel, UserModel} from '@common/models';
import {NicoCollection, NicoUtils} from '@system/utilities';
import {DateUtility} from '@common/utilities/date-utility';
import {ChartLegendInterface} from '@common/interface/chart-legend.interface';

@Component({
  selector: 'app-chart-monthly-record',
  templateUrl: './chart-monthly-record.component.html',
  styleUrls: ['./chart-monthly-record.component.scss']
})
export class ChartMonthlyRecordComponent extends AbstractBaseComponent implements OnInit, OnChanges {

  public chartOption: any;

  /**
   * Data for chart
   */
  public xAxisData = [];

  public breakTime = [];

  public leaveTime = [];

  @Input() chartModel: NicoCollection<AttendanceCalendarModel>;

  /**
   * Chart colors
   */
  public leaveColor: string;

  public workColor: string;

  public breakColor: string;

  public holidayColor: string;

  public weekendColor: string;

  /**
   * Chart legends
   */

  public leaveLegend = this.translateService.instant('MOD_ATTENDANCE.MONTHLY_RECORD_CHART.LEAVE_TIME_LABEL');

  public workLegend = this.translateService.instant('MOD_ATTENDANCE.MONTHLY_RECORD_CHART.WORK_TIME_LABEL');

  public breakLegend = this.translateService.instant('MOD_ATTENDANCE.MONTHLY_RECORD_CHART.BREAK_TIME_LABEL');

  public holidayLegend = this.translateService.instant('MOD_ATTENDANCE.MONTHLY_RECORD_CHART.HOLIDAY_LABEL');

  public weekendLegend = this.translateService.instant('MOD_ATTENDANCE.MONTHLY_RECORD_CHART.WEEKEND_LABEL');

  /**
   * Constructor
   */
  constructor(protected injector: Injector) {
    super(injector);
    this.leaveColor = '#FBC531';
    this.workColor = '#32A321';
    this.breakColor = '#aab88a';
    this.holidayColor = '#ff5432';
    this.weekendColor = '#999999';
  }

  /**
   * On Component ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DASHBOARD.HTML_TITLE');
    this.resetChart();
  }

  /**
   * Get calendar data
   */
  public resetChart(): void {
    this.setChartData();
    this.chartOption = this.setChartOptions();
  }

  /**
   * On Changes
   */
  public ngOnChanges(changes: SimpleChanges): void {
    this.resetChart();
  }

  /**
   * Set chart data
   */
  public setChartData(): void {
    let i = 1;
    this.xAxisData = [];
    this.breakTime = [];
    this.leaveTime = [];
    if (this.chartModel) {
      this.chartModel.forEach(data => {
        this.xAxisData.push(i);
        this.breakTime.push(this.getHoursFromSeconds(data.break_time));
        if (NicoUtils.isNullOrUndefined(data.leave_time)) {
          this.leaveTime.push(0);
        } else {
          this.leaveTime.push(this.getHoursFromSeconds(data.leave_time));
        }
        i++;
      });
    }
  }

  /**
   * Set chart options
   */
  public setChartOptions(): any {
    return {
      legend: {
        show: false
      },
      grid: {
        left: 0,
        right: 0,
        containLabel: true
      },
      tooltip: {
        formatter: (point) => {
          return point.marker + this.getTime(point);
        }
      },
      xAxis: {
        data: this.xAxisData,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {
        type: 'value',
        axisLabel: {
          formatter: this.translateService.instant('MOD_ATTENDANCE.MONTHLY_RECORD_CHART.HOUR_LABEL', {hour: '{value}'})
        },
        interval: 1,
        minInterval: 0
      },
      series: [
        {
          name: this.workLegend,
          type: 'bar',
          data: this.findDayAndAssignColor(),
          stack: 'one',
          animationDelay: (idx) => idx * 10,
        },
        {
          name: this.leaveLegend,
          type: 'bar',
          stack: 'one',
          data: this.leaveTime,
          animationDelay: (idx) => idx * 10,
          itemStyle: {
            color: this.leaveColor
          }
        },
        {
          name: this.breakLegend,
          type: 'bar',
          stack: 'one',
          data: this.breakTime,
          animationDelay: (idx) => idx * 10,
          itemStyle: {
            color: this.breakColor
          }
        }
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };
  }

  /**
   * Set chart legends
   */
  public setChartLegend(): ChartLegendInterface[] {
    return [
      {marker: this.workColor, title: this.workLegend},
      {marker: this.leaveColor, title: this.leaveLegend},
      {marker: this.breakColor, title: this.breakLegend},
      {marker: this.holidayColor, title: this.holidayLegend},
      {marker: this.weekendColor, title: this.weekendLegend}
    ];
  }

  /**
   * To get hours
   */
  public getHoursFromSeconds(seconds: number): number {
    return seconds / 3600;
  }

  /**
   * To find if the given day is holiday, work day, leave, or a weekend
   */
  public findDayAndAssignColor(): any {
    const workTime = [];
    this.chartModel?.forEach(data => {
      if (data.holiday === true) {
        workTime.push({
          value: this.getHoursFromSeconds(data.work_time),
          name: this.holidayLegend,
          itemStyle: {
            color: this.holidayColor
          }
        });
      } else if (data.weekend === true) {
        workTime.push({
          value: this.getHoursFromSeconds(data.work_time),
          name: this.weekendLegend,
          itemStyle: {
            color: this.weekendColor
          }
        });
      } else {
        workTime.push({
          value: this.getHoursFromSeconds(data.work_time),
          name: this.workLegend,
          itemStyle: {
            color: this.workColor
          }
        });
      }
    });
    return workTime;
  }

  /**
   * Get time label for tooltip
   */
  public getTime(point: any): any {
    let res: any;
    if (point.value) {
      res = `${point.seriesName}: <strong style="font-weight: bolder">${DateUtility.getTimeFromHours(point.value)}</strong><br>`;
    } else {
      res = `${point.seriesName}: <strong style="font-weight: bolder">${DateUtility.getTimeFromHours(point.value)}</strong><br>`;
    }
    return res;
  }
}
