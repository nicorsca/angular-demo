import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarComponent } from './calendar.component';
import {ViewPunchInOutHistoryComponent} from '../view-punch-in-out-history/view-punch-in-out-history.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog/dialog-ref';
import {NicoCollection} from '@system/utilities';
import {AttendanceDetailModel} from '@common/models';

describe('CalendarComponent', () => {
  let component: CalendarComponent;
  let fixture: ComponentFixture<CalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ViewPunchInOutHistoryComponent],
      providers: [
        ...MOCK_PROVIDERS
      ],
      imports: [
        TranslateModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not open modal in case of empty cell click', () => {
    component.calendarModel = {} as unknown as NicoCollection<AttendanceDetailModel>;
    fixture.detectChanges();
    spyOn(component.dialog, 'open');
    const $event = {startTime: new Date()};
    component.onCellClick($event);
    expect(component.dialog.open).not.toHaveBeenCalled();
  });
});
