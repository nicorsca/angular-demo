import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { AttendanceSummaryComponent } from './attendance-summary.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AttendanceSummaryService} from '@common/services';
import {MockAttendanceService} from '@common/testing-resources/mock-services/MockAttendanceService';

describe('AttendanceSummaryComponent', () => {
  let component: AttendanceSummaryComponent;
  let fixture: ComponentFixture<AttendanceSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AttendanceSummaryComponent],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: AttendanceSummaryService, useClass: MockAttendanceService},
      ],
      imports: [
        TranslateModule,
        CommonModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('should convert time to hours', () => {
    expect(component.getHoursAndMinutes(60000)).toEqual('MOD_ATTENDANCE.ATTENDANCE_SUMMARY.HOURS_AND_MINUTES_LABEL');
  });
});
