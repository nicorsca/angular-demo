import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChartDailyAttendanceComponent } from './chart-daily-attendance.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {AttendanceCalendarService} from '@common/services/attendance-calendar.service';
import {MockCalendarService} from '@common/testing-resources/mock-services';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NicoCollection} from '@system/utilities';
import {AttendanceCalendarModel} from '@common/models';
import {NgxEchartsModule} from 'ngx-echarts';

describe('ChartDailyAttendanceComponent', () => {
  let component: ChartDailyAttendanceComponent;
  let fixture: ComponentFixture<ChartDailyAttendanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChartDailyAttendanceComponent],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: AttendanceCalendarService, useClass: MockCalendarService}
      ],
      imports: [
        TranslateModule,
        NgxEchartsModule.forRoot({
          echarts: () => import('echarts')
        })
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartDailyAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should assign color according to time', () => {
    spyOn(component, 'assignLateTiming');
    spyOn(component, 'setChartOptions').and.callThrough();
    component.chartModel = [{holiday: true, weekend: false, data: {work_time: 3600}}] as unknown as NicoCollection<AttendanceCalendarModel>;
    fixture.detectChanges();
    fixture.whenStable();
    component.setChartOptions();
    expect(component.assignLateTiming).toHaveBeenCalled();
  });

  it('should assign value to tooltip', () => {
    expect(component.formatTooltipInfo([{value: 1000, marker: '', seriesName: 'Test1'}, {value: 1000, marker: '', seriesName: 'Test2'}])).toEqual(' Test2: MOD_ATTENDANCE.ATTENDANCE_SUMMARY.HOURS_LABEL<br>  Test1: MOD_ATTENDANCE.ATTENDANCE_SUMMARY.HOURS_LABEL');
  });
});
