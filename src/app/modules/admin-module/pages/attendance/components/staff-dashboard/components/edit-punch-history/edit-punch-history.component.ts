import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AttendanceDetailModel, EmployeeContactsModel} from '@common/models';
import {AttendanceDetailService, AttendanceService} from '@common/services';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {DateUtility} from '@common/utilities/date-utility';

@Component({
  selector: 'app-edit-punch-history',
  templateUrl: './edit-punch-history.component.html',
  styleUrls: ['./edit-punch-history.component.scss']
})
export class EditPunchHistoryComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * Subscription
   */
  public editPunchHistorySubscription: Subscription;

  /**
   * Form
   */
  public editPunchHistoryForm: FormGroup;

  public editPunchHistoryId = 'contact-form';

  public switchState = false;

  public attendDay: string;

  public localAttendDay: string;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    @Inject(MAT_DIALOG_DATA) public modal: EmployeeContactsModel,
    public dialogRef: MatDialogRef<EditPunchHistoryComponent>,
    public attendanceService: AttendanceService,
    public attendanceDetailService: AttendanceDetailService,
    public formBuilder: FormBuilder) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DASHBOARD.MOD_VIEW_PUNCH_HISTORY.MOD_EDIT_PUNCH_HISTORY.EDIT_LOG_TITLE');
    super.onComponentReady();
    this.createForm();
  }

  /**
   * Create Form
   */
  public createForm(): void {
    this.editPunchHistoryForm = this.formBuilder.group({
      attend_at: new FormControl('', [Validators.required]),
      reason: new FormControl('', [Validators.required]),
    });
    if (this.modal.content) {
      this.attendDay = this.modal.content.attend_at;
      this.localAttendDay = DateUtility.utcToLocalFormat(this.modal?.content?.attend_at);
      this.editPunchHistoryForm.patchValue(this.modal?.content);
      this.editPunchHistoryForm.get('attend_at').setValue(new Date(DateUtility.utcToLocalFormat(this.modal?.content?.attend_at)));
      this.editPunchHistoryForm.get('reason').setValue('');
    }
  }

  /**
   * Display previous change time
   */
  public showPreviousChange(): string {
    return this.translateService.instant('MOD_ATTENDANCE.TIME_CHANGE_LABEL',
      {
        from: DateUtility.utcToLocalFormat(this.modal?.content?.parents[this.modal.content.parents.length - 1]?.attend_at, 'HH:mm'),
        to: DateUtility.utcToLocalFormat(this.modal?.content?.attend_at, 'HH:mm'),
        reason: this.modal?.content?.reason
      });
  }

  /**
   * On Form Submit
   */
  public onFormSubmit(): void {
    const formData = new AttendanceDetailModel();
    formData.reason = this.editPunchHistoryForm.get('reason').value;
    formData.attend_at = DateUtility.localToUTCFormat(this.editPunchHistoryForm.get('attend_at').value);
    this.editPunchHistorySubscription = this.attendanceDetailService.editTimeLog(formData, this.modal?.content?.id, this.modal?.content)
      .subscribe(response => {
        this.attendanceService.setattendanceChange();
        if (this.dialogRef.close) {
          this.dialogRef.close(response);
        }
        this.showSuccessSnackBar(this.translateService.instant('MOD_DASHBOARD.MOD_VIEW_PUNCH_HISTORY.MOD_EDIT_PUNCH_HISTORY.EDIT_SUCCESS_MESSAGE_LABEL'));
      });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.editPunchHistorySubscription != null) {
      this.editPunchHistorySubscription.unsubscribe();
    }
    this.pageTitleService.setTitle(this.translateService.instant('MOD_DASHBOARD.HTML_TITLE'));
  }
}
