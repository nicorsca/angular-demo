import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import { ChartMonthlyRecordComponent } from './chart-monthly-record.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NicoCollection} from '@system/utilities';
import {AttendanceCalendarModel} from '@common/models';
import {AttendanceCalendarService} from '@common/services/attendance-calendar.service';
import {MockCalendarService} from '@common/testing-resources/mock-services';
import {NgxEchartsModule} from 'ngx-echarts';

describe('ChartMonthlyRecordComponent', () => {
  let component: ChartMonthlyRecordComponent;
  let fixture: ComponentFixture<ChartMonthlyRecordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChartMonthlyRecordComponent],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: AttendanceCalendarService, useClass: MockCalendarService}
      ],
      imports: [
        TranslateModule,
        NgxEchartsModule.forRoot({
          echarts: () => import('echarts')
        })
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartMonthlyRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get hours from seconds', () => {
    expect(component.getHoursFromSeconds(3600)).toEqual(1);
  });

  it('should assign color according to day', () => {
    spyOn(component, 'findDayAndAssignColor');
    spyOn(component, 'setChartOptions').and.callThrough();
    component.chartModel = [{holiday: true, weekend: false, data: {work_time: 3600}}] as unknown as NicoCollection<AttendanceCalendarModel>;
    fixture.detectChanges();
    fixture.whenStable();
    component.setChartOptions();
    expect(component.findDayAndAssignColor).toHaveBeenCalled();
  });

  it('should assign value to tooltip', () => {
    fixture.whenStable();
    expect(component.getTime({seriesName: 'test', value: 1})).toEqual(`test: <strong style="font-weight: bolder">01:00</strong><br>`);
  });
});
