import {Component, Injector, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {NicoCollection, NicoUtils} from '@system/utilities';
import {DateUtility} from '@common/utilities/date-utility';
import {AttendanceCalendarModel} from '@common/models';
import {ChartLegendInterface} from '@common/interface/chart-legend.interface';
import * as moment from 'moment';

@Component({
  selector: 'app-chart-daily-attendance',
  templateUrl: './chart-daily-attendance.component.html',
  styleUrls: ['./chart-daily-attendance.component.scss']
})
export class ChartDailyAttendanceComponent extends AbstractBaseComponent implements OnInit, OnChanges {

  public chartOption: any;

  /**
   * Data for chart
   */
  public xAxisData = [];

  public checkInTime = [];

  public checkOutTime = [];

  @Input() chartModel: NicoCollection<AttendanceCalendarModel>;

  /**
   * Chart colors
   */
  public onTimeColor: string;

  public lateColor: string;

  public lineColor: string;

  /**
   * Chart legends
   */

  public checkInLegend = this.translateService.instant('MOD_ATTENDANCE.DAILY_RECORD_CHART.CHECK_IN_LEGEND');

  public checkOutLegend = this.translateService.instant('MOD_ATTENDANCE.DAILY_RECORD_CHART.CHECK_OUT_LEGEND');

  public onTimeLegend = this.translateService.instant('MOD_ATTENDANCE.DAILY_RECORD_CHART.ON_TIME_LABEL');

  public lateLegend = this.translateService.instant('MOD_ATTENDANCE.DAILY_RECORD_CHART.LATE_LABEL');

  /**
   * Constructor
   */
  constructor(protected injector: Injector) {
    super(injector);
    this.onTimeColor = '#855CF8';
    this.lateColor = '#E84118';
    this.lineColor = '#E84118';
  }

  /**
   * On Component ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DASHBOARD.HTML_TITLE');
    this.resetChart();
  }

  /**
   * Get calendar data
   */
  public resetChart(): void {
    if (this.userSettingsItems) {
      this.setChartData();
      this.chartOption = this.setChartOptions();
    }
  }

  /**
   * Set chart data
   */
  public setChartData(): void {
    let i = 1;
    this.xAxisData = [];
    this.checkInTime = [];
    this.checkOutTime = [];
    if (this.chartModel) {
      this.chartModel.forEach(data => {
        this.xAxisData.push(i);
        this.checkInTime.push(data.check_in ?
          DateUtility.getMomentObject(DateUtility.utcToLocalFormat(data.check_in)).diff(DateUtility.getMomentObject(DateUtility.utcToLocalFormat(data.check_in)).startOf('day').toISOString(), 'milliseconds')
          : data.check_in);
        this.checkOutTime.push(data.check_out ?
          DateUtility.getMomentObject(DateUtility.utcToLocalFormat(data.check_out)).diff(DateUtility.getMomentObject(DateUtility.utcToLocalFormat(data.check_out)).startOf('day').toISOString(), 'milliseconds')
          : data.check_out);
        i++;
      });
    }
  }

  /**
   * Format tooltip information
   */
  public formatTooltipInfo(point: any): any {
    const point0 = NicoUtils.isNullOrUndefined(point[0].value) ? '' : `<br> ${point[0].marker} ${point[0].seriesName}: ${this.getHoursAndMinutes(point[0].value / 1000)}`;
    const point1 = NicoUtils.isNullOrUndefined(point[1].value) ? '' : `${point[1].marker} ${point[1].seriesName}: ${this.getHoursAndMinutes(point[1].value / 1000)}`;
    return point1 + point0;
  }

  /**
   * Set chart options
   */
  public setChartOptions(): any {
    return {
      legend: {
        show: false
      },
      grid: {
        left: 0,
        right: 0,
        containLabel: true
      },
      tooltip: {
        trigger: 'axis',
        formatter: (point) => {
          return this.formatTooltipInfo(point);
        }
      },
      xAxis: {
        data: this.xAxisData,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {
        type: 'value',
        axisLabel: {
          formatter: (value) => {
            return DateUtility.setDateFormat(value, 'h:mm A');
          }
        },
        interval: 1000 * 60 * 90,
        minInterval: 1000 * 60 * 90
      },
      series: [
        {
          name: this.checkInLegend,
          type: 'line',
          smooth: true,
          data: this.assignLateTiming(this.checkInTime),
          stack: 'one',
          symbolSize: 8,
          symbol: 'circle',
          animationDelay: (idx) => idx * 10,
          lineStyle: {
            color: this.lateColor
          }
        },
        {
          name: this.checkOutLegend,
          type: 'line',
          smooth: true,
          data: this.assignLateTiming(this.checkOutTime, false),
          stack: 'one',
          symbolSize: 8,
          symbol: 'circle',
          animationDelay: (idx) => idx * 10,
          lineStyle: {
            color: this.lateColor
          }
        },
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };
  }

  /**
   * On Changes
   */
  public ngOnChanges(changes: SimpleChanges): void {
    this.resetChart();
  }

  /**
   * Set chart legends
   */
  public setChartLegend(): ChartLegendInterface[] {
    return [
      {marker: this.onTimeColor, title: this.onTimeLegend},
      {marker: this.lateColor, title: this.lateLegend},
    ];
  }

  /**
   * Assign colors according to input time
   */
  public assignLateTiming(data: any, isEntryTime = true): any {
    const chartData = [];
    let workStartTime = JSON.parse(this.userSettingsItems).employee_work_time.split('-')[0];
    let workEndTime = JSON.parse(this.userSettingsItems).employee_work_time.split('-')[1];
    workStartTime = DateUtility.utcToLocalFormat(moment(new Date(Date.UTC(0, 0, 0, workStartTime.split(':')[0], workStartTime.split(':')[1]))), 'HH:mm');
    workEndTime = DateUtility.utcToLocalFormat(moment(new Date(Date.UTC(0, 0, 0, workEndTime.split(':')[0], workEndTime.split(':')[1]))), 'HH:mm');
    const startComparison = new Date(0, 0, 0, workStartTime.split(':')[0], workStartTime.split(':')[1]);
    const endComparison = new Date(0, 0, 0, workEndTime.split(':')[0], workEndTime.split(':')[1]);
    const earlyStyleColor = isEntryTime ? this.lateColor : this.onTimeColor;
    const lateStyleColor = isEntryTime ? this.onTimeColor : this.lateColor;
    data.forEach(entry => {
      const entryTime = DateUtility.getTimeFromHours(String(entry / 360000));
      if (new Date(0, 0, 0, entryTime.split(':')[0], entryTime.split(':')[1]) >= (isEntryTime ? startComparison : endComparison)) {
        chartData.push({
          value: entry,
          itemStyle: {
            color: earlyStyleColor
          },
          lineStyle: {
            color: this.lineColor
          }
        });
      } else {
        chartData.push({
          value: entry,
          itemStyle: {
            color: lateStyleColor
          },
          lineStyle: {
            color: this.lineColor
          }
        });
      }
    });
    return chartData;
  }
}
