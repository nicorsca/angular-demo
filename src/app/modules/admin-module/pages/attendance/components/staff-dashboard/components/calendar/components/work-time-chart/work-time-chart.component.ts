import {Component, Injector, Input, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../../../AbstractBaseComponent';
import {AttendanceService} from '@common/services';
import {AttendanceCalendarService} from '@common/services/attendance-calendar.service';
import {DateUtility} from '@common/utilities/date-utility';

@Component({
  selector: 'app-work-time-chart',
  templateUrl: './work-time-chart.component.html',
  styleUrls: ['./work-time-chart.component.scss']
})
export class WorkTimeChartComponent extends AbstractBaseComponent implements OnInit {

  @Input() workTime: string;

  @Input() breakTime: string;

  public breakTimeInHours: string;

  public chartOption: any;

  /**
   * Data for chart
   */
  public xAxisData = [];

  /**
   * Chart colors
   */
  public workTimeColor: string;

  public breakTimeColor: string;

  /**
   * Constructor
   */
  constructor(protected injector: Injector, protected attendanceService: AttendanceService, protected attendanceCalendarService: AttendanceCalendarService) {
    super(injector);
    this.workTimeColor = '#16556F';
    this.breakTimeColor = '#75BBC8';
  }

  /**
   * On Component ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DASHBOARD.HTML_TITLE');
    this.chartOption = this.setChartOptions();
    this.breakTimeInHours = this.getHoursAndMinutes(Number(this.breakTime));
  }

  /**
   * Set chart options
   */
  public setChartOptions(): any {
    return {
      legend: {
        show: false
      },
      grid: {
        left: '10%',
        right: '10%',
        containLabel: true
      },
      tooltip: {
        trigger: 'item',
        formatter: (point) => {
          return point.marker + point.name + ': ' + `<strong style="font-weight: bolder">${DateUtility.getTimeFromHours(String(point.value / 3600))}</strong><br>`;
        }
      },
      series: [
        {
          name: this.translateService.instant('MOD_ATTENDANCE.CALENDAR.WORK_CHART_LABEL'),
          type: 'pie',
          radius: ['56%', '80%'],
          avoidLabelOverlap: false,
          label: {
            show: true,
            formatter: () => this.setWorkHourLabel(),
            position: 'center',
            fontSize: '24',
          },
          labelLine: {
            show: false
          },
          data: [
            {
              value: this.workTime,
              name: this.translateService.instant('MOD_ATTENDANCE.CALENDAR.WORK_TIME_LABEL'),
              itemStyle: {
                color: this.workTimeColor
              }
            },
            {
              value: this.breakTime,
              name: this.translateService.instant('MOD_ATTENDANCE.CALENDAR.BREAK_TIME_LABEL'),
              itemStyle: {
                color: this.breakTimeColor
              }
            }
          ]
        }
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };
  }

  /**
   * Set hour label inside chart
   */
  private setWorkHourLabel(): any {
    return this.getHoursAndMinutes(Number(this.workTime));
  }
}
