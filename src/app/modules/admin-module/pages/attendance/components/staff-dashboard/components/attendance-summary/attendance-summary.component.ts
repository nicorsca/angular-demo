import {Component, Injector, Input, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AttendanceService} from '@common/services';
import {MatDialog} from '@angular/material/dialog';
import {AttendanceSummaryModel, UserModel} from '@common/models';
import {PaginatedNicoCollection} from '@system/utilities';
import {DateUtility} from '@common/utilities/date-utility';

@Component({
  selector: 'app-attendance-summary',
  templateUrl: './attendance-summary.component.html',
  styleUrls: ['./attendance-summary.component.scss']
})
export class AttendanceSummaryComponent extends AbstractBaseComponent implements OnInit {

  @Input() employeeModel: UserModel;

  @Input() summaryModel: PaginatedNicoCollection<AttendanceSummaryModel>;

  /**
   * Constructor
   */
  constructor(protected injector: Injector,
              public attendanceService: AttendanceService,
              public dialog: MatDialog) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DASHBOARD.HTML_TITLE');
  }

}
