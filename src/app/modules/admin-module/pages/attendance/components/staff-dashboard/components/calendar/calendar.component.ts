import {Component, Injector, Input, OnChanges, OnInit, Output, SimpleChanges, EventEmitter} from '@angular/core';
import {
  AgendaService,
  DayService,
  MonthAgendaService,
  MonthService, TimelineMonthService, TimelineViewsService,
  WeekService,
  WorkWeekService,
  EventSettingsModel,
  PopupOpenEventArgs, View,
  ActionEventArgs
} from '@syncfusion/ej2-angular-schedule';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {NicoCollection} from '@system/utilities';
import {AttendanceCalendarModel, UserModel} from '@common/models';
import {EjsScheduleInterface} from '@common/interface/ejs-schedule.interface';
import {DateUtility} from '@common/utilities/date-utility';
import {MatDialog} from '@angular/material/dialog';
import {TimesheetModalComponent} from './components/timesheet-modal/timesheet-modal.component';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  providers: [DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService]
})
export class CalendarComponent extends AbstractBaseComponent implements OnInit, OnChanges {

  @Input() calendarModel: NicoCollection<AttendanceCalendarModel>;

  @Input() employeeModel: UserModel;

  @Output() startEndDate = new EventEmitter<string>();

  @Output() endDate = new EventEmitter<string>();

  public scheduleModel: EjsScheduleInterface[];

  public eventSettings: EventSettingsModel;

  public views: Array<View> = ['Month'];

  public isLeave = false;

  constructor(protected injector: Injector,
              public dialog: MatDialog) {
    super(injector);
  }

  /**
   * On component ready
   */
  public onComponentReady(): void {
    this.getScheduleModel();
  }

  /**
   * Get schedule model for calendar
   */
  public getScheduleModel(): void {
    this.scheduleModel = [];
    this.calendarModel?.forEach(calendarData => {
      if (calendarData.leave_out) {
        this.scheduleModel.push({
          StartTime: new Date(calendarData.leave_in),
          EndTime: new Date(calendarData.leave_out),
          WorkHours: this.translateService.instant('MOD_ATTENDANCE.TOTAL_WORKING_HOUR_LABEL',
            {
              hour: this.getHoursAndMinutes(calendarData.work_time)
            }),
          Leave: this.translateService.instant('MOD_ATTENDANCE.CALENDAR.ON_LEAVE_LABEL')
        });
      } else {
        this.scheduleModel.push({
          StartTime: new Date(DateUtility.utcToLocalFormat(calendarData.check_in)),
          EndTime: new Date(DateUtility.utcToLocalFormat(calendarData.check_out)),
          WorkHours: this.getHoursAndMinutes(calendarData.work_time),
          Leave: null
        });
      }
    });
    this.eventSettings = {
      dataSource: this.scheduleModel
    };
  }

  /**
   * Get total work time (start + end)
   */
  public getTotalWorkTime(startTime: string, endTime: string): string {
    return this.translateService.instant('MOD_ATTENDANCE.CALENDAR.TOTAL_WORK_TIME', {
      startTime: DateUtility.utcToLocalFormat(startTime, 'HH:mm'),
      endTime: DateUtility.utcToLocalFormat(endTime, 'HH:mm')
    });
  }

  /**
   * On changes
   */
  public ngOnChanges(changes: SimpleChanges): void {
    this.getScheduleModel();
  }

  /**
   * Track change in months date
   */
  public onNavigationClick($event): void {
    const startDate = DateUtility.setDateFormat(DateUtility.getMomentObject($event.currentDate).startOf('month'), 'YYYY-MM-DD');
    let endDate = '';
    if (DateUtility.getMomentObject($event.currentDate).month() !== DateUtility.getMomentDate().month()) {
      endDate = DateUtility.setDateFormat(DateUtility.getMomentObject($event.currentDate).endOf('month'), 'YYYY-MM-DD');
    } else {
      endDate = DateUtility.setDateFormat(DateUtility.getMomentDate(), 'YYYY-MM-DD');
    }
    this.startEndDate.emit(startDate + ':' + endDate);
  }

  public onPopupOpen($event: PopupOpenEventArgs): void {
    $event.cancel = true;
  }

  /**
   * Track event click in day
   */
  public onEventClick($event): void {
    if (this.calendarModel.length > 0) {
      const todaysData = this.calendarModel?.first(data =>
        DateUtility.utcToLocalFormat(data.check_in, 'YYYY-MM-DD') ===
        DateUtility.setDateFormat($event.event.StartTime, 'YYYY-MM-DD'));
      this.dialog.open(TimesheetModalComponent, {
        data: {
          employeeId: this.employeeModel?.id,
          selectedDate: $event.event.StartTime,
          workTime: todaysData?.work_time,
          breakTime: todaysData?.break_time,
          workTimeCount: ''
        },
        panelClass: ['modal-lg'],
        disableClose: true
      });
    }
  }

  public onCellClick($event): void {
    $event.cancel = true;
    return;
  }
}
