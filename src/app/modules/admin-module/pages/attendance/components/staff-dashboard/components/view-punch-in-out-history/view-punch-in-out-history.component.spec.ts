import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ViewPunchInOutHistoryComponent} from './view-punch-in-out-history.component';
import {TranslateModule} from '@ngx-translate/core';
import {MatDialogRef} from '@angular/material/dialog';
import {AttendanceDetailModel} from '@common/models';

describe('ViewPunchInOutHistoryComponent', () => {
  let component: ViewPunchInOutHistoryComponent;
  let fixture: ComponentFixture<ViewPunchInOutHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ViewPunchInOutHistoryComponent],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: MatDialogRef, useValue: {}}
      ],
      imports: [
        TranslateModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPunchInOutHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show edit button in case of data that is of today or yesterday', () => {
    const attendance = {
      type: 'check_out',
      attend_at: new Date()
    } as unknown as AttendanceDetailModel;
    expect(component.hideEditButton(attendance)).toBeTrue();
  });

  it('should hide edit button in case of data that is older than 2 days', () => {
    const attendance = {
      type: 'leave_out',
      attend_at: new Date()
    } as unknown as AttendanceDetailModel;
    expect(component.hideEditButton(attendance)).toBeFalse();
  });

  it('should generate message for edit history', () => {
    const attendance = {
      type: 'leave_out',
      attend_at: new Date(),
      reason: 'test',
      parents: []
    } as unknown as AttendanceDetailModel;
    expect(component.showPreviousChange(attendance)).toEqual('MOD_ATTENDANCE.TIME_CHANGE_LABEL');
  });

  it ('should assign check in message to check in out', () => {
    const attendance = {
      type: 'check_in',
      attend_at: new Date()
    } as unknown as AttendanceDetailModel;
    expect(component.assignMessageForCheckInOut(attendance)).toEqual('MOD_ATTENDANCE.CHECK_IN_INFO_LABEL');
  });

  it ('should assign check out message to check in out', () => {
    const attendance = {
      type: 'check_out',
      attend_at: new Date()
    } as unknown as AttendanceDetailModel;
    expect(component.assignMessageForCheckInOut(attendance)).toEqual('MOD_ATTENDANCE.CHECK_OUT_INFO_LABEL');
  });

});
