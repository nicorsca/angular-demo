import {Routes} from '@angular/router';
import {AttendanceComponent} from './attendance.component';
import {StaffDashboardComponent} from './components/staff-dashboard/staff-dashboard.component';
import {AttendanceHistoryComponent} from './components/attendance-history/attendance-history.component';

export const ATTENDANCE_ROUTES: Routes = [
  {
    path: '', component: AttendanceComponent, children: [
      {path: 'my-dashboard', component: StaffDashboardComponent},
      {path: 'attendance-history', component: AttendanceHistoryComponent}
    ]
  },
];
