
import { Routes} from '@angular/router';
import { DepartmentsComponent } from './departments.component';
import {DepartmentResolver} from '@common/resolvers/department.resolver';

/**
 * App Routes
 */
export const DEPARTMENT_ROUTES: Routes = [
  {path: '', component: DepartmentsComponent},
  {path: ':id',
    resolve: {
      departmentResolver: DepartmentResolver,
    },
      loadChildren: () =>
      import('./components/view-detail-department/view-detail-department.module')
        .then(m => m.ViewDetailDepartmentModule),
      data: { breadcrumb: {skip: true},
  }}
];

