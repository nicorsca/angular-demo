import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import { AddEditDepartmentComponent } from './add-edit-department.component';
import {DepartmentService} from '@common/services/department.service';
import {MockDepartmentService} from '@common/testing-resources/mock-services/MockDepartmentService';
import {NicoHttpClient} from '@system/http-client';
import {
  MockAjaxSpinnerService,
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {DepartmentModel} from '@common/models';
import {By} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {BreadcrumbService} from 'xng-breadcrumb';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';

describe('AddEditDepartmentComponent', () => {
  let component: AddEditDepartmentComponent;
  let fixture: ComponentFixture<AddEditDepartmentComponent>;
  let service: DepartmentService;
  const mockData = {
    created_at: '2021-07-15T16:25:17.000000Z',
    description: 'Finance Department',
    employees_count: 0,
    hod: null,
    id: 4,
    status: 1,
    title: 'Finance department',
    updated_at: '2021-07-16T07:39:38.000000Z',
  } as unknown as DepartmentModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditDepartmentComponent ],
      providers: [
        {provide: DepartmentService, useClass: MockDepartmentService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: AjaxSpinnerService, useClass: MockAjaxSpinnerService},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useClass: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {data: {model: {}}} },
        {provide: HttpErrorResponse, useValue: {headers: {}, error: {message: 'Test', status: '417'}}},
        {provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
        {provide: ActivatedRoute, useValue:  { snapshot: {queryParams: {page: 1, sort_order: 'asc', per_page: 16}}}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useValue: undefined}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        MatDialogModule,
        CommonModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditDepartmentComponent);
    service = TestBed.inject(DepartmentService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should change page title to add department', () => {
  //   component.ngOnInit();
  //   expect(component.pageTitle).toEqual('MOD_DEPARTMENT.MOD_ADD_DEPARTMENT.PAGE_TITLE');
  // });
  //
  // it('should change form title to add department', () => {
  //   component.ngOnInit();
  //   const formTitle = fixture.debugElement.query(By.css('.modal-title'));
  //   expect(formTitle.nativeElement.outerText).toEqual('MOD_DEPARTMENT.MOD_ADD_DEPARTMENT.PAGE_TITLE');
  // });

  it('should create a form on initial', () => {
    spyOn(component, 'createFormGroup');
    component.ngOnInit();
    expect(component.createFormGroup).toHaveBeenCalled();
  });

  it('should call a post method on save', fakeAsync(async () => {
    service.saveDepartment(mockData).subscribe(response => {
      expect(response).toEqual(mockData);
    });
  }));

  // it('should not post data on error ', fakeAsync( () => {
  //   service.saveDepartment(null).subscribe(response => {
  //     expect(htmlError.error).toEqual(response.error);
  //   });
  // }));

  it('should change status publish/unpublish on toggle', () => {
    const state = component.switchState;
    component.onSwitchValueChange();
    expect(component.switchState).not.toEqual(state);
  });

  it('should disable save button on invalid form ', () => {
    component.departmentForm.setValue({
      description: '',
      status: 1,
      title: 'Finance Department',
    });
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.save')).nativeElement.disabled).toBe(true);
  });

  it('should call onSubmit on click save btn', () => {
    spyOn(component, 'onSubmitForm');
    const submitButton: DebugElement =
      fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onSubmitForm).toHaveBeenCalledTimes(1);
  });

  it('should change page title to edit', fakeAsync(() => {
    component.data = {model: mockData};
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.pageTitle).toEqual('MOD_DEPARTMENT.MOD_EDIT_DEPARTMENT.PAGE_TITLE');
  }));

  it('should change form title to edit', fakeAsync(() => {
    component.data = {model: mockData};
    component.ngOnInit();
    fixture.detectChanges();
    const formTitle = fixture.debugElement.query(By.css('.modal-title'));
    expect(formTitle.nativeElement.outerText).toEqual('MOD_DEPARTMENT.MOD_EDIT_DEPARTMENT.PAGE_TITLE');
  }));

  it('should fill data on the form', () => {
    component.data = {model: mockData};
    component.ngOnInit();
    expect(component.departmentForm.value.title).not.toBeNull();
    expect(component.departmentForm.value.description).not.toBeNull();
    expect(component.departmentForm.value.status).not.toBeNull();
  });



  it('should have form labels to respective form fields', () => {
    const titleLabel = fixture.debugElement.query(By.css('label[for=title]'));
    expect(titleLabel.nativeElement.outerText).toEqual( 'MOD_DEPARTMENT.MOD_EDIT_DEPARTMENT.DEPARTMENT_NAME_LABEL ');

    const descriptionLabel = fixture.debugElement.query(By.css('label[for=description]'));
    expect(descriptionLabel.nativeElement.outerText).toEqual( 'MOD_DEPARTMENT.MOD_EDIT_DEPARTMENT.DESCRIPTION_LABEL ');

    const statusLabel = fixture.debugElement.query(By.css('label[for=status]'));
    expect(statusLabel.nativeElement.outerText).toEqual( 'MOD_DEPARTMENT.MOD_EDIT_DEPARTMENT.STATUS_LABEL');
  });

  it('should have form placeholder to respective form fields', () => {
    const titlePlaceholder = fixture.debugElement.query(By.css('input#title'));
    expect(titlePlaceholder.nativeElement.placeholder).toEqual('MOD_DEPARTMENT.MOD_EDIT_DEPARTMENT.DEPARTMENT_NAME_PLACEHOLDER');

    const descriptionPlaceholder = fixture.debugElement.query(By.css('textarea#description'));
    expect(descriptionPlaceholder.nativeElement.placeholder).toEqual('MOD_DEPARTMENT.MOD_EDIT_DEPARTMENT.DESCRIPTION_PLACEHOLDER');
  });

});
