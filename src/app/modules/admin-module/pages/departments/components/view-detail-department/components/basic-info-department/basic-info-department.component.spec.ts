import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {BasicInfoDepartmentComponent} from './basic-info-department.component';
import {HttpErrorResponse} from '@angular/common/http';
import {LoginService} from '@common/services';
import {
  MockLoginService,
  MockNicoHttpClient,
} from '@common/testing-resources/mock-services';
import {NicoHttpClient} from '@system/http-client';
import {ActivatedRoute} from '@angular/router';
import {
  TranslateModule
} from '@ngx-translate/core';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {NicoPageTitle} from '@system/services';
import {DepartmentModel, EmployeeModel} from '@common/models';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {of} from 'rxjs';
import {MatDialogRef} from '@angular/material/dialog/dialog-ref';
import {BreadcrumbService} from 'xng-breadcrumb';
import {MockBreadcrumService} from '@common/testing-resources/mock-services/MockBreadcrumService';
import {PaginatedNicoCollection} from '@system/utilities';

describe('BasicInformationComponent', () => {
  let component: BasicInfoDepartmentComponent;
  let fixture: ComponentFixture<BasicInfoDepartmentComponent>;
  let pageTitleService: NicoPageTitle;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BasicInfoDepartmentComponent],
      providers: [
        {provide: HttpErrorResponse, useValue: {headers: {}, error: {message: 'Test', status: '417'}}},
        {provide: LoginService, useClass: MockLoginService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        ...MOCK_PROVIDERS,
        {provide: ActivatedRoute, useValue: {snapshot: {queryParams: {page: 1, sort_order: 'asc', per_page: 16}}}},
        {provide: BreadcrumbService, useClass: MockBreadcrumService}
      ],
      imports: [TranslateModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInfoDepartmentComponent);
    pageTitleService = TestBed.inject(NicoPageTitle);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set page title', () => {
    expect(pageTitleService.getTitle()).toEqual('mock-title');
  });

  it('should assign HOD in case of an existing HOD', fakeAsync(async () => {
    component.model = {
      id: 1,
      title: 'HR',
      description: 'This is a test',
      status: 2,
      employees_count: 5,
      hod: 'Jane Doe',
      employees: []
    } as unknown as DepartmentModel;
    fixture.detectChanges();
    fixture.whenStable();
    expect(component.model.hod).toEqual('Jane Doe');
  }));

  it('should un-assign HOD in case of an empty HOD', fakeAsync(async () => {
    component.model = {
      id: 1,
      title: 'HR',
      description: 'This is a test',
      status: 2,
      employees_count: 5,
      hod: '',
      employees: []
    } as unknown as DepartmentModel;
    fixture.detectChanges();
    fixture.whenStable();
    expect(component.model.hod).toEqual('');
  }));

  it('should list employees', () => {
    component.employeesModel = new PaginatedNicoCollection<EmployeeModel>();
    const dummy = {id: 1, name: 'Jane Doe'} as unknown as EmployeeModel;
    component.employeesModel.push(dummy);
    fixture.detectChanges();
    const empTable = fixture.debugElement.nativeElement.querySelector('div').getElementsByClassName('flex-table');
    expect(empTable.length).toEqual(1);
  });

  it('should not list employees if no employees are present', () => {
    component.model = {
      id: 1,
      title: 'HR',
      description: 'This is a test',
      status: 2,
      employees_count: 5,
      hod: '',
      employees: []
    } as unknown as DepartmentModel;
    fixture.detectChanges();
    const empTable = fixture.debugElement.nativeElement.querySelector('div').getElementsByClassName('flex-table');
    expect(empTable.length).toEqual(0);
  });

  it('should open edit modal on edit button click', () => {
    spyOn(component, 'onDepartmentEditClick');
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    expect(component.onDepartmentEditClick).toHaveBeenCalled();
  });

  it('should reload in case of edit', fakeAsync (async () => {
    spyOn(component.dialog, 'open')
      .and
      .returnValue({afterClosed: () => of(true)} as unknown as MatDialogRef<any>);
    spyOn(component, 'getModel');
    component.model = {
      id: 1,
      title: 'HR',
      description: 'This is a test',
      status: 1,
      employees_count: 0,
      hod: '',
      employees: [{id: 1, name: 'Jane Doe'}]
    } as unknown as DepartmentModel;
    fixture.detectChanges();
    component.onDepartmentEditClick(component.model);
    fixture.detectChanges();
    fixture.whenStable();
    expect(component.getModel).toHaveBeenCalled();
  }));

});
