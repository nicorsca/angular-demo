import {NgModule} from '@angular/core';
import {BasicInfoDepartmentComponent} from './components/basic-info-department/basic-info-department.component';
import {RouterModule} from '@angular/router';
import {VIEW_DETAIL_DEPARTMENT_ROUTES} from './view-detail-department-routes.module';
import {HrCommonModule} from '@common/hr-common.module';
import {TranslateModule} from '@ngx-translate/core';
import {NicoComponentsModule} from '@system/components';

@NgModule({
  declarations: [
    BasicInfoDepartmentComponent
  ],
    imports: [
        RouterModule.forChild(VIEW_DETAIL_DEPARTMENT_ROUTES),
        HrCommonModule,
        RouterModule,
        TranslateModule.forChild({
            extend: true
        }),
        NicoComponentsModule,
    ]
})

export class ViewDetailDepartmentModule { }
