import {Component, Injector, OnDestroy} from '@angular/core';
import {AbstractBaseComponent} from 'app/AbstractBaseComponent';
import {MatDialog} from '@angular/material/dialog';
import {Status} from '@common/enums/status.enums';
import {DepartmentModel} from '@common/models';
import {Subscription} from 'rxjs';
import {DepartmentService} from '@common/services';

@Component({
  selector: 'app-view-detail-department',
  templateUrl: './view-detail-department.component.html',
  styleUrls: ['./view-detail-department.component.scss']
})
export class ViewDetailDepartmentComponent extends AbstractBaseComponent implements OnDestroy{

  public pageTitle = '';
  public model: DepartmentModel;
  public departmentInfoSubscription: Subscription;
  public status = Status;
  public componentsList = [
    {name: 'Basic Information', link: 'basic-info-department', icon: 'far fa-info-circle'},
  ];

  constructor(protected injector: Injector,
              protected dialog: MatDialog,
              public departmentService: DepartmentService) {
    super(injector);
    this.breadcrumbService.set('@departmentName', ' ');
  }

  onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
    this.departmentInfoSubscription = this.departmentService.watchUserChange().subscribe(model => {
      this.model = model;
      this.breadcrumbService.set('@departmentName', this.model.title);
    });
  }

  public getModels(): void {
    this.activatedRoute.parent?.data.subscribe((model: any) => {
      this.model = model.departmentResolver;
      this.breadcrumbService.set('@departmentName', this.model.title);
    });
  }

  public ngOnDestroy(): void {
    if (this.departmentInfoSubscription != null) {
      this.departmentInfoSubscription.unsubscribe();
    }
  }


}
