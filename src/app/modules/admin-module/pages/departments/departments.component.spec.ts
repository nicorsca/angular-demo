import {ComponentFixture, TestBed} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DepartmentService } from '@common/services/department.service';
import { DepartmentsComponent } from './departments.component';
import {
  MockMatDialog, MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService, MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {NicoHttpClient} from '@system/http-client';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {ActivatedRoute, Router} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {MockDepartmentService} from '@common/testing-resources/mock-services/MockDepartmentService';
import {DepartmentModel} from '@common/models';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {CommonModule, Location} from '@angular/common';
import {SearchComponent} from '@common/components/search/search.component';
import {PaginatedNicoCollection} from '@system/utilities';
import {MockSearchService} from '@common/testing-resources/mock-services/MockSearchService';
import {BreadcrumbService} from 'xng-breadcrumb';


describe('DepartmentsComponent', () => {
  let component: DepartmentsComponent;
  let fixture: ComponentFixture<DepartmentsComponent>;
  let service: DepartmentService;
  let pageTitleService: NicoPageTitle;
  let dialog: MatDialog;
  let spinner: AjaxSpinnerService;
  let location: Location;
  let router: Router;
  const mockData = {
    created_at: '2021-07-15T16:25:17.000000Z',
    description: 'Finance Department',
    employees_count: 0,
    hod: null,
    id: 4,
    status: 1,
    title: 'Finance department',
    updated_at: '2021-07-16T07:39:38.000000Z',
  } as unknown as DepartmentModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartmentsComponent ],
      providers: [
        {provide: DepartmentService, useClass: MockDepartmentService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useClass: MockMatDialog},
        {provide: SearchComponent, useClass: MockSearchService},
        {provide: Router, useValue: {navigate: jasmine.createSpy('navigate')}},
        {provide: ActivatedRoute, useValue:  { snapshot: {queryParams: {page: 1, sort_order: 'asc', per_page: 16}}}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        Location,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useValue: undefined},
      ],
      imports: [
        RouterTestingModule.withRoutes([{path: '/departments/4/basic-info-department', component: DepartmentsComponent}]),
        TranslateModule,
        CommonModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentsComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(DepartmentService);
    location = TestBed.inject(Location);
    router = TestBed.inject(Router);
    pageTitleService = TestBed.inject(NicoPageTitle);
    spinner = TestBed.inject(AjaxSpinnerService);
    dialog = TestBed.inject(MatDialog);
    fixture.detectChanges();
  });


  it('should call get models on init', () => {
    spyOn(component, 'getModels');
    component.onComponentReady();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should get models from the server',  async () => {
      service.get().subscribe(response => {
        expect(response.length).toBeGreaterThan(0);
      });
  });

  it('should change page title ', () => {
    component.onComponentReady();
    expect(component.pageTitle).toBe('MOD_DEPARTMENT.PAGE_TITLE');
  });

  it('should open modal after click on edit fly menu', () => {
    spyOn(component, 'dialogView');
    component.onFlyMenuAction('edit', mockData);
    expect(component.dialogView).toHaveBeenCalled();
  });

  it('should open modal after click on new button', () => {
    spyOn(component, 'onAddDepartment');
    component.onAddDepartment();
    expect(component.onAddDepartment).toHaveBeenCalled();
  });

  it('should open confirmation dialog to delete a department', () => {
    spyOn(component, 'onRemove').withArgs(mockData);
    component.onRemove(mockData);
    expect(component.onRemove).toHaveBeenCalled();
  });

  it('should delete items from list after confirm delete', async () => {
    service.delete(mockData).subscribe(response => {
      expect(response).toEqual({body: 'success'});
    });
  });

  it('should open Add Modal on click New btn', () => {
    spyOn(component, 'onAddDepartment');
    const submitButton: DebugElement =
      fixture.debugElement.query(By.css('.new-btn'));
    submitButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onAddDepartment).toHaveBeenCalledTimes(1);
  });

  it('should change view on click toggle view btn', () => {
    const view = component.isTableView;
    component.onViewToggle(true);
    expect(component.isTableView).not.toEqual(view);
  });



  it('should change status pill on update status', () => {
    component.models = new PaginatedNicoCollection<DepartmentModel>();
    component.isTableView = true;
    component.models.push(mockData);
    fixture.detectChanges();
    const statusPill = fixture.debugElement.query(By.css('app-status-pill'));
    let status = 0;
    service.changeStatus(mockData).subscribe(response => {
      status = response.status;
      mockData.status = status;
    });
    expect(mockData.status).not.toBe(status);
    expect(statusPill.properties.status).toEqual(1);
  });

  it('should show no data if there are no data', () => {
    component.models = new PaginatedNicoCollection<DepartmentModel>();
    component.isTableView = true;
    fixture.detectChanges();
    const noDataDiv: DebugElement =
      fixture.debugElement.query(By.css('app-empty-state'));
    expect(noDataDiv).toBeTruthy();
  });

  it('should have table heading for respective data', () => {
    component.models = new PaginatedNicoCollection<DepartmentModel>();
    component.isTableView = true;
    fixture.detectChanges();
    component.models.push(mockData);
    component.models.push(mockData);
    fixture.detectChanges();
    const tableTitle = fixture.debugElement.query(By.css('.cell-data'));
    expect(tableTitle.nativeElement.outerText).toEqual('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.DEPARTMENT_NAME_LABEL');
  });


  it('should show error message if a department is in use while changing status', () => {
    const dummyData = {
      created_at: '2021-07-15T16:25:17.000000Z',
      description: 'Finance Department',
      employees_count: 2,
      hod: null,
      id: 4,
      status: 1,
      title: 'Finance department',
      updated_at: '2021-07-16T07:39:38.000000Z',
    } as unknown as DepartmentModel;
    const error = {
      code: 'err_employee_exists_exception',
      code_text: 'Please assign HOD to your department first.',
      message: 'Employee exists in the department. Unable to perform this operation.'
    };
    service.changeHOD(dummyData, dummyData.id).subscribe(response => {
      expect(response.error).toEqual(error);
    });
  });

  it('should show error message if a department is in use while deleting', () => {
    const dummyData = {
      created_at: '2021-07-15T16:25:17.000000Z',
      description: 'Finance Department',
      employees_count: 2,
      hod: null,
      id: 4,
      status: 1,
      title: 'Finance department',
      updated_at: '2021-07-16T07:39:38.000000Z',
    } as unknown as DepartmentModel;
    const error = {
      code: 'err_employee_exists_exception',
      code_text: 'Failed to perform operation, employees exists in this department',
      message: 'Employee exists in the department. Unable to perform this operation.'
    };
    service.delete(dummyData).subscribe(response => {
      expect(response.error).toEqual(error);
    });
  });
  // it('should re-fetch data from server after success delete', () => {
  //     spyOn(component, 'dialogRef')
  //       .and
  //       .returnValue({afterClosed: () => of(true)} as MatDialogRef<any>);
  //     spyOn(component, 'getModels');
  //     fixture.detectChanges();
  //     component.onRemove(mockData);
  //     fixture.detectChanges();
  //     fixture.whenStable();
  //     expect(component.getModels).toHaveBeenCalled();
  // });

  it('should filter data on search', () => {
    spyOn(component, 'onSearchTextChange');
    const searchNav = fixture.debugElement.query(By.css('app-search-nav'));
    searchNav.triggerEventHandler('searchChange', 'bachelors');
    fixture.detectChanges();
    expect(component.onSearchTextChange).toHaveBeenCalled();
  });

  it('should filter data on on Sort By Change', () => {
    spyOn(component, 'onSortByChange');
    const searchNav = fixture.debugElement.query(By.css('app-search-nav'));
    searchNav.triggerEventHandler('sortByChange', 'status');
    fixture.detectChanges();
    expect(component.onSortByChange).toHaveBeenCalled();

  });
  it('should filter data on Sort Order Change ', () => {
    spyOn(component, 'onSortOrderChange');
    const searchNav = fixture.debugElement.query(By.css('app-search-nav'));
    searchNav.triggerEventHandler('sortOrderChange', 'asc');
    fixture.detectChanges();
    expect(component.onSortOrderChange).toHaveBeenCalled();
  });

  it('should have icon and name in the HOD field', () => {
    // tslint:disable-next-line:variable-name
    const customMockData = {
      created_at: '2021-07-15T16:25:17.000000Z',
      description: 'Finance Department',
      employees_count: 0,
      hod: [{id: 1, name: 'john'}],
      id: 4,
      status: 1,
      title: 'Finance department',
      updated_at: '2021-07-16T07:39:38.000000Z',
    } as unknown as DepartmentModel;
    component.models = new PaginatedNicoCollection<DepartmentModel>();
    component.isTableView = true;
    component.models.push(customMockData);
    fixture.detectChanges();
    const avatar = fixture.debugElement.query(By.css('app-user-pill'));
    expect(avatar.nativeElement).toBeTruthy();

  });

  it('should show not assigned in the HOD field if hod not assigned', () => {
    component.models = new PaginatedNicoCollection<DepartmentModel>();
    component.isTableView = true;
    component.models.push(mockData);
    fixture.detectChanges();
    const hodName = fixture.debugElement.query(By.css('#not-hod'));
    expect(hodName.nativeElement).toBeTruthy();
    expect(hodName.properties.label).toEqual('MOD_DEPARTMENT.HOD_UNASSIGNED_LABEL');
  });
});
