import {Routes} from '@angular/router';
import {AdminAttendanceDashboardComponent} from './components/admin-attendance-dashboard/admin-attendance-dashboard.component';
import {AdminAttendanceReportComponent} from './components/admin-attendance-report/admin-attendance-report.component';
import {AdminAttendanceHistoryComponent} from './components/admin-attendance-history/admin-attendance-history.component';

export const ADMIN_ATTENDANCE_ROUTES: Routes = [
  {
    path: '', children: [
      {path: 'dashboard',  component: AdminAttendanceDashboardComponent},
      {path: 'report', component: AdminAttendanceReportComponent},
      {path: 'history',  component: AdminAttendanceHistoryComponent},
      {path: '', redirectTo: 'dashboard', pathMatch: 'full'}
    ]
  },
];

