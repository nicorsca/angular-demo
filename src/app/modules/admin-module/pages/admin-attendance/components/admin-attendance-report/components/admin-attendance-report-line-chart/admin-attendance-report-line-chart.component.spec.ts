import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttendanceReportLineChartComponent } from './admin-attendance-report-line-chart.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AttendanceReportService} from '@common/services/attendance-report.service';
import {MockAttendanceReportService} from '@common/testing-resources/mock-services/MockAttendanceReportService';

describe('AdminAttendanceReportLineChartComponent', () => {
  let component: AdminAttendanceReportLineChartComponent;
  let fixture: ComponentFixture<AdminAttendanceReportLineChartComponent>;
  const mockData = {
    data: [
      {
        avg_break_time: '3744.0000',
        avg_check_in: '2021-10-05 03:34:36',
        avg_check_out: '2021-10-05 13:28:54',
        avg_office_time: '35658.0000',
        avg_work_time: '31914.0000',
        date: '2021-10-05',
        holiday: false,
        leave_days: '0',
        present_days: '10',
        total_break_time: '37440',
        total_office_time: '356580',
        total_work_time: '319140',
        weekend: false
      }
    ]
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAttendanceReportLineChartComponent ],
      providers: [
        {provide: AttendanceReportService, useClass: MockAttendanceReportService},
        ...MOCK_PROVIDERS,
      ],
      imports: [
        RouterTestingModule,
        TranslateModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttendanceReportLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call model on init', () => {
    spyOn(component, 'getModels');
    component.onComponentReady();
    expect(component.getModels).toHaveBeenCalledTimes(1);
  });

  it('should initialize start and end date on component init', () => {
    const start = component.startDate;
    const end = component.startDate;
    expect(start).not.toBeNull();
    expect(end).not.toBeNull();
  });



});
