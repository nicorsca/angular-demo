import { Component, Injector, OnDestroy } from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateUtility } from '@common/utilities/date-utility';
import * as moment from 'moment';
import { NicoStorageService } from '@system/services';

@Component({
  selector: 'app-admin-attendance-report',
  templateUrl: './admin-attendance-report.component.html',
  styleUrls: ['./admin-attendance-report.component.scss']
})
export class AdminAttendanceReportComponent extends AbstractBaseComponent implements OnDestroy {

  public startDate: string;
  public endDate: string;
  public pageTitle = '';
  public employeeWorkTime: string;
  public rangeForm: FormGroup;
  public endTimeThreshold: number;
  public startTimeThreshold: number;
  public min: any;
  public max: any;
  constructor(injector: Injector, public formBuilder: FormBuilder, public nicoStorageService: NicoStorageService) {
    super(injector);
    this.setThreshold();
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ATTENDANCE_REPORT.PAGE_TITLE');
    super.onComponentReady();
    this.createForm();
    this.max =  DateUtility.substractDate(1);
  }


  public createForm(): void {
    this.rangeForm = this.formBuilder.group({
      start_date: [DateUtility.substractDate(30)],
      end_date: [DateUtility.substractDate(1)]
    });
    this.setDefaultDateRange();
  }


  public setThreshold(): void {
    this.employeeWorkTime = JSON.parse(this.nicoStorageService.getItem('settings'))?.employee_work_time;
    const startTimeThreshold = moment(DateUtility.utcToLocalFormat(
      DateUtility.getUtcDateTime(this.employeeWorkTime.split('-')[0], 'HH:mm'),
    )).add('15', 'minute').format('HH:mm');
    const endTimeThreshold = moment(DateUtility.utcToLocalFormat(
      DateUtility.getUtcDateTime(this.employeeWorkTime.split('-')[1], 'HH:mm')))
      .subtract('15', 'minute').format('HH:mm');
    this.startTimeThreshold = DateUtility.timeToSeconds(startTimeThreshold);
    this.endTimeThreshold = DateUtility.timeToSeconds(endTimeThreshold);
  }

  public setDefaultDateRange(): void {
    this.startDate = DateUtility.substractDate(30);
    this.endDate = DateUtility.substractDate(1);
  }
  public onEndDateChange(event: any): void {
    this.endDate = event.value?.format('YYYY-MM-DD');
    this.showErrorOnExceedDate();
   }
   public onStartDateChange(event: any): void {
    this.startDate = event.value?.format('YYYY-MM-DD');
   }

   public showErrorOnExceedDate(): void {
     const date = moment(this.startDate).add(32, 'days');
     if (this.endDate && date.diff(this.endDate) < 0 ) {
       this.rangeForm.patchValue({
         start_date: DateUtility.substractDate(30),
         end_date: DateUtility.substractDate(1)
       });
       this.setDefaultDateRange();
       this.showErrorSnackBar(this.translateService.instant('MOD_ATTENDANCE_REPORT.DATE_RANGE_EXCEED_ERROR'));
     }
   }
   public getDisabledDate(): boolean {
    return true;
   }
}
