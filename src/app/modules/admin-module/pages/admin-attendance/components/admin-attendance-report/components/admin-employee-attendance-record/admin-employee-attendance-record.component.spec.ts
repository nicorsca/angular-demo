import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEmployeeAttendanceRecordComponent } from './admin-employee-attendance-record.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement, SimpleChanges} from '@angular/core';

describe('AdminEmployeeAttendanceRecordComponent', () => {
  let component: AdminEmployeeAttendanceRecordComponent;
  let fixture: ComponentFixture<AdminEmployeeAttendanceRecordComponent>;

  const mockData = {
    attendances: [{date: '2021-10-05', present: false, leave: true, weekend: false, holiday: false}],
    avatar: 'https://static.toiimg.com/thumb/resizemode-4,msid-76729750,imgsize-249247,width-720/76729750.jpg',
    id: 1,
    name: 'Manish Ratna Shakya'
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminEmployeeAttendanceRecordComponent ],
      providers: [
        ...MOCK_PROVIDERS,
      ],
      imports: [
        RouterTestingModule,
        TranslateModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEmployeeAttendanceRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get model on init ', () => {
    spyOn(component, 'getModels');
    component.onComponentReady();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should set color according to the data', () => {
    const classValue = component.setTableColor(mockData.attendances[0]);
    expect(classValue).toEqual('leave');
  });

  it('should initialize start and end date on component init', () => {
    const start = component.startDate;
    const end = component.startDate;
    expect(start).not.toBeNull();
    expect(end).not.toBeNull();
  });

  it('should call get model on change date values', () => {
    spyOn(component, 'getModels');
    component.ngOnChanges(mockData as unknown as SimpleChanges);
    expect(component.getModels).toHaveBeenCalled();
  });
});
