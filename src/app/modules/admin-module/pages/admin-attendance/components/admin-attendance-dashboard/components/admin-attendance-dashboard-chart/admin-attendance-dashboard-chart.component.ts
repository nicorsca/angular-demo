import {Component, Injector, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {AttendanceReportModel} from '@common/models/attendance-report.model';
import {AttendanceReportService} from '@common/services/attendance-report.service';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';

@Component({
  selector: 'app-admin-attendance-dashboard-chart',
  templateUrl: './admin-attendance-dashboard-chart.component.html',
  styleUrls: ['./admin-attendance-dashboard-chart.component.scss']
})
export class AdminAttendanceDashboardChartComponent extends  AbstractBaseComponent implements OnDestroy {

  public modelSubscription: Subscription;
  public todaySummary: AttendanceReportModel;
  public noValue = 0;
  constructor(
    injector: Injector, public attendanceService: AttendanceReportService) {
    super(injector);
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ATTENDANCE.MOD_ADMIN_ATTENDANCE.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  public getModels(): void {
    this.modelSubscription = this.attendanceService.getAttendanceTodaySummary().subscribe(summary => {
      this.todaySummary = summary;
    });
  }

  public getChartData(key): number {
    if (this.todaySummary){
      if (this.todaySummary[key] !== 0){
        return (this.todaySummary[key] / (this.todaySummary?.present)) * 100;
      }
      else{
        return this.noValue;
      }
    }
    return this.noValue;
  }

  public ngOnDestroy(): void {
    if (this.modelSubscription != null) {
      this.modelSubscription.unsubscribe();
    }
  }
}
