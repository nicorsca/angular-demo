import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttendanceDashboardListComponent } from './admin-attendance-dashboard-list.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AttendanceReportService} from '@common/services/attendance-report.service';
import {MockAdminReportService} from '@common/testing-resources/mock-services/MockAdminReportService';
import {AttendanceReportModel} from '@common/models/attendance-report.model';
import {PaginatedNicoCollection} from '@system/utilities';
import {By} from '@angular/platform-browser';

describe('AdminAttendanceDashboardListComponent', () => {
  let component: AdminAttendanceDashboardListComponent;
  let fixture: ComponentFixture<AdminAttendanceDashboardListComponent>;
  let service: AttendanceReportService;
  const mockData = [{
    attend_date: null,
    avatar: 'https://static.toiimg.com/thumb/resizemode-4,msid-76729750,imgsize-249247,width-720/76729750.jpg',
    break_time: null,
    check_in: null,
    check_out: null,
    code: 'EN202101',
    email: 'manish.shakya@ensue.us',
    employee_id: null,
    id: 1,
    leave_in: 1635096209,
    leave_out: 1635096222,
    leave_time: null,
    name: 'Manish Ratna Shakya',
    office_time: null,
    position: 'Junior frontend developer',
    work_time: null
  }] as unknown as PaginatedNicoCollection<AttendanceReportModel>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAttendanceDashboardListComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: AttendanceReportService, useClass: MockAdminReportService}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttendanceDashboardListComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(AttendanceReportService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call get model on init', () => {
    spyOn(component, 'getModels');
    component.ngOnInit();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should return calculated value for the progress bar ', () => {
    spyOn(component, 'workHourToPercent');
    component.todayAttendance = mockData;
    fixture.detectChanges();
    expect(component.workHourToPercent).toHaveBeenCalled();
    const returnValue = component.withOutCheckoutTime(10, 0);
    expect(returnValue).toEqual(100);
  });


  it('should return data for the pill', () => {
    component.todayAttendance = mockData;
    fixture.detectChanges();
    const pill = fixture.debugElement.query(By.css('app-pill'));
    expect(pill.properties.label).toEqual('MOD_ATTENDANCE.MOD_ADMIN_ATTENDANCE.LEAVE_LABEL');
  });
});
