import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { AdminAttendanceHistoryComponent } from './admin-attendance-history.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {MatDialogRef} from '@angular/material/dialog';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {EmployeeModel} from '@common/models';

describe('AdminAttendanceHistoryComponent', () => {
  let component: AdminAttendanceHistoryComponent;
  let fixture: ComponentFixture<AdminAttendanceHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminAttendanceHistoryComponent],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: MatDialogRef, useValue: {}}
      ],
      imports: [
        TranslateModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttendanceHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get Employee Details on init', fakeAsync(async () => {
    spyOn(component, 'getEmployeeModel');
    component.onComponentReady();
    fixture.whenStable();
    expect(component.getEmployeeModel).toHaveBeenCalled();
    flush();
  }));

  it('should get attendance data on init', fakeAsync(async () => {
    spyOn(component, 'getModels');
    await fixture.detectChanges();
    await component.onComponentReady();
    await fixture.whenStable();
    expect(component.getModels).toHaveBeenCalled();
  }));

  it('should fetch models on date change', () => {
    spyOn(component, 'getModels');
    component.onEndDateChange();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should fetch models on employee selection', () => {
    spyOn(component, 'getModels');
    component.onEmployeeSelect({id: 1} as unknown as EmployeeModel);
    expect(component.getModels).toHaveBeenCalled();
  });
});
