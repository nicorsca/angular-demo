import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AttendanceTableViewComponent } from './attendance-table-view.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {MatDialogRef} from '@angular/material/dialog';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('AttendanceTableViewComponent', () => {
  let component: AttendanceTableViewComponent;
  let fixture: ComponentFixture<AttendanceTableViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AttendanceTableViewComponent],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: MatDialogRef, useValue: {}}
      ],
      imports: [
        TranslateModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create attendance table view component', () => {
    expect(component).toBeTruthy();
  });

  it('should convert time to hours and minutes', () => {
    component.userSettingsItems = '{"attendance_employee_update_before_days":"1","attendance_employee_update_enable":"1","default_email_groups":["lasata.shakya@ensue.us","amar.lamichhane@ensue.us"],"employee_work_time":"03:15-12:15","employee_working_hours":"8","leaves_can_request_before_days":"5","leaves_max_request_days":"10","leaves_type":["sick","personal","maternity","paternity","bereavement","compensatory"],"project_type":["in_house","client","others"],"system_fiscal_date_start":"07-16","system_weekends":["0","6"],"user_list_view":"grid","user_theme":"light","user_timezone":"utc"}';
    fixture.detectChanges();
    expect(component.convertDateToHoursAndMinutes('2021-02-10 11:12:00')).toEqual({ label: '-', color: 'display-none' });
  });

  it('should convert date time to format YYYY-MM-DD', () => {
    expect(component.convertDateTimeToDate('2021-02-10 11:12:00')).toEqual('2021-02-10');
  });

  it('should assign work time color', () => {
    component.userSettingsItems = '{"attendance_employee_update_before_days":"1","attendance_employee_update_enable":"1","default_email_groups":["lasata.shakya@ensue.us","amar.lamichhane@ensue.us"],"employee_work_time":"03:15-12:15","employee_working_hours":"8","leaves_can_request_before_days":"5","leaves_max_request_days":"10","leaves_type":["sick","personal","maternity","paternity","bereavement","compensatory"],"project_type":["in_house","client","others"],"system_fiscal_date_start":"07-16","system_weekends":["0","6"],"user_list_view":"grid","user_theme":"light","user_timezone":"utc"}';
    fixture.detectChanges();
    expect(component.assignWorkTimeColor(123456)).toEqual('');
  });
});
