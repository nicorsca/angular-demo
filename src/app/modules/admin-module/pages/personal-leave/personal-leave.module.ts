import {NicoComponentsModule} from '@system/components';
import {PersonalLeaveComponent} from './personal-leave.component';
import {RouterModule} from '@angular/router';
import {NicoFileUploadModule} from '@system/modules/nico-file-upload';
import {NgModule} from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';
import {TranslateModule} from '@ngx-translate/core';
import {MatInputModule} from '@angular/material/input';
import {RequestLeaveComponent} from './components/request-leave/request-leave.component';
import {CancellationLeaveComponent} from './components/cancellation-leave/cancellation-leave.component';
import {LeaveDetailsComponent} from './components/leave-details/leave-details.component';
import {PERSONAL_LEAVE_ROUTES} from './personal-leave.routes';


@NgModule({
  declarations: [
    PersonalLeaveComponent,
    RequestLeaveComponent,
    CancellationLeaveComponent,
    LeaveDetailsComponent
  ],
  imports: [
    HrCommonModule,
    NicoComponentsModule,
    RouterModule.forChild(PERSONAL_LEAVE_ROUTES),
    TranslateModule.forChild({
      extend: true
    }),
    MatInputModule,
    NicoFileUploadModule,
  ],
  providers: [
  ],
})
export class PersonalLeaveModule { }
