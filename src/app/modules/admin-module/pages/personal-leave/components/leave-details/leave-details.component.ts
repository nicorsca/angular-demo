import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {LeaveService} from '@common/services/leave.service';
import {LeaveModel} from '@common/models/leave.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppRouteConstants} from '@common/constants/appRouteConstants';
import {DeclineModalComponent} from '@common/components/decline-modal/decline-modal.component';
import {Subscription} from 'rxjs';
import {LeaveStatus} from '@common/enums/status.enums';
// @ts-ignore
import {StatusInterface} from '@common/interface/status.interface';
import {DateUtility} from "@common/utilities/date-utility";

@Component({
  selector: 'app-leave-details',
  templateUrl: './leave-details.component.html',
  styleUrls: ['./leave-details.component.scss']
})
export class LeaveDetailsComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public leaveApproveSubscription: Subscription;
  public modelLeaveSubscription: Subscription;
  public viewReason: boolean;
  public model: LeaveModel;
  public declineForm: FormGroup;
  public leaveForm: FormGroup;
  public declineFormID: string;
  public leaveForEmployee: boolean;
  public leaveStatus = LeaveStatus;
  public oldTitle: string;
  public pageTitle = '';
  public startDate: any;
  public endDate: any;
  constructor(injector: Injector,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<LeaveDetailsComponent>,
              public confirmationRefs: MatDialogRef<LeaveDetailsComponent>,
              public leaveService: LeaveService,
              public formBuilder: FormBuilder,
              public dialog: MatDialog
              ) {
    super(injector);
    this.viewReason = false;
    this.data.leaveFor === AppRouteConstants.EMPLOYEE_LEAVE ?
      this.leaveForEmployee = true :
      this.leaveForEmployee = false;
    this.declineFormID = 'declineFormID';
    this.oldTitle = this.pageTitleService.getTitle().split('-')[1];
    this.startDate = DateUtility.utcToLocalFormat(this.data.leave.start_at);
    this.endDate = DateUtility.utcToLocalFormat(this.data.leave.end_at);
  }

 public onComponentReady(): void {
     this.pageTitle = this.translateService.instant('MOD_LEAVE.LEAVE_DETAIL_LABEL');
     super.onComponentReady();
     this.onCreateForm();
     this.getModel();
 }

 public getModel(): void {
   this.modelLeaveSubscription = this.leaveService.getLeavesDetail(this.data.leave.id).subscribe(model => {
      this.model = model;
      this.setFormData(this.model);
    });
 }

 public setFormData(model: LeaveModel): void {
    this.leaveForm.patchValue(model);
 }
 public onCreateForm(): void {
    this.declineForm = this.formBuilder.group({
      reason: ['', Validators.required]
    });
    this.leaveForm = this.formBuilder.group({
      title: [null],
      description: [null],
      start_at: [null],
      end_at: [null],
    });
 }

  public onDecline(): void {
    const modalDialog = this.dialog.open(DeclineModalComponent, {panelClass: 'modal-xs',
                   data: {model: this.model, message: this.translateService.instant('MOD_LEAVE.LEAVE_CANCEL_MESSAGE')}});
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.dialogRef.close(true);
      }
    });
  }


// @ts-ignore
  public getPillData(status): StatusInterface {
    switch (status) {
      case LeaveStatus.Approved:
        return {
          label: this.translateService.instant('MOD_LEAVE.APPROVED_LABEL'),
          color: 'success'
        };

      case LeaveStatus.Pending:
        return {
          label: this.translateService.instant('MOD_LEAVE.PENDING_LABEL'),
          color: 'info'
        };

      case LeaveStatus.Cancelled:
        return {
          label: this.translateService.instant('MOD_LEAVE.CANCELLED_LABEL'),
          color: 'danger'
        };

      case LeaveStatus.Declined:
        return {
          label: this.translateService.instant('MOD_LEAVE.DECLINED_LABEL'),
          color: 'danger'
        };

      default:
        return {
          label: '',
          color: null
        };
    }
  }

  ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.leaveApproveSubscription != null){
      this.leaveApproveSubscription.unsubscribe();
    }
    if (this.modelLeaveSubscription != null){
      this.modelLeaveSubscription.unsubscribe();
    }
  }
}
