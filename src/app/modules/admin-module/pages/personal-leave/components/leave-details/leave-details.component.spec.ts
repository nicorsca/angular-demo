import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LeaveDetailsComponent } from './leave-details.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {EmployeeDocumentsService} from '@common/services/employee-documents.service';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule} from '@angular-material-components/datetime-picker';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {LeaveModel} from '@common/models/leave.model';

describe('LeaveDetailsComponent', () => {
  let component: LeaveDetailsComponent;
  let fixture: ComponentFixture<LeaveDetailsComponent>;
  const mockData = {
    avatar: undefined,
    code: undefined,
    created_at: '2021-09-17T06:54:35.000000Z',
    days: 3,
    department: undefined,
    description: 'mero khutta bacyo',
    email: undefined,
    employee: {id: 1, name: 'Manish Ratna Shakya', email: 'manish.shakya@ensue.us', avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7w-VUkvVobuC_P3TImDJalLsfOGGijkNRmA&usqp=CAU', code: 'EN200701'},
    end_at: '2021-11-23 18:14:59',
    id: 9,
    is_emergency: false,
    name: undefined,
    requested: {id: 20, name: 'Angel Ratna Roman', email: 'angel@gmail.com', avatar: null, code: 'EN202106'},
    start_at: '2021-11-20 18:15:00',
    status: 1,
    title: 'khutta bhachyo',
    type: 'sick',
    updated_at: '2021-09-17T06:54:35.000000Z',
  } as unknown as LeaveModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaveDetailsComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        {provide: EmployeeDocumentsService, useClass: MockEmployeesService},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useClass: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {leave: mockData}},
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule,
        NgxMatDatetimePickerModule,
        NgxMatNativeDateModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have page title to be leave details', () => {
    component.ngOnInit();
    expect(component.pageTitle).toBe('MOD_LEAVE.LEAVE_DETAIL_LABEL');
  });

  it('should have modal title to be based on leave model title', () => {
    const title: DebugElement =
      fixture.debugElement.query(By.css('.modal-title'));
    expect(title.nativeElement.outerText).toEqual('MOD_LEAVE.LEAVE_DETAIL_LABEL');
    fixture.detectChanges();
  });
  it('should get model on init', () => {
    spyOn(component, 'getModel');
    component.onComponentReady();
    expect(component.getModel).toHaveBeenCalled();
  });

  it('should close drawer on click cross btn', () => {
    spyOn(component.dialogRef,  'close');
    const closeBtn: DebugElement =
      fixture.debugElement.query(By.css('.icon-btn-circular'));
    closeBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(closeBtn.attributes['mat-dialog-close']).toBe('');
  });

  it('should show status on pill', () => {
    const pill: DebugElement =
      fixture.debugElement.query(By.css('app-pill'));
  });
});
