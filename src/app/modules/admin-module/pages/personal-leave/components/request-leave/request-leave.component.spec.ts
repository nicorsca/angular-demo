import { ComponentFixture, TestBed } from '@angular/core/testing';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {EmployeeDocumentsService} from '@common/services/employee-documents.service';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';
import {NicoStorageService} from '@system/services';
import {LeaveModel} from '@common/models/leave.model';
import { NgxMatDatetimePickerModule, NgxMatNativeDateModule} from '@angular-material-components/datetime-picker';
import {ReactiveFormsModule} from '@angular/forms';
import {RequestLeaveComponent} from './request-leave.component';

describe('RequestLeaveComponent', () => {
  let component: RequestLeaveComponent;
  let fixture: ComponentFixture<RequestLeaveComponent>;
  let storageService: NicoStorageService;
  const mockData = {
    avatar: undefined,
    code: undefined,
    created_at: '2021-09-17T06:54:35.000000Z',
    days: 3,
    department: undefined,
    description: 'mero khutta bacyo',
    email: undefined,
    employee: {id: 1, name: 'Manish Ratna Shakya', email: 'manish.shakya@ensue.us', avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7w-VUkvVobuC_P3TImDJalLsfOGGijkNRmA&usqp=CAU', code: 'EN200701'},
    end_at: '2021-11-23 18:14:59',
    id: 9,
    is_emergency: false,
    name: undefined,
    requested: {id: 20, name: 'Angel Ratna Roman', email: 'angel@gmail.com', avatar: null, code: 'EN202106'},
    start_at: '2021-11-20 18:15:00',
    status: 1,
    title: 'khutta bhachyo',
    type: 'sick',
    updated_at: '2021-09-17T06:54:35.000000Z',
  } as unknown as LeaveModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestLeaveComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        {provide: EmployeeDocumentsService, useClass: MockEmployeesService},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useValue: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {model: {id: 1, requested: {id: 1}}}},
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule,
        NgxMatDatetimePickerModule,
        NgxMatNativeDateModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestLeaveComponent);
    storageService = TestBed.inject(NicoStorageService);
    storageService.setItem('settings',  JSON.stringify({leave_type: ['a', 'b', 'c']}));
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have page title to request leave', () => {
    component.data = {};
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_LEAVE.MOD_LEAVE_REQUEST.PAGE_TITLE');
  });

  it('should have page title to update leave on data available', () => {
    component.data = {model: {requested: { id : 1}}};
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_LEAVE.MOD_UPDATE_REQUEST.PAGE_TITLE');
  });

  it('should be empty form on initial ', () => {
    component.data = {};
    component.createForm();
    const defaultData = {
      title: null,
      type: '',
      description: null,
      start_at: null,
      is_emergency: false,
      requested_to: null,
    } as unknown as LeaveModel;
    expect(component.requestForm.value).toEqual(defaultData);
  });

  it('should be filled form on update leave ', () => {
    component.ngOnInit();
    expect(component.requestForm.value).not.toBeNull();
  });

  it('should disable end at if start at is not filled', () => {
    component.data = {};
    component.createForm();
    expect(component.requestForm.controls.end_at.status).toBe('DISABLED');
  });

  it('should enable end at if start at is not filled', () => {
    component.data = {};
    component.setEndDatePick();
    expect(component.requestForm.controls.end_at.status).toBe('VALID');
  });
  it('should be greater value of end at of start at', () => {
    component.data = {};
    component.createForm();
    component.requestForm.get('start_at').setValue(new Date('2021/9/18 00:00:00'));
    component.endMin = component.requestForm.get('start_at').value;
    fixture.detectChanges();
    component.setEndDatePick();
    component.requestForm.get('start_at').setValue(new Date('2021/9/17 00:00:00'));
    fixture.detectChanges();
    expect(component.requestForm.get('end_at').status).toBe('INVALID');
  });

  // it('')
});
