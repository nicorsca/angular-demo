import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {LeaveService} from '@common/services/leave.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DateUtility} from '@common/utilities/date-utility';
import * as moment from 'moment';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-cancellation-leave',
  templateUrl: './cancellation-leave.component.html',
  styleUrls: ['./cancellation-leave.component.scss']
})
export class CancellationLeaveComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public cancellationSubscription: Subscription;
  public leaveCancellationSubscrption: Subscription;
  public date: moment.Moment;
  public showSpinners = true;
  public showSeconds = true;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;

  public env: any;
  public min: Date;
  public max: Date;
  public today = DateUtility.getMomentDate();
  public defaultTime = [0, 0, 0];
  public defaultTimeEnd = [23, 59, 59 ];
  public oldTitle = '';
  public pageTitle = '';
  public cancellationForm: FormGroup;
  public cancellationFormID = 'cancellationFormID';
  public endMin: Date;
  public endMax: Date;
  public endDateTooltip: string;
  public startDateTooltip: string;
  constructor(injector: Injector,
              public leaveService: LeaveService,
              public formBuilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<CancellationLeaveComponent>) {
    super(injector);
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_LEAVE.MOD_CANCELLATION.PAGE_TITLE');
    super.onComponentReady();
    this.createForm();
    this.setDatePickRange();
    this.setEndDatePick();
  }

  public createForm(): void {
    this.cancellationForm = this.formBuilder.group({
      title: [null, [Validators.required]],
      description: [null, [Validators.required]],
      start_at: [null, [Validators.required]],
      end_at: [null, [Validators.required]],
    });
    this.cancellationForm.controls.end_at.disable();
    this.endDateTooltip = DateUtility.utcToLocalFormat(this.data.leave.end_at);
    this.startDateTooltip = DateUtility.utcToLocalFormat(this.data.leave.start_at);
  }

  public onFormSubmit(): void {
    this.leaveCancellationSubscrption = this.leaveService
      .onCancellationLeave(this.data.leave.id, this.cancellationForm.value).subscribe(response => {
      this.dialogRef.close(true);
    },  error => {
      this.onFormSubmissionError(error, this.cancellationFormID);
    });
  }

  public setDatePickRange(): void {
    this.min = new Date(DateUtility.utcToLocalFormat(this.data.leave.start_at));
    this.max = new Date(DateUtility.utcToLocalFormat(this.data.leave.end_at));
  }

  public setEndDatePick(): void{
    this.cancellationSubscription = this.cancellationForm.controls.start_at.valueChanges.subscribe( value => {
      if (value) {
        this.cancellationForm.controls.end_at.enable();
        this.endMin = value;
        this.endMax = new Date(DateUtility.utcToLocalFormat(this.data.leave.end_at));
      }
    });
  }

  public ngOnDestroy(): void {
      if (this.cancellationSubscription != null){
          this.cancellationSubscription.unsubscribe();
      }
      if (this.leaveCancellationSubscrption != null){
          this.leaveCancellationSubscrption.unsubscribe();
      }
  }

}
