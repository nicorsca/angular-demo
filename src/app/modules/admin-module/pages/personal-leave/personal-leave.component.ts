import {NicoStorageService} from '@system/services';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {PaginatedNicoCollection} from '@system/utilities';
import {MatDialog} from '@angular/material/dialog';
import {LeaveDetailsComponent} from './components/leave-details/leave-details.component';
import {FlyMenuItemInterface} from '@system/components';
import {LeaveStatus} from '@common/enums/status.enums';
import {LeaveService} from '@common/services/leave.service';
import {RequestLeaveComponent} from './components/request-leave/request-leave.component';
import {LeaveModel} from '@common/models/leave.model';
import {CancellationLeaveComponent} from './components/cancellation-leave/cancellation-leave.component';
import {Subscription} from 'rxjs';
import {StatusInterface} from '@common/interface/status.interface';
import {environment} from '../../../../../environments/environment';
import {NicoHttpClient} from '@system/http-client';


@Component({
  selector: 'app-personal-view',
  templateUrl: './personal-leave.component.html',
  styleUrls: ['./personal-leave.component.scss']
})
export class PersonalLeaveComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public modelSubscription: Subscription;
  public leaveSummarySubscription: Subscription;
  public dialogSubscription: Subscription;
  public models: PaginatedNicoCollection<LeaveModel>;
  public summary: any;
  public isTableView: boolean;
  public userDetail: any;
  public leaveStatus = LeaveStatus;
  public leaveFor: string;
  public pageTitle = '';
  public summarizedData: any = [];
  public dataToDisplay: any = [];
  public env: any;
  public flyMenuExtraAddOns: FlyMenuItemInterface[] = [ {
    name: 'edit',
    label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.EDIT_BUTTON_LABEL'),
    active: true,
    faIcon: 'far fa-pen'
  },
    {
      name: 'cancellation',
      label: this.translateService.instant('MOD_LEAVE.CANCELLATION_LABEL'),
      active: true,
      faIcon: 'far fa-ban'
    }
  ];
  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {
      name: 'edit',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.EDIT_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-pen'
    }
  ];
  public statusDropdownValues = [
    {
      status: this.translateService.instant('MOD_EMPLOYEES.STATUS_DROPDOWN_VALUES.ALL'),
      value: 'all'
    },
    {
      status: this.translateService.instant('MOD_LEAVE.PENDING_LABEL'),
      value: LeaveStatus.Pending
    },
    {
      status: this.translateService.instant('MOD_LEAVE.CANCELLED_LABEL'),
      value: LeaveStatus.Cancelled
    },
    {
      status: this.translateService.instant('MOD_LEAVE.APPROVED_LABEL'),
      value: LeaveStatus.Approved
    },
    {
      status: this.translateService.instant('MOD_LEAVE.DECLINED_LABEL'),
      value: LeaveStatus.Declined
    }
  ];

  public sortByDropdownValues = [
    {
      label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.TITLE_LABEL'),
      value: 'title'
    },
    {
      label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.START_AT_LABEL'),
      value: 'start_at'
    },
    {
      label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.END_AT_LABEL'),
      value: 'end_at'
    },
    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.STATUS'),
      value: 'status'
    },
    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.CREATED_AT'),
      value: 'created_at'
    },
  ];
  constructor(injector: Injector,
              public leaveService: LeaveService,
              public storageService: NicoStorageService,
              public httpClient: NicoHttpClient,
              public dialog: MatDialog) {
    super(injector);
    this.env = environment.api;
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_LEAVE.PERSONAL_VIEW.PAGE_TITLE');
    super.onComponentReady();
    this.userDetail = JSON.parse(this.storageService.getItem('user_details'));
    this.filterQueryParams.sort_by = 'created_at';
    this.setFilterHttpQueryParams();
    if (this.userDetail?.id) {
      this.getModels();
      this.getSummary();
    }
    this.storageService.getItem('view') ?
      this.isTableView = this.storageService.getItem('view') === 'true' :
      this.isTableView = false;
  }

  public getSummary(): void {
    this.leaveSummarySubscription = this.httpClient.get(`${this.env}leaves/summary`,
      {params: this.getFilterHttpQueryParams()}).subscribe(summary => {
      this.summary = summary;
      this.summarizedData = [];
      this.dataToDisplay = [];
      this.calculateSummary();
    });
  }

  public calculateSummary(): void {
    let totalValue = {};
    for (const summary in this.summary) {
      if (summary === 'creatableAttributes') {
        continue;
      }
      else if (summary === 'total'){
        totalValue = { summary: {...this.summary[summary],
            title: summary,
            value: this.setChartValue(this.summary[summary]), fillValue: this.setFillValue(summary, this.summary),
            color: this.setColorChart()
          }
        };
        delete this.summary.total;
     }
      else{
        this.summarizedData.push({summary: {
          ...this.summary[summary],
              title: summary,
              value: this.setChartValue(this.summary[summary]), fillValue: this.setFillValue(summary, this.summary),
              color: this.setColorChart()
            }
          }
        );
      }
    }
    this.summarizedData = Object.entries(this.summarizedData).sort(this.compare).reverse()
      .map((e) => (e[1]));
    this.summarizedData.unshift(totalValue);
    this.extractMaxFourData();
  }


  public setColorChart(): string {
    const chartColors = ['primary', 'accent', 'warning', 'info', 'success', 'danger'];
    return chartColors[Math.floor(Math.random() * chartColors.length)];
  }

  public extractMaxFourData(): void {
    let sumAllocated = 0;
    let sumLeaves = 0;
    this.summarizedData.forEach((data, index) => {
      if (index <= 2 ){
        this.dataToDisplay.push(data);
      }
      else {
        sumAllocated += data.summary?.allocated;
        sumLeaves += data.summary?.leaves;
      }
    });
    this.dataToDisplay.push({ summary: {
        title: this.translateService.instant('MOD_LEAVE.OTHER_LABEL'),
        allocated: sumAllocated,
        leaves: sumLeaves,
        color: this.setColorChart(),
        value: sumLeaves === 0 && sumAllocated === 0 ? '0/0' : sumLeaves + '/' + sumAllocated,
        fillValue: sumLeaves !== 0 && sumAllocated !== 0 ?
          (sumLeaves / sumAllocated) * 100 : 0
      }
    });
  }


  public setChartValue(leaveObj): string {
    return leaveObj?.leaves + '/' + leaveObj?.allocated;
  }

  public setFillValue(key, obj): number {
    if (obj[key]?.allocated > 0) {
      return Math.ceil((obj[key]?.leaves / obj[key]?.allocated * 100));
    } else {
      return 0;
    }
  }

  public getModels(): void {
    this.filterQueryParams.employee_id = this.userDetail?.id ;
    this.setFilterHttpQueryParams();
    this.modelSubscription = this.leaveService.getLeaves(this.getFilterHttpQueryParams()).subscribe(models => {
      this.models = models;
    });
  }

  public getFlyMenuAdOns(model: LeaveModel): FlyMenuItemInterface[] {
    if (model.type === 'cancellation') {
      return [...this.flyMenuAddOns];
    }
    else if (model.status === LeaveStatus.Approved) {
      return [...this.flyMenuExtraAddOns];
    }
    else{
      return [...this.flyMenuAddOns];
    }
  }

  public onFlyMenuAction(event: any, model: LeaveModel): void {
    switch (event) {
      case 'edit':
        if (model.status === LeaveStatus.Pending) {
          this.requestLeave(model);
        } else {
          this.showErrorSnackBar(this.translateService.instant('MOD_LEAVE.NO_EDIT_ERROR_LABEL'));
        }
        break;

      case 'cancellation':
        this.onCancellation(model);
        break;
    }
  }

  public onViewToggle($event): void {
    this.isTableView = $event;
  }

  public onSelectTableRow(model: LeaveModel): void {
    this.onViewLeave(model);
  }


  public onViewLeave(leave: LeaveModel): void {
    const modalDialog = this.dialog
      .open(LeaveDetailsComponent, {
        panelClass: ['modal-drawer-view', 'modal-xs'],
        disableClose: true,
        data: {leaveFor: this.leaveFor, leave}
      });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
        this.getSummary();
      }
    });
  }

  public onCancellation(leave: LeaveModel): void {
    if (leave.status === LeaveStatus.Approved ) {
      if (leave.type !== 'cancellation') {
        const modalDialog = this.dialog
          .open(CancellationLeaveComponent, {
            panelClass: ['modal-drawer-view', 'modal-xs'],
            disableClose: true,
            data: {leave}
          });
        modalDialog.afterClosed().subscribe(reload => {
          if (reload) {
            this.getModels();
            this.getSummary();
          }
        });
      }
      else{
        this.showErrorSnackBar(this.translateService.instant('MOD_LEAVE.CANCELLATION_ERROR_MESSAGE'));

      }
    } else {
      this.showErrorSnackBar(this.translateService.instant('MOD_LEAVE.APPROVE_CAN_BE_CANCELLED'));
    }
  }

// @ts-ignore
  public getPillData(status): StatusInterface {
    switch (status) {
      case LeaveStatus.Approved:
        return {
          label: this.translateService.instant('MOD_LEAVE.APPROVED_LABEL'),
          color: 'success'
        };

      case LeaveStatus.Pending:
        return {
          label: this.translateService.instant('MOD_LEAVE.PENDING_LABEL'),
          color: 'info'
        };

      case LeaveStatus.Cancelled:
        return {
          label: this.translateService.instant('MOD_LEAVE.CANCELLED_LABEL'),
          color: 'danger'
        };

      case LeaveStatus.Declined:
        return {
          label: this.translateService.instant('MOD_LEAVE.DECLINED_LABEL'),
          color: 'danger'
        };

      default:
        return {
          label: '',
          color: null
        };
    }
  }

  public requestLeave(model?: LeaveModel): void {
    const modalDialog = this.dialog
      .open(RequestLeaveComponent, {
        panelClass: ['modal-drawer-view', 'modal-xs'],
        disableClose: true,
        data: {model}
      });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
        this.getSummary();
      }
    });
  }

  public ngOnDestroy(): void {
    if (this.modelSubscription != null) {
      this.modelSubscription.unsubscribe();
    }
    if (this.leaveSummarySubscription != null) {
      this.leaveSummarySubscription.unsubscribe();
    }
    if (this.dialogSubscription != null) {
      this.dialogSubscription.unsubscribe();
    }
  }

  public compare(a, b): number {
    if (a[1].summary.allocated < b[1].summary.allocated) {
      return -1;
    }
    if (a[1].summary.allocated > b[1].summary.allocated) {
      return 1;
    }
    return 0;
  }

}
