import {Component, Injector, OnInit} from '@angular/core';
import {FlyMenuItemInterface} from '@system/components';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent extends AbstractBaseComponent implements OnInit {

  constructor(protected injector: Injector) {
    super(injector);
  }

  /**
   * Page title
   */

  public pageTitle = '';

  /**
   * Fly menu interface
   */
  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {name: 'edit', label: 'Edit', active: true},
    {name: 'delete', label: 'Delete', active: true},
  ];

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DASHBOARD.HTML_TITLE');
    super.onComponentReady();
  }
}
