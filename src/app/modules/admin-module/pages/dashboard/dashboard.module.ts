import { NgModule } from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';
import {DashboardComponent} from './dashboard.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {DASHBOARD_ROUTES} from './dashboard-routes.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    DashboardComponent,
  ],
  imports: [
    HrCommonModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(DASHBOARD_ROUTES),
    TranslateModule.forChild({
      extend: true
    }),
  ],
  providers: [],
})

export class DashboardModule { }
