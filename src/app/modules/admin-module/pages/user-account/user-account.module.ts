import { NgModule } from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {UserAccountComponent} from './user-account.component';
import {USER_ACCOUNT_ROUTES} from './user-account-route.module';
import { ChangePasswordModalComponent } from './components/change-password-modal/change-password-modal.component';
import {BasicDetailsComponent} from './components/basic-details/basic-details.component';
import {NicoComponentsModule} from '@system/components';
import { UserContactsComponent } from './components/user-contacts/user-contacts.component';
import { UserBanksComponent } from './components/user-banks/user-banks.component';
import {UserSocialSecurityComponent} from './components/user-social-security/user-social-security.component';
import {UserAddressComponent} from './components/user-address/user-address.component';
import {UserAllocatedLeavesComponent} from './components/user-allocated-leaves/user-allocated-leaves.component';

@NgModule({
  declarations: [
    UserAccountComponent,
    ChangePasswordModalComponent,
    BasicDetailsComponent,
    UserContactsComponent,
    UserBanksComponent,
    UserSocialSecurityComponent,
    UserAddressComponent,
    UserAllocatedLeavesComponent
  ],
  imports: [
    HrCommonModule,
    RouterModule.forChild(USER_ACCOUNT_ROUTES),
    TranslateModule.forChild({
      extend: true
    }),
    NicoComponentsModule,
  ],
  providers: [],
})

export class UserAccountModule { }
