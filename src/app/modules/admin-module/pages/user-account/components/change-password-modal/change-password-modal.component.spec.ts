import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { ChangePasswordModalComponent } from './change-password-modal.component';
import {TranslateModule
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {throwError} from 'rxjs';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {ChangePasswordService} from '@common/services/change-password.service';
import {MockResetPasswordService} from '@common/testing-resources/mock-services/MockResetPasswordService';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('ChangePasswordModalComponent', () => {
  let component: ChangePasswordModalComponent;
  let fixture: ComponentFixture<ChangePasswordModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangePasswordModalComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: MatDialogRef, useClass: MockMatDialog},
        {provide: ChangePasswordService, useClass: MockResetPasswordService}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set page title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_ACCOUNT.MOD_CHANGE_PASSWORD.HTML_TITLE');
  });

  it('should create Change Password Component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form', () => {
    spyOn(component, 'createForm');
    component.ngOnInit();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should not show success snack bar in case of error', fakeAsync(async () => {
    // @ts-ignore
    spyOn(component, 'showSuccessSnackBar').and.returnValue(throwError({error: {status: 417}}));
    component.saveChangedPassword();
    expect(component.showSuccessSnackBar).not.toHaveBeenCalled();
    flush();
  }));

  it('should not show error message in case of success', fakeAsync(async () => {
    spyOn(component, 'onFormSubmissionError').and.returnValue();
    component.saveChangedPassword();
    expect(component.onFormSubmissionError).not.toHaveBeenCalled();
  }));

  it('should disable button until conditions are met', () => {
    component.createForm();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toMatch('');
  });

  it('should activate button after conditions are met', () => {
    component.onComponentReady();
    component.changePasswordFormGroup.patchValue({old_password: 'test', new_password: 'test1A@@CC', new_password_confirmation: 'test1A@@CC'});
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toBeDefined();
  });

  it('should show success bar on success', fakeAsync(async () => {
    spyOn(component, 'showSuccessSnackBar');
    component.onComponentReady();
    component.changePasswordFormGroup.patchValue({old_password: 'validPassword', new_password: 'AASSTest12@', new_password_confirmation: 'AASSTest12@'});
    await component.saveChangedPassword();
    await fixture.whenStable();
    expect(component.showSuccessSnackBar).toHaveBeenCalled();
  }));

  it('should show error bar on error', fakeAsync(async () => {
    spyOn(component, 'showSuccessSnackBar');
    component.onComponentReady();
    component.changePasswordFormGroup.patchValue({old_password: 'test', new_password: 'AASSTest12@', new_password_confirmation: 'AASSTest12@'});
    component.saveChangedPassword();
    expect(component.showSuccessSnackBar).not.toHaveBeenCalled();
  }));

});
