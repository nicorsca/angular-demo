import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {MatDialogRef} from '@angular/material/dialog';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {Subscription} from 'rxjs';
import {AuthUserChangePasswordService} from '@common/services';
import {HrValidators} from '@common/utilities';

@Component({
  selector: 'app-change-password-modal',
  templateUrl: './change-password-modal.component.html',
})
export class ChangePasswordModalComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Page title
   */

  public pageTitle = '';

  /**
   * Form
   */
  public changePasswordFormGroup: FormGroup;

  public changePasswordFormId = 'change-pass-form';

  /**
   * Subscription
   */
  public changePasswordSubscription: Subscription;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<ChangePasswordModalComponent>,
    public formBuilder: FormBuilder,
    public changePasswordService: AuthUserChangePasswordService,
  ) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ACCOUNT.MOD_CHANGE_PASSWORD.HTML_TITLE');
    super.onComponentReady();
    this.createForm();
  }

  /**
   * To create the login form
   */
  public createForm(): void {
    this.changePasswordFormGroup = this.formBuilder.group({
      old_password: new FormControl('', [Validators.required]),
      new_password: new FormControl('', [Validators.required, Validators.minLength(8), HrValidators.validatePassword]),
      new_password_confirmation: new FormControl('', [Validators.required]),
    });
    this.changePasswordFormGroup.get('new_password_confirmation').setValidators(
      [
        Validators.required,
        HrValidators.fieldsMatch(this.changePasswordFormGroup.get('new_password'))
      ]
    );
  }

  /**
   * To close modal
   */
  public closeModal(changePassword: boolean): void {
    changePassword ? this.saveChangedPassword() : this.dialogRef.close();
  }

  /**
   * On form submit
   */
  public saveChangedPassword(): void {
    this.changePasswordSubscription = this.changePasswordService.put(this.changePasswordFormGroup.getRawValue())
      .subscribe(() => {
      this.dialogRef.close();
      this.showSuccessSnackBar(this.translateService.instant('MOD_ACCOUNT.SUCCESS_MESSAGE_CHANGE_PASSWORD'));
    }, err => {
      this.onFormSubmissionError(err, this.changePasswordFormId);
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.changePasswordSubscription != null) {
      this.changePasswordSubscription.unsubscribe();
    }
    this.pageTitleService.setTitle(this.translateService.instant('MOD_ACCOUNT.HTML_TITLE'));
  }

}
