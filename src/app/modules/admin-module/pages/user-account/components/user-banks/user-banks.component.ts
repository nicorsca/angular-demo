import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {EmployeeBanksModel, UserModel} from '@common/models';
import {Subscription} from 'rxjs';
import {Status} from '@common/enums/status.enums';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {UserDetailService} from '@common/services/user-detail.service';

@Component({
  selector: 'app-user-banks',
  templateUrl: './user-banks.component.html',
  styleUrls: ['./user-banks.component.scss']
})
export class UserBanksComponent extends AbstractBaseComponent implements OnInit, OnDestroy {


  /**
   * Models
   */
  public bankModel: EmployeeBanksModel;

  /**
   * User Info
   */

  /**
   * Subscriptions
   */
  public userInfoSubscription: Subscription;

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * Enum
   */
  public status = Status;

  /**
   * User Info
   */
  public user: UserModel;

  /**
   * Constructor
   */
  constructor(protected injector: Injector,
              private userDetailService: UserDetailService
              ) {
    super(injector);
  }

  /**
   * Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get all contacts
   */
  public getModels(): void {
    this.userInfoSubscription = this.userDetailService.get().subscribe(userInfo => {
      this.user = userInfo;
      this.bankModel = this.user.bank;
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.userInfoSubscription != null) {
      this.userInfoSubscription.unsubscribe();
    }
  }


}
