import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAddressComponent } from './user-address.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {UserDetailService} from '@common/services';
import {MockUserDetailInfoService} from '@common/testing-resources/mock-services';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('UserAddressComponent', () => {
  let component: UserAddressComponent;
  let fixture: ComponentFixture<UserAddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserAddressComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: UserDetailService, useClass: MockUserDetailInfoService}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get model on init', () => {
    spyOn(component, 'getModels');
    component.ngOnInit();
    expect(component.getModels).toHaveBeenCalled();
  });
});
