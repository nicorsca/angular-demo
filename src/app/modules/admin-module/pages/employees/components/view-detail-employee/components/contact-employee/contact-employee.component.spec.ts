import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';
import {ContactEmployeeComponent} from './contact-employee.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {EmployeeContactsModel} from '@common/models';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {EmployeeContactsService} from '@common/services/employee-contacts.service';

describe('ContactEmployeeComponent', () => {
  let component: ContactEmployeeComponent;
  let fixture: ComponentFixture<ContactEmployeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContactEmployeeComponent
      ],
      providers: [
        {provide: EmployeeContactsService, useClass: MockEmployeesService},
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
            snapshot: {
              queryParams: {
                page: 1, sort_order: 'asc', per_page: 16
              },
              parent: {
                params: {
                  id: 1
                }
              }
            }
          }
        },
        { provide: MatDialogRef, useValue: MockMatDialog },
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create contact employee component', fakeAsync (async () => {
    expect(component).toBeTruthy();
  }));

  it('should set page title', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.PAGE_TITLE');
    flush();
  }));

  // it('should show error in case of error in retrieving data', fakeAsync (async () => {
  //   spyOn(component, 'showErrorSnackBar').and.callThrough();
  //   await component.onComponentReady();
  //   fixture.whenStable();
  //   expect(component.showErrorSnackBar).toHaveBeenCalled();
  // }));
  //
  // it('should show error when an error occurs while contact is being deleted', fakeAsync (async () => {
  //   spyOn(component, 'showErrorSnackBar');
  //   await component.onComponentReady();
  //   fixture.whenStable();
  //   await component.onDelete({} as EmployeeContactsModel);
  //   expect(component.showErrorSnackBar).toHaveBeenCalled();
  // }));

  // it('should reload in case of successful deletion', fakeAsync (async () => {
  //   spyOn(component, 'getModels');
  //   component.onDelete({id: 1} as unknown as EmployeeContactsModel);
  //   expect(component.getModels).toHaveBeenCalled();
  //   flush();
  // }));

  // it('should show error when an error occurs while contact status is being changed', fakeAsync (async () => {
  //   spyOn(component, 'showErrorSnackBar');
  //   await component.onComponentReady();
  //   fixture.whenStable();
  //   await component.onContactStatusToggle(1);
  //   expect(component.showErrorSnackBar).toHaveBeenCalled();
  // }));
  //
  // it ('should toggle status on status toggle success', fakeAsync (async () => {
  //   spyOn(component, 'showErrorSnackBar');
  //   component.onContactStatusToggle(2);
  //   expect(component.showErrorSnackBar).not.toHaveBeenCalled();
  // }));

  it('should open add-edit drawer on clicking new button', () => {
    spyOn(component, 'onAddEditContact');
    component.onComponentReady();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    fixture.whenStable();
    expect(component.onAddEditContact).toHaveBeenCalled();
  });
});
