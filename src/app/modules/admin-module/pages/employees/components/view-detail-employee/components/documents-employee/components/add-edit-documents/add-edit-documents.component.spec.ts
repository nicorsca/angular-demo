import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';
import { AddEditDocumentsComponent } from './add-edit-documents.component';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockFileUploadService, MockMatDialog} from '@common/testing-resources/mock-services';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {EmployeeDocumentsService} from '@common/services/employee-documents.service';
import {EmployeeDocumentsModel} from '@common/models';
import {Status} from '@common/enums/status.enums';
import {FileUploadService} from '@common/services';

describe('AddEditDocumentsComponent', () => {
  let component: AddEditDocumentsComponent;
  let fixture: ComponentFixture<AddEditDocumentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditDocumentsComponent ],
      providers: [
        {provide: EmployeeDocumentsService, useClass: MockEmployeesService},
        {provide: FileUploadService, useClass: MockFileUploadService},
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        { provide: MatDialogRef, useClass: MockMatDialog },
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set add page title in case of add', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.ADD_DOCUMENT_PAGE_TITLE');
    flush();
  }));

  it ('should set edit page title in case of edit', () => {
    component.modal = {content: {title: 'Passport', url: 'storage/test.png', thumbnail: 'storage/test_thumb.png', status: 2}, empId: 1} as unknown as EmployeeDocumentsModel;
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.EDIT_DOCUMENT_PAGE_TITLE');
  });

  it('should initialize form', () => {
    spyOn(component, 'createForm');
    component.ngOnInit();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should check initial form values in case of add', () => {
    const contactForm = component.documentForm;
    const contactFormValues = {
      title: '',
      file: '',
      url: '',
      thumbnail: '',
      type: '',
      status: Status.Published
    };
    expect(contactForm.value).toEqual(contactFormValues);
  });

  it('should set form values in case of edit', () => {
    component.modal = {content: {title: 'Passport', url: 'storage/test.png', thumbnail: 'storage/test_thumb.png', status: 2}, empId: 1} as unknown as EmployeeDocumentsModel;
    component.onComponentReady();
    const contactForm = component.documentForm;
    const contactFormValues = {
      title: 'Passport',
      file: '',
      url: 'storage/test.png',
      thumbnail: 'storage/test_thumb.png',
      type: '',
      status: Status.Published
    };
    expect(contactForm.value).toEqual(contactFormValues);
  });

  it('should disable submit button until conditions are met', () => {
    component.createForm();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toMatch('');
  });

  it('should activate button after conditions are met', () => {
    component.onComponentReady();
    component.documentForm.patchValue({title: 'Passport', file: 'test', status: 1});
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toBeDefined();
  });

  it ('should check if the file is of the correct format', () => {
    component.onComponentReady();
    component.checkFormat('txt');
    expect(component.fileUploadFlag).toBeFalse();
  });

  it('should show success snackbar in case of successful form submission', fakeAsync (async () => {
    spyOn(component, 'showSuccessSnackBar');
    component.documentForm.setValue({title: 'Passport', file: '', status: 2, url: 'storage/test.png', thumbnail: 'storage/test_thumb.png', type: 'image/svg+xml'});
    fixture.detectChanges();
    await component.saveEmployeeDocument();
    await fixture.whenStable();
    expect(component.showSuccessSnackBar).toHaveBeenCalled();
  }));
});
