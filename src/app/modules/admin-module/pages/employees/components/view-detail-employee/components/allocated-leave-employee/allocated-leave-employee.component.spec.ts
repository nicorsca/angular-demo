import {ComponentFixture, TestBed} from '@angular/core/testing';

import { AllocatedLeaveEmployeeComponent } from './allocated-leave-employee.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {AllocatedLeaveModel} from '@common/models/allocated-leaves.model';
import {AllocatedLeaveService} from '@common/services/allocated-leaves.service';
import {MockAllocatedLeaveService} from '@common/testing-resources/mock-services/MockAllocatedLeaveService';
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";


describe('AllocatedLeaveEmployeeComponent', () => {
  let component: AllocatedLeaveEmployeeComponent;
  let fixture: ComponentFixture<AllocatedLeaveEmployeeComponent>;
  let service: AllocatedLeaveService;
  const mockData = {
    created_at: '2021-08-24T09:05:28.000000Z',
    days: 10,
    end_date: '2023-07-15',
    id: 31,
    start_date: '2022-07-16',
    status: 1,
    title: 'test1',
    type: 'casual',
    updated_at: '2021-08-24T09:05:28.000000Z',
  } as unknown as AllocatedLeaveModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllocatedLeaveEmployeeComponent ],
      providers: [
        {provide: AllocatedLeaveService, useClass: MockAllocatedLeaveService},
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        { provide: MatDialogRef, useValue: MockMatDialog },
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocatedLeaveEmployeeComponent);

    service = TestBed.inject(AllocatedLeaveService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change page title to allocated leaves', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_ALLOCATED_LEAVE.PAGE_TITLE');
  });

  it('should set html title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_ALLOCATED_LEAVE.PAGE_TITLE');
  });

  it('should get model on init', () => {
    spyOn(component, 'getModels');
    component.ngOnInit();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should open delete confirmation dialog', () => {
    spyOn(component, 'onRemove');
    component.onFlyMenuAction('remove', mockData);
    expect(component.onRemove).toHaveBeenCalled();
  });

  it('should open drawer on click new btn', () => {
    spyOn(component, 'onAddEditLeave');
    const newBtn = fixture.debugElement.query(By.css('.new-btn'));
    newBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onAddEditLeave).toHaveBeenCalled();
  });

  it('should change status on toggle', () => {
    service.toggleStatusAllocatedLeave('1', mockData).subscribe(response => {
        expect(response.data).toEqual(mockData);
    });
  });
});
