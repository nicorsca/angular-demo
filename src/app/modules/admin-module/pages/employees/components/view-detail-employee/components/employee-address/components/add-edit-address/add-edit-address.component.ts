import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../../../AbstractBaseComponent';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AddressType} from '@common/enums/addressType.enum';
import {AddressService} from '@common/services/address.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AddressModel} from '@common/models/address.model';
import {Subscription} from 'rxjs';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-add-edit-address',
  templateUrl: './add-edit-address.component.html',
  styleUrls: ['./add-edit-address.component.scss']
})
export class AddEditAddressComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public addAddressSubscription: Subscription;
  public editAddressSubscription: Subscription;
  public addressForm: FormGroup;
  public addressFormID: string;
  public pageTitle = '';
  public oldTitle: string;
  public addressType = [
    {type: AddressType.temporary , label: this.translateService.instant('MOD_ADDRESS.TEMPORARY_ADDRESS_LABEL')},
    {type: AddressType.permanent , label:  this.translateService.instant('MOD_ADDRESS.PERMANENT_ADDRESS_LABEL')}
    ];
  constructor(injector: Injector,
              private addressService: AddressService,
              public dialogRef: MatDialogRef<AddEditAddressComponent>,
              @Inject(MAT_DIALOG_DATA) public modal: any) {
    super(injector);
    this.addressFormID = 'address-form-id';
    this.oldTitle = this.pageTitleService.getTitle().split('-')[1];
  }

  public onComponentReady(): void {
    if (this.modal.model) {
      this.pageTitle = this.translateService.instant('MOD_ADDRESS.MOD_ADD_EDIT_ADDRESS.PAGE_TITLE_EDIT');
    } else {
      this.pageTitle = this.translateService.instant('MOD_ADDRESS.MOD_ADD_EDIT_ADDRESS.PAGE_TITLE_ADD');
    }
    super.onComponentReady();
    this.createForm();
  }

  public createForm(): void {
    this.addressForm = new FormGroup({
      country: new FormControl(null, [Validators.required]),
      state: new FormControl(null, [Validators.required]),
      city: new FormControl(null, [Validators.required]),
      street: new FormControl(null, [Validators.required]),
      zip_code: new FormControl(null, [Validators.required, Validators.pattern('[1-9]{1}[0-9]{4}')]),
      type: new FormControl(AddressType.temporary, [Validators.required]),
    });

    if (this.modal.model){
      this.setFormData(this.modal.model);
    }
  }

  public setFormData(model: AddressModel): void {
    this.addressForm.patchValue(model);
    this.addressForm.controls.type.disable();
  }

  public onSubmitForm(): void {
    this.addAddressSubscription = this.addressService.saveAddress(this.modal.employeeId, this.addressForm.getRawValue(), this.modal.model)
      .subscribe(response => {
        this.dialogRef.close(response);
        NicoUtils.isNullOrUndefined(this.modal.model) ?
          this.showSuccessSnackBar(this.translateService.instant('MOD_ADDRESS.MOD_ADD_EDIT_ADDRESS.ADDRESS_ADD_SUCCESS_LABEL')) :
          this.showSuccessSnackBar(this.translateService.instant('MOD_ADDRESS.MOD_ADD_EDIT_ADDRESS.ADDRESS_EDIT_SUCCESS_LABEL'));
      }, error => {
        this.onFormSubmissionError(error, this.addressFormID);
      });
  }

  public ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.addAddressSubscription != null) {
      this.addAddressSubscription.unsubscribe();
    }
    if (this.editAddressSubscription != null) {
      this.editAddressSubscription.unsubscribe();
    }
  }
}
