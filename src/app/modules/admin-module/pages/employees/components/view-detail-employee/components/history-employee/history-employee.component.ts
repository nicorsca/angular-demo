import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '../../../../../../../../AbstractBaseComponent';
import { EmployeeHistoryService } from '@common/services';
import { PaginatedNicoCollection } from '@system/utilities';
import { EmployeeHistoriesModel } from '@common/models/employee-histories.model';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AddEmployeeHistoryComponent } from './components/add-employee-history/add-employee-history.component';

@Component({
  selector: 'app-history-employee',
  templateUrl: './history-employee.component.html',
  styleUrls: ['./history-employee.component.scss'],
})
export class HistoryEmployeeComponent
  extends AbstractBaseComponent
  implements OnInit, OnDestroy
{
  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * Models
   */
  public historyModel: PaginatedNicoCollection<EmployeeHistoriesModel>;

  /**
   * Subscriptions
   */
  public historySubscription: Subscription;

  /**
   * Employee activation status
   */
  public employeeActivationStatus: boolean;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    public employeesService: EmployeeHistoryService,
    protected dialog: MatDialog
  ) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant(
      'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_HISTORIES.PAGE_TITLE'
    );
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get History Information
   */
  public getModels(): void {
    this.historySubscription = this.employeesService
      .getHistories(this.activatedRoute.snapshot.parent.params.id)
      .subscribe((history) => {
        this.historyModel = history;
        if (this.historyModel.length > 0) {
          this.employeeActivationStatus =
            this.historyModel?.all().length % 2 === 0;
        }
      });
  }

  /**
   * Toggle Employee Activation Status
   */
  public onToggleEmployeeActivation(): void {
    const employeeHistory: EmployeeHistoryInterface = {
      empId: this.activatedRoute.snapshot.parent.params.id,
      activatedEmployee: this.employeeActivationStatus,
    };
    const modalDialog = this.dialog.open(AddEmployeeHistoryComponent, {
      data: employeeHistory,
      panelClass: ['modal-drawer-view', 'modal-xs'],
      disableClose: true,
    });
    modalDialog.afterClosed().subscribe((reload) => {
      if (reload) {
        this.getModels();
      }
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.historySubscription != null) {
      this.historySubscription.unsubscribe();
    }
  }
}

export interface EmployeeHistoryInterface {
  empId: string;
  activatedEmployee: boolean;
}
