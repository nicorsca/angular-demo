import {Component, Injector, OnInit} from '@angular/core';
import {AddEditSocialSecurityComponent} from './components/add-edit-social-security/add-edit-social-security.component';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {MatDialog} from '@angular/material/dialog';
import {SocialSecurityModel} from '@common/models/social-security.model';
import {SocialSecurityService} from '@common/services/social-security.service';
import {FlyMenuItemInterface} from '@system/components';
import {PaginatedNicoCollection} from '@system/utilities';
import {Status} from '@common/enums/status.enums';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-social-security',
  templateUrl: './social-security.component.html',
})
export class SocialSecurityComponent extends AbstractBaseComponent implements OnInit {

  public statusChangeSubscription: Subscription;
  public models: PaginatedNicoCollection<SocialSecurityModel>;
  public employeeId = this.activatedRoute.snapshot.parent.params;
  public pageTitle = '';
  public dialogRef: any;
  public status = Status;
  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {
      name: 'edit',
      label: this.translateService.instant('MOD_SOCIAL_SECURITY.EDIT_LABEL'),
      active: true,
      faIcon: 'far fa-pen'
    },
    {
      name: 'remove',
      label: this.translateService.instant('MOD_SOCIAL_SECURITY.REMOVE_LABEL'),
      active: true,
      faIcon: 'far fa-trash'
    }
  ];

  constructor(injector: Injector, public dialog: MatDialog, private socialSecurityService: SocialSecurityService) {
    super(injector);
  }


  public onFlyMenuAction(event: any, model: SocialSecurityModel): void {
    switch (event) {
      case 'remove':
        this.onRemove(model);
        break;

      case 'edit':
        this.onAddEditSecurity(model);
        break;
    }
  }

  public getFlyMenuAdOns(): FlyMenuItemInterface[] {
    return [...this.flyMenuAddOns];
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_SOCIAL_SECURITY.HTML_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  public onAddEditSecurity(security?: SocialSecurityModel): void {
    this.openDrawerView(this.employeeId.id, security);
  }

  public openDrawerView(id: string, model: SocialSecurityModel ): void{
    const modalDialog = this.dialog.open(AddEditSocialSecurityComponent,
      {panelClass: ['modal-drawer-view', 'modal-xs'], disableClose: true, data: { model , employeeId: this.employeeId.id }});
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
      }
    });
  }

  public getModels(): void {
    this.socialSecurityService.getSocialSecurity(this.employeeId.id, this.getFilterHttpQueryParams()).subscribe(models => {
        this.models = models;
      });
  }


  public onRemove(model: SocialSecurityModel): void {
    this.dialogRef = this.openConfirmationDialogBox({
      title: this.translateService.instant('MOD_SOCIAL_SECURITY.DELETE_TITLE'),
      message: this.translateService.instant('MOD_SOCIAL_SECURITY.DELETE_MESSAGE')
    });
    this.dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.socialSecurityService.deleteSocialSecurity(this.employeeId.id, model).subscribe(response => {
          this.dialogRef.close(response);
          this.getModels();
        });
      }
    });
  }

  public onSwitchValueChange(model: SocialSecurityModel): void {
    this.statusChangeSubscription = this.socialSecurityService.changeSocialSecurityStatus(this.employeeId.id, model)
      .subscribe((state) => {
        this.getModels();
      });
  }
}
