import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Status} from '@common/enums/status.enums';
import {AbstractBaseComponent} from '../../../../../../../../../../AbstractBaseComponent';
import {NicoStorageService} from '@system/services';
import {AllocatedLeaveService} from '@common/services/allocated-leaves.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {FiscalYear} from '@common/enums/fiscalYear.enum';
import {NicoUtils} from '@system/utilities';
import * as moment from 'moment';

@Component({
  selector: 'app-add-edit-allocated-leave',
  templateUrl: './add-edit-allocated-leave.component.html',
})
export class AddEditAllocatedLeaveComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public allocatedLeaveForm: FormGroup;
  public saveAllocatedLeaveSubscription: Subscription;
  public allocatedLeaveFormID: string;
  public leaveType: any;
  public switchFiscalYear: boolean;
  public pageTitle = '';
  public oldTitle: string;
  public fiscalYear = [
    {
      year: this.translateService.instant('MOD_ALLOCATED_LEAVE.MOD_ADD_EDIT_ALLOCATED_LEAVE.CURRENT_YEAR_LABEL'),
      value: FiscalYear.Current_fiscal_year
    },
    {
      year: this.translateService.instant('MOD_ALLOCATED_LEAVE.MOD_ADD_EDIT_ALLOCATED_LEAVE.NEXT_YEAR_LABEL'),
      value: FiscalYear.Next_fiscal_year
    },
  ];

  constructor(
    injector: Injector,
    private nicoStorageService: NicoStorageService,
    private allocatedLeaveService: AllocatedLeaveService,
    @Inject(MAT_DIALOG_DATA) public modal: any,
    public dialogRef: MatDialogRef<AddEditAllocatedLeaveComponent>,
  ) {
    super(injector);
    const {leaves_type} = JSON.parse(this.nicoStorageService.getItem('settings'));
    this.leaveType = leaves_type;
    this.switchFiscalYear = false;
    this.allocatedLeaveFormID = 'allocated-leave-id';
    this.oldTitle = this.pageTitleService.getTitle().split('-')[1];
  }

  public onComponentReady(): void {
    if (this.modal.content) {
      this.pageTitle = this.translateService.instant('MOD_ALLOCATED_LEAVE.MOD_ADD_EDIT_ALLOCATED_LEAVE.PAGE_TITLE_EDIT');
    } else {
      this.pageTitle = this.translateService.instant('MOD_ALLOCATED_LEAVE.MOD_ADD_EDIT_ALLOCATED_LEAVE.PAGE_TITLE_ADD');
    }
    super.onComponentReady();
    this.onCreateForm();
  }

  public onCreateForm(): void {
    this.allocatedLeaveForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      days: new FormControl('', [Validators.required, Validators.pattern(/^[1-9]\d*$/)]),
      type: new FormControl('', [Validators.required]),
      current_fiscal_year: new FormControl(1, [Validators.required]),
      status: new FormControl(Status.Published, [Validators.required]),
    });
    if (this.modal.content) {
      this.allocatedLeaveForm.patchValue(this.modal.content);
      this.allocatedLeaveForm.get('current_fiscal_year')
        .setValue(this.modal.content.start_date <= this.getCurrentDate() ? FiscalYear.Current_fiscal_year : FiscalYear.Next_fiscal_year);
      this.allocatedLeaveForm.controls.current_fiscal_year.disable();
      this.switchState = this.modal.content.status === Status.Published;
    }
  }

  public getCurrentDate(): any {
    return moment().format('YYYY-MM-DD');
  }

  public onSubmitForm(): void {
    this.saveAllocatedLeaveSubscription = this.allocatedLeaveService
      .saveAllocatedLeave(this.modal.empId, this.allocatedLeaveForm.getRawValue(), this.modal.content)
      .subscribe(response => {
        this.dialogRef.close(response);
        NicoUtils.isNullOrUndefined(this.modal.content) ?
          this.showSuccessSnackBar(this.translateService.instant('MOD_ALLOCATED_LEAVE.MOD_ADD_EDIT_ALLOCATED_LEAVE.ADD_SUCCESS_LABEL')) :
          this.showSuccessSnackBar(this.translateService.instant('MOD_ALLOCATED_LEAVE.MOD_ADD_EDIT_ALLOCATED_LEAVE.EDIT_SUCCESS_LABEL'));

      }, error => {
        this.onFormSubmissionError(error, this.allocatedLeaveFormID);
      });
  }

  public ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.saveAllocatedLeaveSubscription != null) {
      this.saveAllocatedLeaveSubscription.unsubscribe();
    }
  }
}
