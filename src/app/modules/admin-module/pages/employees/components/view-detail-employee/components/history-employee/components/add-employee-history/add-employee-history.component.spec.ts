import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { AddEmployeeHistoryComponent } from './add-employee-history.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {EmployeeHistoriesModel} from '@common/models/employee-histories.model';
import {MockHistoryModel} from '@common/testing-resources/mock-model/MockHistoryModel';
import {EmployeeDocumentsModel} from '@common/models';
import {EmployeeHistoryService} from '@common/services';

describe('AddEmployeeHistoryComponent', () => {
  let component: AddEmployeeHistoryComponent;
  let fixture: ComponentFixture<AddEmployeeHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEmployeeHistoryComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        {provide: EmployeeHistoriesModel, useClass: MockHistoryModel},
        { provide: MatDialogRef, useClass: MockMatDialog },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        {provide: EmployeeHistoryService, useClass: MockEmployeesService},
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEmployeeHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set deactivate page title in case of employee deactivation', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_HISTORIES.DEACTIVATE_EMPLOYEE_LABEL');
    flush();
  }));

  it ('should set activate page title in case of employee activation', () => {
    component.modal = {content: {title: 'Passport', url: 'storage/test.png', thumbnail: 'storage/test_thumb.png', status: 2}, empId: 1} as unknown as EmployeeDocumentsModel;
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_HISTORIES.DEACTIVATE_EMPLOYEE_LABEL');
  });

  it('should initialize form', () => {
    spyOn(component, 'createForm');
    component.ngOnInit();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should check initial form values in case of add', () => {
    const contactForm = component.historyForm;
    const contactFormValues = {
      comment: '',
      file: '',
      url: '',
      thumbnail: '',
      type: ''
    };
    expect(contactForm.value).toEqual(contactFormValues);
  });

  it('should disable submit button until conditions are met', () => {
    component.createForm();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toMatch('');
  });

  it('should activate button after conditions are met', () => {
    component.onComponentReady();
    component.historyForm.patchValue({comment: 'Test deactivation', file: 'test'});
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toBeDefined();
  });

  it('should show success snackbar in case of success', fakeAsync (async () => {
    spyOn(component, 'showSuccessSnackBar');
    component.historyForm.setValue({comment: 'Passport', file: '', url: 'test', thumbnail: 'storage/test_thumb.png', type: 'image/svg+xml'});
    fixture.detectChanges();
    await component.saveHistoryDocument();
    await fixture.whenStable();
    expect(component.showSuccessSnackBar).toHaveBeenCalled();
  }));

  it ('should check if the file is of the correct format', () => {
    component.onComponentReady();
    component.checkFormat('txt');
    expect(component.fileUploadFlag).toBeFalse();
  });

});
