import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { BankDetailEmployeeComponent } from './bank-detail-employee.component';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {EmployeeBanksService} from '@common/services';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('BankDetailEmployeeComponent', () => {
  let component: BankDetailEmployeeComponent;
  let fixture: ComponentFixture<BankDetailEmployeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankDetailEmployeeComponent ],
      providers: [
        {provide: EmployeeBanksService, useClass: MockEmployeesService},
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        { provide: MatDialogRef, useValue: MockMatDialog },
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankDetailEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create bank details employee component', fakeAsync (async () => {
    expect(component).toBeTruthy();
  }));

  it('should set page title', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.PAGE_TITLE');
    flush();
  }));

  // it('should show error when an error occurs while bank status is being changed', fakeAsync (async () => {
  //   spyOn(component, 'showErrorSnackBar');
  //   await component.onComponentReady();
  //   fixture.whenStable();
  //   await component.onBankStatusToggle({id: 2, name: 'Himalayan Bank Ltd.', account_name: 'Jane Doe', account_number: '110022003'} as unknown as EmployeeBanksModel);
  //   fixture.whenStable();
  //   expect(component.showErrorSnackBar).toHaveBeenCalled();
  //   flush();
  // }));

  // it ('should toggle status on status toggle success', fakeAsync (async () => {
  //   spyOn(component, 'showErrorSnackBar');
  //   component.onBankStatusToggle({id: 1, name: 'Himalayan Bank Ltd.', account_name: 'Jane Doe', account_number: '110022003'} as unknown as EmployeeBanksModel);
  //   expect(component.showErrorSnackBar).not.toHaveBeenCalled();
  // }));

  it('should open add-edit drawer on clicking new button', () => {
    spyOn(component, 'onAddEditBank');
    component.onComponentReady();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    fixture.whenStable();
    expect(component.onAddEditBank).toHaveBeenCalled();
  });
});
