import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';
import { AddEditAllocatedLeaveComponent } from './add-edit-allocated-leave.component';
import {MockAllocatedLeaveService} from '@common/testing-resources/mock-services/MockAllocatedLeaveService';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {NicoStorageService} from '@system/services';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AllocatedLeaveService} from '@common/services/allocated-leaves.service';
import {AllocatedLeaveModel} from '@common/models/allocated-leaves.model';

describe('AddEditAllocatedLeaveComponent', () => {
  let component: AddEditAllocatedLeaveComponent;
  let fixture: ComponentFixture<AddEditAllocatedLeaveComponent>;
  let storageService: NicoStorageService;
  let service: AllocatedLeaveService;
  const mockData = {
    created_at: '2021-08-24T09:05:28.000000Z',
    days: 10,
    end_date: '2023-07-15',
    id: 31,
    start_date: '2022-07-16',
    status: 1,
    title: 'test1',
    type: 'casual',
    updated_at: '2021-08-24T09:05:28.000000Z'} as unknown as AllocatedLeaveModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditAllocatedLeaveComponent ],
      providers: [
        {provide: AllocatedLeaveService, useClass: MockAllocatedLeaveService},
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        { provide: MatDialogRef, useValue: MockMatDialog },
        { provide: MAT_DIALOG_DATA, useValue: {data: {}} }
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    storageService = TestBed.inject(NicoStorageService);
    storageService.setItem('settings',  JSON.stringify({leave_type: ['a', 'b', 'c']}));
    service = TestBed.inject(AllocatedLeaveService);
    fixture = TestBed.createComponent(AddEditAllocatedLeaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change page title to add allocated leaves', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_ALLOCATED_LEAVE.MOD_ADD_EDIT_ALLOCATED_LEAVE.PAGE_TITLE_ADD');
  });

  it('should change page title to edit allocated leaves', () => {
    component.modal = {content: {mockData}};
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_ALLOCATED_LEAVE.MOD_ADD_EDIT_ALLOCATED_LEAVE.PAGE_TITLE_EDIT');
  });

  it('should set add page title in case of add', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_ALLOCATED_LEAVE.MOD_ADD_EDIT_ALLOCATED_LEAVE.PAGE_TITLE_ADD');
    flush();
  }));

  it('should fill data on edit allocated leave', () => {
    const dummyData = {
      days: 10,
      status: 1,
      title: 'test1',
      type: 'casual',
    };
    component.modal = {content: {
     ...dummyData
      }};
    component.onComponentReady();
    expect(dummyData).toEqual(component.allocatedLeaveForm.value);
  });
  it('should be empty form on init', () => {
    component.ngOnInit();
    const dummyData = {
      current_fiscal_year: 1,
      days: '',
      status: 2,
      title: '',
      type: ''
    };
    expect(dummyData).toEqual(component.allocatedLeaveForm.value);
  });

  it('should submit form  on click save button', () => {
    spyOn(component, 'onSubmitForm');
    const saveBtn = fixture.debugElement.query(By.css('.save'));
    saveBtn.triggerEventHandler('click', null);
    expect(component.onSubmitForm).toHaveBeenCalled();
  });

  it('should not let user to edit fiscal year', () => {
    component.modal = {content: {mockData}};
    component.onComponentReady();
    fixture.detectChanges();
    expect(component.allocatedLeaveForm.controls.current_fiscal_year.disabled).toBeTruthy();
  });

  it('should post data on save data', () => {
    const dummyData = {
      current_fiscal_year: false,
      days: '10',
      status: 1,
      title: 'Sick',
      type: 'sick'
    } as unknown as AllocatedLeaveModel;
    service.saveAllocatedLeave('1', dummyData).subscribe(response => {
      expect(response).toEqual(mockData);
    });
  });

  it('should not post data on error data ', () => {
    const dummyData = {} as unknown as AllocatedLeaveModel;
    service.saveAllocatedLeave('1', dummyData).subscribe(response => {
      expect(response).toEqual(mockData);
    });
  });

});
