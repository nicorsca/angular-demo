import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {PaginatedNicoCollection} from '@system/utilities';
import {EmployeeBanksModel, EmployeeContactsModel, UserModel} from '@common/models';
import {Subscription} from 'rxjs';
import {Status} from '@common/enums/status.enums';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FlyMenuItemInterface} from '@system/components';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AddEditBankComponent} from './components/add-edit-bank/add-edit-bank.component';
import {EmployeeBanksService} from '@common/services/employee-banks.service';

@Component({
  selector: 'app-bank-detail-employee',
  templateUrl: './bank-detail-employee.component.html',
  styleUrls: ['./bank-detail-employee.component.scss']
})
export class BankDetailEmployeeComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Models
   */
  public bankModel: PaginatedNicoCollection<EmployeeBanksModel>;

  /**
   * User
   */
  public user: UserModel;

  /**
   * Subscriptions
   */
  public bankInfoSubscription: Subscription;

  public contactDeleteSubscription: Subscription;

  public statusToggleSubscription: Subscription;

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * Enum
   */
  public status = Status;

  /**
   * Fly Menu Addons
   */
  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {
      name: 'edit',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.EDIT_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-pen'
    },
    {
      name: 'remove',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.REMOVE_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-trash'
    }
  ];

  /**
   * MatDialogRef
   */
  public dialogRef: MatDialogRef<EmployeeContactsModel>;

  /**
   * Constructor
   */
  constructor(protected injector: Injector, public employeesService: EmployeeBanksService, protected dialog: MatDialog) {
    super(injector);
  }

  /**
   * Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get all banks
   */
  public getModels(): void {
    this.bankInfoSubscription = this.employeesService.getBanks(this.activatedRoute.snapshot.parent.params.id).subscribe(banks => {
      this.bankModel = banks;
    });
  }

  /**
   * Fly menu cases
   */
  public onFlyMenuAction(event: any, model: EmployeeBanksModel): void {
    switch (event) {
      case 'remove':
        this.onDelete(model);
        break;

      case 'edit':
        this.onAddEditBank(model);
        break;
    }
  }

  /**
   * Add/edit new contact (Open drawer)
   */
  public onAddEditBank(model?: EmployeeBanksModel): void {
    const modalDialog = this.dialog.open(AddEditBankComponent, {
      data: {content: model, empId: this.activatedRoute.snapshot.parent.params.id},
      panelClass: ['modal-drawer-view', 'modal-xs'],
      disableClose: true
    });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
      }
    });
  }

  /**
   * Fly Menu addons
   */
  public getFlyMenuAdOns(): FlyMenuItemInterface[] {
    return [...this.flyMenuAddOns];
  }

  /**
   * On Contact delete
   */
  public onDelete(model: EmployeeBanksModel): void {
    this.dialogRef = this.openConfirmationDialogBox({
      title: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.DELETE_MODAL_TITLE'),
      message: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.DELETE_MODAL_MESSAGE')
    });
    this.dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.contactDeleteSubscription = this.employeesService.deleteBank(this.activatedRoute.snapshot.parent.params.id, model)
          .subscribe(response => {
            this.dialogRef.close(true);
            this.getModels();
          });
      }
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.bankInfoSubscription != null) {
      this.bankInfoSubscription.unsubscribe();
    }
    if (this.statusToggleSubscription != null) {
      this.statusToggleSubscription.unsubscribe();
    }
    if (this.contactDeleteSubscription != null) {
      this.contactDeleteSubscription.unsubscribe();
    }
  }
}
