import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { AddEditContactComponent } from './add-edit-contact.component';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {EmployeeContactsModel} from '@common/models';
import {Status} from '@common/enums/status.enums';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {EmployeeContactsService} from '@common/services/employee-contacts.service';

describe('AddEditContactComponent', () => {
  let component: AddEditContactComponent;
  let fixture: ComponentFixture<AddEditContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditContactComponent ],
      providers: [
        {provide: EmployeeContactsService, useClass: MockEmployeesService},
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        { provide: MatDialogRef, useClass: MockMatDialog },
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set add page title in case of add', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.ADD_CONTACT_PAGE_TITLE');
    flush();
  }));

  it ('should set edit page title in case of edit', () => {
    component.modal = {content: {number: '1111111111', type: 2, status: 2}, empId: 1} as unknown as EmployeeContactsModel;
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.EDIT_CONTACT_PAGE_TITLE');
  });

  it('should initialize form', () => {
    spyOn(component, 'createForm');
    component.ngOnInit();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should check initial form values in case of add', () => {
    const contactForm = component.contactForm;
    const contactFormValues = {
      number: '',
      type: component.contactType.Personal,
      status: Status.Published
    };
    expect(contactForm.value).toEqual(contactFormValues);
  });

  it('should set form values in case of edit', () => {
    component.modal = {content: {number: '1111111111', type: 2, status: 2}, empId: 1} as unknown as EmployeeContactsModel;
    component.onComponentReady();
    const contactForm = component.contactForm;
    const contactFormValues = {
      number: '1111111111',
      type: component.contactType.Home,
      status: Status.Published
    };
    expect(contactForm.value).toEqual(contactFormValues);
  });

  it('should call showSuccessSnackBar in case of success', fakeAsync(async () => {
    spyOn(component, 'showSuccessSnackBar');
    component.onComponentReady();
    component.contactForm.setValue({number: '1111111111', type: 1, status: 2});
    await component.onFormSubmit();
    await fixture.whenStable();
    expect(component.showSuccessSnackBar).toHaveBeenCalled();
    flush();
  }));

  it('should disable submit button until conditions are met', () => {
    component.createForm();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toMatch('');
  });

  it('should activate button after conditions are met', () => {
    component.onComponentReady();
    component.contactForm.patchValue({number: '1111111111', type: 1, status: 1});
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toBeDefined();
  });
});
