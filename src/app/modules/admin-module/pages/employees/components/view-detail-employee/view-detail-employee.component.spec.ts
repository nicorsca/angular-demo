import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDetailEmployeeComponent } from './view-detail-employee.component';

import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {EmployeesService} from '@common/services/employees.service';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {BreadcrumbService} from 'xng-breadcrumb';
import {MockBreadcrumService} from '@common/testing-resources/mock-services/MockBreadcrumService';

describe('ViewDetailEmployeeComponent', () => {
  let component: ViewDetailEmployeeComponent;
  let fixture: ComponentFixture<ViewDetailEmployeeComponent>;
  let pageTitleService: NicoPageTitle;
  let service: EmployeesService;
  let router: Router;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewDetailEmployeeComponent ],
      providers: [
        {provide: EmployeesService, useClass: MockEmployeesService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useValue: MockMatDialog},
        // {provide: ActivatedRoute, useValue:  { data:  {value: {employeeResolver: {id: 1}}}}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useClass: MockBreadcrumService}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDetailEmployeeComponent);
    component = fixture.componentInstance ;
    service = TestBed.inject(EmployeesService);
    pageTitleService = TestBed.inject(NicoPageTitle);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
