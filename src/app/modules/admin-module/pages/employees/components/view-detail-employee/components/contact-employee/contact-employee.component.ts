import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {EmployeeContactsModel} from '@common/models';
import {FlyMenuItemInterface} from '@system/components';
import {Status} from '@common/enums/status.enums';
import {Subscription} from 'rxjs';
import {PaginatedNicoCollection} from '@system/utilities';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AddEditContactComponent} from './components/add-edit-contact/add-edit-contact.component';
import {EmployeeContactsService} from '@common/services/employee-contacts.service';

@Component({
  selector: 'app-contact-employee',
  templateUrl: './contact-employee.component.html',
  styleUrls: ['./contact-employee.component.scss']
})
export class ContactEmployeeComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Models
   */
  public contactModel: PaginatedNicoCollection<EmployeeContactsModel>;

  /**
   * Subscriptions
   */
  public contactSubscription: Subscription;

  public contactDeleteSubscription: Subscription;

  public statusChangeSubscription: Subscription;

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * Enum
   */
  public status = Status;

  /**
   * Fly menu addons
   */
  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {
      name: 'edit',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.EDIT_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-pen'
    },
    {
      name: 'remove',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.REMOVE_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-trash'
    }
  ];

  /**
   * MatDialogRef
   */
  public dialogRef: MatDialogRef<EmployeeContactsModel>;

  /**
   * Constructor
   */
  constructor(protected injector: Injector, public employeesService: EmployeeContactsService, protected dialog: MatDialog) {
    super(injector);
  }

  /**
   * Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get all contacts
   */
  public getModels(): void {
    this.contactSubscription = this.employeesService.getContacts(this.activatedRoute.snapshot.parent.params.id).subscribe(contacts => {
      this.contactModel = contacts;
    });
  }

  /**
   * Fly menu cases
   */
  public onFlyMenuAction(event: any, model: EmployeeContactsModel): void {
    switch (event) {
      case 'remove':
        this.onDelete(model);
        break;

      case 'edit':
        this.onAddEditContact(model);
        break;
    }
  }

  /**
   * Add/edit new contact (Open drawer)
   */
  public onAddEditContact(model?: EmployeeContactsModel): void {
    const modalDialog = this.dialog.open(AddEditContactComponent, {
      data: {content: model, empId: this.activatedRoute.snapshot.parent.params.id},
      panelClass: ['modal-drawer-view', 'modal-xs'],
      disableClose: true
    });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
      }
    });
  }

  /**
   * Get Fly Menu addons
   */
  public getFlyMenuAdOns(): FlyMenuItemInterface[] {
    return [...this.flyMenuAddOns];
  }

  /**
   * On Contact delete
   */
  public onDelete(model: EmployeeContactsModel): void {
    this.dialogRef = this.openConfirmationDialogBox({
      title: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.DELETE_MODAL_TITLE'),
      message: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.DELETE_MODAL_MESSAGE')
    });
    this.dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.contactDeleteSubscription = this.employeesService.deleteContact(this.activatedRoute.snapshot.parent.params.id, model).subscribe(response => {
          this.dialogRef.close(true);
          this.getModels();
        });
      }
    });
  }

  /**
   * Toggle publish status
   */
  public onContactStatusToggle(model): void {
    this.statusChangeSubscription = this.employeesService.toggleContactStatus(this.activatedRoute.snapshot.parent.params.id, model)
      .subscribe((state) => {
        this.getModels();
      });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.contactSubscription != null) {
      this.contactSubscription.unsubscribe();
    }
    if (this.contactDeleteSubscription != null) {
      this.contactDeleteSubscription.unsubscribe();
    }
    if (this.statusChangeSubscription != null) {
      this.statusChangeSubscription.unsubscribe();
    }
  }

}
