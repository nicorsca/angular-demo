import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AbstractBaseComponent} from '../../../../../../../../../../AbstractBaseComponent';
import {Status} from '@common/enums/status.enums';
import {DateUtility} from '@common/utilities/date-utility';
import {SocialSecurityService} from '@common/services/social-security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SocialSecurityModel} from '@common/models/social-security.model';
import {Subscription} from 'rxjs';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-add-edit-social-security',
  templateUrl: './add-edit-social-security.component.html',
})
export class AddEditSocialSecurityComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public addSecuritySubscription: Subscription;
  public socialSecurityForm: FormGroup;
  public socialSecurityFormID: string;
  public switchState: boolean;
  public today = new Date();
  public pageTitle = '';
  public oldTitle: string;

  constructor(injector: Injector,
              private socialSecurityService: SocialSecurityService,
              public dialogRef: MatDialogRef<AddEditSocialSecurityComponent>,
              @Inject(MAT_DIALOG_DATA) public modal: any
  ) {
    super(injector);
    this.today.setDate(this.today.getDate());
    this.switchState = false;
    this.oldTitle = this.pageTitleService.getTitle().split('-')[1];
    this.socialSecurityFormID = 'socialSecurityFormID';
  }

  public onComponentReady(): void {
    if (this.modal.model) {
      this.pageTitle = this.translateService.instant('MOD_SOCIAL_SECURITY.MOD_ADD_EDIT_SOCIAL_SECURITY.HTML_TITLE_EDIT');
    } else {
      this.pageTitle = this.translateService.instant('MOD_SOCIAL_SECURITY.MOD_ADD_EDIT_SOCIAL_SECURITY.HTML_TITLE_ADD');
    }
    super.onComponentReady();
    this.onCreateForm();
  }

  public onCreateForm(): void {
    this.socialSecurityForm = new FormGroup({
      title: new FormControl(null, [Validators.required]),
      number: new FormControl(null, [Validators.required]),
      start_date: new FormControl(null, [Validators.required]),
      end_date: new FormControl(null),
      status: new FormControl(Status.Published)
    });

    if (this.modal.model) {
      this.setFormData(this.modal.model);
    }
  }

  private setFormData(model: SocialSecurityModel): void {
    this.socialSecurityForm.patchValue(model);
    this.socialSecurityForm.get('start_date').setValue(DateUtility.getMomentObject(model.start_date));
    this.socialSecurityForm.get('end_date').setValue(model.end_date ? DateUtility.getMomentObject(model.end_date) : null);
    model.status === Status.Published ? this.switchState = true : this.switchState = false;
  }

  public onSwitchValueChange(): void {
    this.switchState = !this.switchState;
    if (this.socialSecurityForm.value.status === Status.Published) {
      this.socialSecurityForm.get('status').setValue(Status.Unpublished);
    } else {
      this.socialSecurityForm.get('status').setValue(Status.Published);
    }
  }

  public onSubmitForm(): void {
    this.addSecuritySubscription = this.socialSecurityService
      .saveSocialSecurity(this.modal.employeeId, this.socialSecurityForm.value, this.modal.model)
      .subscribe(response => {
        this.dialogRef.close(response);
        NicoUtils.isNullOrUndefined(this.modal.model) ?
          this.showSuccessSnackBar(this.translateService.instant('MOD_SOCIAL_SECURITY.MOD_ADD_EDIT_SOCIAL_SECURITY.ADD_SUCCESS_LABEL')) :
          this.showSuccessSnackBar(this.translateService.instant('MOD_SOCIAL_SECURITY.MOD_ADD_EDIT_SOCIAL_SECURITY.EDIT_SUCCESS_LABEL'));
      }, error => {
        this.onFormSubmissionError(error, this.socialSecurityFormID);
      });
  }

  public ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.addSecuritySubscription != null) {
      this.addSecuritySubscription.unsubscribe();
    }
  }
}
