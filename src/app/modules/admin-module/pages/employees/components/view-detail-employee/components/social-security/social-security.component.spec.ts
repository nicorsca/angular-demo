import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import { SocialSecurityComponent } from './social-security.component';
import {EmployeesService} from '@common/services/employees.service';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserModule, By} from '@angular/platform-browser';
import {PaginatedNicoCollection} from '@system/utilities';
import {SocialSecurityModel} from '@common/models/social-security.model';
import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {of} from 'rxjs';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';
import {BreadcrumbService} from 'xng-breadcrumb';

describe('SocialSecurityComponent', () => {
  let component: SocialSecurityComponent;
  let fixture: ComponentFixture<SocialSecurityComponent>;
  const mockData = {
    created_at: '2021-08-13T05:13:46.000000Z',
    end_date: '',
    id: 17,
    number: '123321123',
    start_date: '2006-05-23',
    status: 1,
    title: 'Bachelors of Information and Management',
    updated_at: '2021-08-13T06:55:34.000000Z'
  } as unknown as SocialSecurityModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SocialSecurityComponent ],
      providers: [
        {provide: EmployeesService, useClass: MockEmployeesService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useValue: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {}},
        {provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
        {provide: ActivatedRoute, useValue:  { snapshot: {parent: {params: {id: 1}}}}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useValue: undefined}
      ],
      imports: [
        CommonModule,
        RouterTestingModule,
        TranslateModule,
        BrowserModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialSecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get model on init', () => {
    spyOn(component, 'getModels');
    component.ngOnInit();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should change page title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_SOCIAL_SECURITY.HTML_TITLE');
  });

  it('should open drawer on add new social security', () => {
    spyOn(component, 'onAddEditSecurity');
    component.models = new PaginatedNicoCollection<SocialSecurityModel>();
    component.models.push(mockData);
    fixture.detectChanges();
    const newBtn = fixture.debugElement.query(By.css('.new-btn'));
    newBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onAddEditSecurity).toHaveBeenCalled();
  });

  it('should open drawer on edit social security', () => {
    spyOn(component, 'onAddEditSecurity');
    component.onFlyMenuAction('edit', mockData);
    expect(component.onAddEditSecurity).toHaveBeenCalled();
  });

  it('should open confirmation on delete social security', () => {
    spyOn(component, 'onRemove');
    component.onFlyMenuAction('remove', mockData);
    expect(component.onRemove).toHaveBeenCalled();
  });

  it('should reload on close drawer and data save', fakeAsync (() => {
      spyOn(component.dialog, 'open')
        .and
        .returnValue({afterClosed: () => of(true)} as MatDialogRef<any>);
      spyOn(component, 'getModels');
      fixture.detectChanges();
      component.openDrawerView('1', mockData);
      fixture.detectChanges();
      fixture.whenStable();
      expect(component.getModels).toHaveBeenCalled();
  }));

  it('should not display end date if end date is not assigned', () => {
    component.models = new PaginatedNicoCollection<SocialSecurityModel>();
    component.models.push(mockData);
    const endDate = fixture.debugElement.query(By.css('.end-date'));
    expect(endDate).not.toBeTruthy();
  });

  it('should not show if end date is undefined', () => {
    component.models = new PaginatedNicoCollection<SocialSecurityModel>();
    component.models.push(mockData);
    fixture.detectChanges();
    const endDate = fixture.debugElement.query(By.css('.end-date'));
    expect(endDate).not.toBeTruthy();
  });
});
