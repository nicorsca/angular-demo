import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { DocumentsEmployeeComponent } from './documents-employee.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {EmployeeDocumentsService} from '@common/services/employee-documents.service';
import {Observable, of} from 'rxjs';
import {EmployeeDocumentsModel} from '@common/models';
import {PaginatedNicoCollection} from '@system/utilities';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {MatDialogRef} from '@angular/material/dialog';

describe('DocumentsEmployeeComponent', () => {
  let component: DocumentsEmployeeComponent;
  let fixture: ComponentFixture<DocumentsEmployeeComponent>;

  const mockClose = {
    close: () => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentsEmployeeComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        {provide: EmployeeDocumentsService, useClass: MockEmployeesService},
        {provide: MatDialogRef, useClass: MockMatDialog},
        {
          provide: MAT_DIALOG_DATA,
          useValue: []
        }
      ],
      imports: [
        TranslateModule,
        RouterTestingModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set page title', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.PAGE_TITLE');
    flush();
  }));

  it('should open add-edit drawer on clicking new button', () => {
    spyOn(component, 'onAddEditDocument');
    component.onComponentReady();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    fixture.whenStable();
    expect(component.onAddEditDocument).toHaveBeenCalled();
  });

  it('should retrieve data', fakeAsync (async () => {
    spyOn(component.employeesService, 'getDocuments').and.returnValue(
      of({id: 1, title: 'test', url: 'test.png'}
      ) as unknown as Observable<PaginatedNicoCollection<EmployeeDocumentsModel>>);
    component.onComponentReady();
    fixture.whenStable();
    expect(component.documentModel).not.toBeNull();
  }));

  // it('should reload in case of successful deletion', fakeAsync (async () => {
  //   spyOn(component, 'getModels');
  //   component.onDelete({} as unknown as EmployeeDocumentsModel);
  //   expect(component.getModels).toHaveBeenCalled();
  //   flush();
  // }));
});
