import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { AddEditAddressComponent } from './add-edit-address.component';
import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {AddressModel} from '@common/models/address.model';
import {AddressService} from '@common/services/address.service';
import {MockAddressService} from '@common/testing-resources/mock-services/MockAddressService';
import {HttpErrorResponse} from '@angular/common/http';
import {BreadcrumbService} from 'xng-breadcrumb';
import {EmployeeContactsModel} from '@common/models';

describe('AddEditAddressComponent', () => {
  let component: AddEditAddressComponent;
  let fixture: ComponentFixture<AddEditAddressComponent>;
  let service: AddressService;
  const mockData = {
    id : 1,
    country: 'Nepal',
    state: 3,
    city: 'ktm',
    street: 'bijeshwori',
    zip_code: 44600,
    type: 2,
    created_at: '',
    updated_at: ''
  } as unknown as AddressModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditAddressComponent ],
      providers: [
        {provide: AddressService, useClass: MockAddressService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useValue: MockMatDialog},
        {provide: ActivatedRoute, useValue:  { snapshot: {parent: {params: {id: '1'}}}}},
        {provide: HttpErrorResponse, useValue: {headers: {}, error: {message: 'Test', status: '417'}}},
        {provide: MatDialogRef, useValue: component},
        {provide: MAT_DIALOG_DATA, useValue: {model: null}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useValue: undefined}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule,
        CommonModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]


    })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditAddressComponent);
    service = TestBed.inject(AddressService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change html title add address', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_ADDRESS.MOD_ADD_EDIT_ADDRESS.PAGE_TITLE_ADD');
  });

  it ('should set edit page title in case of edit', () => {
    component.modal = {content: {number: '1111111111', type: 2, status: 2}, empId: 1} as unknown as EmployeeContactsModel;
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_ADDRESS.MOD_ADD_EDIT_ADDRESS.PAGE_TITLE_ADD');
  });

  it('should set add page title in case of add', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_ADDRESS.MOD_ADD_EDIT_ADDRESS.PAGE_TITLE_ADD');
    flush();
  }));

  it('should be empty form on initial', () => {
    const addressFormGroup = component.addressForm;
    const addressFormValues = {
      country: null,
      state: null,
      city: null,
      street: null,
      zip_code: null,
      type: 1,
    };
    expect(addressFormGroup.value).toEqual(addressFormValues);
  });

  it( 'should submit form on click save btn ', () => {
      spyOn(component, 'onSubmitForm');
      const submitButton: DebugElement =
        fixture.debugElement.query(By.css('button[type=submit]'));
      submitButton.triggerEventHandler('click', null);
      fixture.detectChanges();
      expect(component.onSubmitForm).toHaveBeenCalledTimes(1);
    });

  it('should disable save btn on form error ', () => {
    expect(fixture.debugElement.query(By.css('.save')).nativeElement.disabled).toBe(true);
  });

  it('should post data on server on save', fakeAsync(() => {
    const dummyData = {
      id : 1,
      country: 'Nepal',
      state: 3,
      city: 'ktm',
      street: 'bijeshwori',
      zip_code: 44600,
      type: 2,
    } as unknown as AddressModel;
    service.saveAddress(mockData.id, dummyData).subscribe(response => {
      expect(response).not.toBeNull();
    });
  }));

  it('should close dialog box on cancel', () => {
    const cancelBtn: DebugElement =
      fixture.debugElement.query(By.css('.cancel-btn'));
    cancelBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(cancelBtn.nativeElement.getAttribute('mat-dialog-close')).toBe('');
  });

  it('should not post data on error', () => {
    service.saveAddress(mockData.id, {} as AddressModel).subscribe(response => {
      expect(response.error).toEqual({message: 'Test', status: '417'});
    });
  });
});
