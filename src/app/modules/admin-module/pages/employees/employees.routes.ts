import {Routes} from '@angular/router';
import {EmployeesComponent} from './employees.component';
import {ViewDetailEmployeeComponent} from './components/view-detail-employee/view-detail-employee.component';
import {BasicInfoEmployeeComponent} from './components/view-detail-employee/components/basic-info-employee/basic-info-employee.component';
import {SocialSecurityComponent} from './components/view-detail-employee/components/social-security/social-security.component';
import {EmployeeAddressComponent} from './components/view-detail-employee/components/employee-address/employee-address.component';
import {ContactEmployeeComponent} from './components/view-detail-employee/components/contact-employee/contact-employee.component';
// tslint:disable-next-line:max-line-length
import {BankDetailEmployeeComponent} from './components/view-detail-employee/components/bank-detail-employee/bank-detail-employee.component';
import {DocumentsEmployeeComponent} from './components/view-detail-employee/components/documents-employee/documents-employee.component';
import {AllocatedLeaveEmployeeComponent} from './components/view-detail-employee/components/allocated-leave-employee/allocated-leave-employee.component';
import {HistoryEmployeeComponent} from './components/view-detail-employee/components/history-employee/history-employee.component';
import {EmployeesResolver} from '@common/resolvers/employees.resolver';

/**
 * App Routes
 */
export const EMPLOYEES_ROUTES: Routes = [
  {path: '', component: EmployeesComponent},
  {
    path: ':id',
    resolve: {
      employeeResolver: EmployeesResolver,
    },
    component: ViewDetailEmployeeComponent, data: {breadcrumb: {alias: 'employeeName'}},
    children: [
      {path: 'basic-info', component: BasicInfoEmployeeComponent, data: {breadcrumb: {skip: true}}},
      {path: 'social-security', component: SocialSecurityComponent, data: {breadcrumb: {skip: true}}},
      {path: 'address', component: EmployeeAddressComponent, data: {breadcrumb: {skip: true}}},
      {path: 'contact', component: ContactEmployeeComponent, data: {breadcrumb: {skip: true}}},
      {path: 'bank-details', component: BankDetailEmployeeComponent, data: {breadcrumb: {skip: true}}},
      {path: 'documents', component: DocumentsEmployeeComponent, data: {breadcrumb: {skip: true}}},
      {path: 'allocated-leaves', component: AllocatedLeaveEmployeeComponent, data: {breadcrumb: {skip: true}}},
      {path: 'histories', component: HistoryEmployeeComponent, data: {breadcrumb: {skip: true}}},
      {path: '', redirectTo: 'basic-info', pathMatch: 'full'}
    ]
  },
];
