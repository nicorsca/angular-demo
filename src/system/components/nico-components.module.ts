import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NicoAcronymComponent} from './nico-acronym/nico-acronym.component';
import {NicoActionFlyMenuComponent} from './action-fly-menu/nico-action-fly-menu.component';
import {NicoAdvancedSelectComponent} from './advanced-select/nico-advanced-select.component';
import {NicoDateComponent} from './nico-date/nico-date.component';


@NgModule({
  declarations: [
    NicoAcronymComponent,
    NicoActionFlyMenuComponent,
    NicoAdvancedSelectComponent,
    NicoDateComponent
  ],
  exports: [
    NicoAcronymComponent,
    NicoActionFlyMenuComponent,
    NicoAdvancedSelectComponent,
    NicoDateComponent
  ],
  imports: [
    CommonModule,
  ]
})
export class NicoComponentsModule { }
