import {TestBed} from '@angular/core/testing';
import {NicoActionFlyMenuComponent} from './nico-action-fly-menu.component';

describe('NicoActionFlyMenuComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        NicoActionFlyMenuComponent
      ],
    }).compileComponents();
  });

  it('should create the Fly menu component', () => {
    const fixture = TestBed.createComponent(NicoActionFlyMenuComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });


  /*it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('IS-Web app is running!');
  });*/
});
