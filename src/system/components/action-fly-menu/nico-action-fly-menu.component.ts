import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {NicoUtils} from '../../utilities';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'nico-action-fly-menu',
  template: `
    <div class="nico-action-fly-menu dropdown-container {{options.dropdownContainerClassName}}" [class.open]="isOpen">
      <div (click)="openFlyMenu($event)" *ngIf="options?.showCustomContent">
        <ng-content></ng-content>
      </div>
      <div class="dropdown-menu dropdown-icon" (click)="openFlyMenu($event)" *ngIf="options.showIconOnly">
        <svg class="svg-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd"
                d="M12 8C10.897 8 10 7.103 10 6C10 4.897 10.897 4 12 4C13.103 4 14 4.897 14 6C14 7.103 13.103 8 12 8ZM12 14C10.897 14 10 13.103 10 12C10 10.897 10.897 10 12 10C13.103 10 14 10.897 14 12C14 13.103 13.103 14 12 14ZM10 18C10 19.103 10.897 20 12 20C13.103 20 14 19.103 14 18C14 16.897 13.103 16 12 16C10.897 16 10 16.897 10 18Z"/>
        </svg>
      </div>
      <div class="dropdown-menu dropdown-label" (click)="openFlyMenu($event)" *ngIf="options.dropdownLabel">
        <span>{{options.dropdownLabel}}</span>
        <span class="chevron-down">
          <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
            <path fill-rule="evenodd"
                  d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
          </svg>
        </span>
      </div>
      <ul *ngIf="isOpen" id="actionFlyDropdown" class="dropdown-option-container {{options.dropdownPlacementClassName}}"
          role="menu" [@nicoFlyMenuAnimation]="isOpen">
        <ng-container *ngFor="let item of commands">
          <ng-container *ngIf="item.active">
            <li class="dropdown-option" role="menuitem">
              <a class="dropdown-item"
                 (click)="callItemClickHandler($event, item);">
                <ng-container *ngIf="item.faIcon">
                  <div class="icon-container">
                    <i [class]="item.faIcon"></i>
                  </div>
                </ng-container>
                <span class="option-label">{{item.label}}</span>
              </a>
            </li>
          </ng-container>
        </ng-container>
      </ul>
    </div>`,
  animations: [
    trigger('nicoFlyMenuAnimation', [
      state('void', style({opacity: 0, transform: 'scale(0.2)'})),
      state('*', style({opacity: 1, transform: 'scale(1)'})),
      transition('* <=> void', animate('0.1s cubic-bezier(.32, 0,.72, 1)'))
    ])
  ],
  styleUrls: ['nico-action-fly-menu.scss']
})
export class NicoActionFlyMenuComponent implements OnInit, OnChanges, OnDestroy {

  @Input() addOns: Array<FlyMenuItemInterface>;

  @Input() addOnPrepend = false;

  @Output() action: EventEmitter<FlyMenuActionEnum | any>;

  @Input() options: ActionFlyMenuOptionsInterface = {};

  public commands: Array<FlyMenuItemInterface> = [];

  public isOpen: boolean;

  @ViewChild('dropdown') dropDownElement: ElementRef;

  private documentClickListener: any;

  /**
   * Constructor
   */
  public constructor(protected elemRef: ElementRef) {
    this.action = new EventEmitter();
    this.addOns = [];
  }

  /**
   * Get default actions
   */
  protected getDefaultsActionItems(): Array<FlyMenuItemInterface> {
    return [
      {
        label: 'Edit',
        faIcon: '',
        name: FlyMenuActionEnum.Edit,
        active: !this.options.disableEditItem,
      },
      {
        label: 'Remove',
        faIcon: '',
        name: FlyMenuActionEnum.Remove,
        active: !this.options.disableRemoveItem,
      },

    ];
  }

  /**
   * On init life cycle hook
   */
  public ngOnInit(): void {
    if (NicoUtils.isNullOrUndefined(this.options)) {
      this.options = {};
    }
    if (!NicoUtils.isNullOrUndefined(this.addOns) && this.addOns.length > 0) {
      if (this.addOnPrepend === true) {
        this.commands = [].concat(this.addOns, this.commands);
      } else {
        this.commands = [].concat(this.commands, this.addOns);
      }
    }
    this.options.dropdownPlacementClassName = NicoUtils.isNullOrUndefined(this.options.dropdownPlacementClassName) ? 'topRight' : this.options.dropdownPlacementClassName;
  }

  private registerDocumentClick(): void {
    this.documentClickListener = (event: any) => {
      if (event.target && !this.elemRef.nativeElement.contains(event.target)) {
        this.closeFlyMenu();
      }
      this.stopDefault(event);
    };
    document.addEventListener('click', this.documentClickListener);
  }

  public ngOnDestroy(): void {
    document.removeEventListener('click', this.documentClickListener);
  }

  /**
   * On changes event
   * @param change
   */
  public ngOnChanges(change): void {

  }

  /**
   * private action command
   * @param action
   * @private
   */
  private _action(action: FlyMenuActionEnum | any): void {
    if (action === FlyMenuActionEnum.Custom) {
      this.action.emit(name);
    } else {
      this.action.emit(action);
    }
  }

  /**
   * Call item event handler
   * @param event
   * @param item
   */
  public callItemClickHandler(event: MouseEvent, item: FlyMenuItemInterface): void {
    this.stopDefault(event);
    this.closeFlyMenu();
    this._action(item.name);
  }

  public openFlyMenu(event: MouseEvent): void {
    event.preventDefault();
    this.isOpen = !this.isOpen;
    if (this.isOpen) {
      this.registerDocumentClick();
    }else{
      document.removeEventListener('click', this.documentClickListener);
    }
  }

  public closeFlyMenu(): void {
    document.removeEventListener('click', this.documentClickListener);
    this.isOpen = false;
  }

  public stopDefault(event: MouseEvent): void {
    // added prevent default to stop link from opening on resource card tap
    event.preventDefault();
    event.stopPropagation();
  }

}

export enum FlyMenuActionEnum {
  Status, Edit, Remove, Custom
}

export interface FlyMenuItemInterface {
  label: string;
  faIcon?: string;
  svgIcon?: any;
  name: FlyMenuActionEnum | any;
  active?: boolean;
}

export interface ActionFlyMenuOptionsInterface {
  showIconOnly?: boolean;
  dropdownLabel?: string;
  dropdownContainerClassName?: string;
  dropdownPlacementClassName?: 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight';
  disableEditItem?: boolean;
  disableRemoveItem?: boolean;
  disableStatusItem?: boolean;
  showCustomContent?: boolean;
}
