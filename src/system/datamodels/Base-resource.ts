import {BaseResourceInterface} from './base-resource.interface';
import {NicoCollection} from '../utilities';
import {JsonObject} from '@angular/compiler-cli/ngcc/src/packages/entry_point';

export class BaseResource implements BaseResourceInterface {

  public htmlId?: string;
  /**
   * The primary key identifier
   *  string
   */
  public primaryKey: string;
  /**
   * The creatable Attributes
   *  Array<string>
   */
  public creatableAttributes: Array<string>;
  /**
   * title of resource
   */
  public title?: string;
  /**
   * Description of resource
   */
  public description?: string;


  // accept any pair of key value for dynamic property
  [key: string]: any;

  public create(obj: any, isDynamic?: boolean): BaseResourceInterface {
    if (!obj) {
      return null;
    }
    const resource: BaseResourceInterface = Object.create(this);

    if (isDynamic){
      Object.keys(obj).forEach((key: string, index: number) => {
        resource[key] = obj[key];
      });
    }
    else{
      this.creatableAttributes.forEach((v: string, index: number) => {
        resource[v] = obj[v];
      });
    }
    return resource;
  }

  public createFromCollection(items: NicoCollection<any>): NicoCollection<BaseResourceInterface> {
    const ret: NicoCollection<BaseResourceInterface> = new NicoCollection<BaseResourceInterface>();
    const inst = Object.create(this);
    // if (NicoUtils.isNullOrUndefined(items.length)) {
    //   ret.push(inst.create(items));
    // } else {
    items.forEach((value: any, index: number) => {
      ret.push(inst.create(value));
    });
    // }
    return ret;
  }

  public json(): JsonObject {
    const ret = {};
    this.creatableAttributes.forEach((key: string) => {
      ret[key] = this[key];
    });
    return ret;
  }

  public jsonString(): string {
    return JSON.stringify(this.json());
  }


}
