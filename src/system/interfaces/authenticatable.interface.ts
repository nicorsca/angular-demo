export interface AuthenticatableInterface {
  getEmail(): string;
  getId(): string;
}
