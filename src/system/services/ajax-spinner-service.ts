import {NicoUtils} from '../utilities';

export enum AjaxSpinnerEnum {
  CircularSpinner = '<svg class="spinner circularSpinner" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80 80" xml:space="preserve">\n' +
    '    <path fill="currentColor" d="M40,72C22.4,72,8,57.6,8,40C8,22.4, 22.4,8,40,8c17.6,0,32,14.4,32,32c0,1.1-0.9,2-2,2 s-2-0.9-2-2c0-15.4-12.6-28-28-28S12,24.6,12,40s12.6, 28,28,28c1.1,0,2,0.9,2,2S41.1,72,40,72z">\n' +
    '         <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 40 40" to="360 40 40"  dur="1s" repeatCount="indefinite"></animateTransform>\n' +
    '    </path>\n' +
    '</svg>',
  CircularLineSpinner = '<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="spinner circularLineSpinner">\n' +
    '<g transform="rotate(0 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.916s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(30 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.833s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(60 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.75s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(90 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.666s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(120 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.583s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(150 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(180 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.416s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(210 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.333s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(240 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.25s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(270 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.166s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(300 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.083s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(330 50 50)">\n' +
    '  <rect x="47" y="18" rx="3" ry="3" width="6" height="16" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g></svg>',
  CircularRoundSpinner = '<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="spinner circularRoundSpinner">\n' +
    '<g transform="rotate(0 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.916s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(30 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.833s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(60 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.75s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(90 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.666s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(120 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.583s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(150 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(180 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.416s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(210 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.333s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(240 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.25s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(270 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.166s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(300 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.083s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g><g transform="rotate(330 50 50)">\n' +
    '  <rect x="40" y="20" rx="5" ry="5" width="10" height="10" fill="currentColor">\n' +
    '    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animate>\n' +
    '  </rect>\n' +
    '</g></svg>',
}


export enum AjaxSpinnerPositionEnum {
  Absolute = 0,
  Fixed = 1
}

export class AjaxSpinnerService {
  /**
   * Spinner Array
   *  Array
   */
  protected spinners: Array<Spinner> = [];

  /**
   * Show spinner
   * @return Spinner
   */
  public showSpinner(container?: string, type?: AjaxSpinnerEnum, position?: AjaxSpinnerPositionEnum, hasOverlayBackground?: boolean): Spinner {
    const spinner: Spinner = new Spinner(container, type, position, hasOverlayBackground);
    spinner.show();
    this.spinners.push(spinner);
    return spinner;
  }

  public hideAll(): void {
    // tslint:disable-next-line:forin
    for (const i in this.spinners) {
      this.spinners[i].hide();
    }
  }
}

export class Spinner {
  protected element: HTMLDivElement;
  // tslint:disable-next-line:variable-name
  protected _html: AjaxSpinnerEnum;
  protected spinnerPosition: AjaxSpinnerPositionEnum;
  protected hasOverlayBackground: boolean;
  protected parentContainer: HTMLElement = null;

  /**
   * Constructor
   * @param container - Container where the spinner loads
   * @param type - Spinner Type
   * @param position - CSS position (Absolute/Fixed) - Absolute by Default
   * @param hasOverlayBackground - Defines if the overlay should have background color - has dark background overlay set by Default
   */
  constructor(container?: string, type?: AjaxSpinnerEnum, position?: AjaxSpinnerPositionEnum, hasOverlayBackground?: boolean) {

    if (container) {
      this.parentContainer = document.getElementById(container);
    }

    if (!this.parentContainer) {
      this.parentContainer = document.body;
    }

    this.spinnerPosition = position;
    if (NicoUtils.isNullOrUndefined(this.spinnerPosition)) {
      this.spinnerPosition = AjaxSpinnerPositionEnum.Absolute;
    }

    hasOverlayBackground = NicoUtils.isNullOrUndefined(hasOverlayBackground) ? true : hasOverlayBackground;
    let className: string = this.parentContainer.className || '';
    className = (className.replace(`cssload-parent ${hasOverlayBackground ? '' : 'without-background'}`, '')).trim();
    this.parentContainer.className = className + ` cssload-parent ${hasOverlayBackground ? '' : 'without-background'}`;

    this._html = type;
    if (NicoUtils.isNullOrUndefined(this._html)) {
      this._html = AjaxSpinnerEnum.CircularSpinner;
    }
  }

  private createSpinner(): void {
    this.element = document.createElement('div');
    if (this.spinnerPosition === AjaxSpinnerPositionEnum.Fixed) {
      this.element.className = 'cssload-overlay cssload-fixed';
    } else {
      this.element.className = 'cssload-overlay';
    }
    this.element.innerHTML = this._html;
    this.parentContainer.appendChild(this.element);
  }

  public show(): void {
    this.createSpinner();
  }

  public hide(): void {

    if (this.element) {
      if (this.element.remove) {
        this.element.remove();
      } else {
        this.parentContainer.removeChild(this.element);
      }
      let className: string = this.parentContainer.className || '';
      className = (className.replace('cssload-parent', '')).trim();
      this.parentContainer.className = className;
    }
    this.element = null;
  }
}
