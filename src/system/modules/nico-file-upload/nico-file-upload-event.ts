export class NicoFileUploadEvent {
  public type: 'fileSelected' | 'dragEnter' | 'dragOver' | 'ondragEnd' | 'error';
  public payload: FileList;

  public setType(type): void {
    Object.defineProperty(this, 'type', {
      value: type,
      writable: false
    });
  }

  public setPayload(files: FileList): void {
    Object.defineProperty(this, 'payload', {
      value: files,
      writable: false
    });
  }

  public setErrorDescription(value: string): void {
    Object.defineProperty(this, 'errorDescription', {
      value,
      writable: false
    });
  }

  public getBase64OfFile(file: File): Promise<any>{
    return new Promise<any>((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = () => {
        let str = reader.result as string;
        str = str.split(',')[1];
        resolve(str);
      } ;
      reader.onerror = error => reject(error);
      reader.readAsDataURL(file);
    });
  }

}
