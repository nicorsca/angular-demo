import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {NicoFileUploadEvent} from '../nico-file-upload-event';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'nico-file-upload',
  templateUrl: 'nico-file-upload.component.html',
  styleUrls: ['nico-file-upload.component.scss']
})
export class NicoFileUploadComponent implements OnInit, OnChanges {
  @Input() multiple = false;
  @Input() showPreview = true;
  @Input() fileType: 'image' | 'pdf' | 'docx' | any;
  @Input() enableDrop = true;
  @Input() imageSource: string;
  @Input() hint = 'Click or drop your file here';
  @Input() fileExistsIcon = '';
  @Output() upload: EventEmitter<NicoFileUploadEvent>;

  public imagePreviewSource: SafeUrl;

  public hovering = false;

  /**
   * Addition
   */
  public fileName: string;

  @ViewChild('fileInputElement') fileInputElement: ElementRef;

  public constructor(protected sanitizer: DomSanitizer) {
    this.upload = new EventEmitter();
  }

  public ngOnInit(): void {
    window.addEventListener('dragover', e => {
      e.preventDefault();
    }, false);
    window.addEventListener('drop', e => {
      e.preventDefault();
    }, false);
    if (this.imageSource) {
      this.imagePreviewSource = this.sanitizer.bypassSecurityTrustUrl(this.imageSource);
    }
  }

  public ngOnChanges(): void {
    if (this.imageSource) {
      this.fileExistsIcon = '';
      this.imagePreviewSource = this.sanitizer.bypassSecurityTrustUrl(this.imageSource);
    }
  }

  // public setPreview(file: File): void {
  //   if (file && file.type.indexOf('image/') > -1) {
  //     this.imagePreviewSource = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(file));
  //   }
  // }

  /**
   * changed function
   */
  public setPreview(file?: File): void {
    this.showPreview = file && file.type.indexOf('image/') > -1;
    this.fileExistsIcon = '';
    this.imagePreviewSource = file ? this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(file))) :
      this.sanitizer.bypassSecurityTrustUrl((this.imageSource));
    this.fileName = file.name;
  }

  protected fileIsValid(file: File): boolean {
    if (this.fileType === 'image') {
      return file.type.indexOf('image/') > -1;
    }
    return true;
  }

  protected getInvalidImageUploadEvent(): NicoFileUploadEvent {
    const upload = new NicoFileUploadEvent();
    upload.setType('error');
    upload.setErrorDescription('err_invalid_image');
    upload.setPayload(null);
    return upload;
  }

  protected getFileDropEvent(file: FileList): NicoFileUploadEvent {
    const upload = new NicoFileUploadEvent();
    upload.setType('fileSelected');
    upload.setErrorDescription(null);
    upload.setPayload(file);
    return upload;
  }

  protected processFileSelect(event: Event, source: 'DragEvent' | 'Element'): void {
    event.preventDefault();
    event.stopPropagation();
    let files: FileList;
    if (source === 'DragEvent') {
      files = (event as DragEvent).dataTransfer.files;
    } else {
      files = (this.fileInputElement.nativeElement as HTMLInputElement).files;
    }
    let upload: NicoFileUploadEvent;
    if (this.fileIsValid(files.item(0))) {
      upload = this.getFileDropEvent(files);
      this.setPreview(files.item(0));
    } else {
      upload = this.getInvalidImageUploadEvent();
    }
    this.upload.emit(upload);
  }

  public onFileDrop(event: DragEvent): void {
    this.hovering = false;
    this.processFileSelect(event, 'DragEvent');
  }

  public onFileInputChange(event: Event): void {
    this.processFileSelect(event, 'Element');
  }

  public onDragOver(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.hovering = true;
  }

  public onDragLeave(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.hovering = false;
  }

}
