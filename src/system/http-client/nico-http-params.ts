import {HttpParams} from '@angular/common/http';

export class NicoHttpParams {

  private values: any = {};

  public set(key: string, value: string): void {
    this.values[key] = value;
  }

  public get(key: string): string {
    return this.values[key];
  }

  public getValues(): any {
    return this.values;
  }

  public toAngularHttpParams(): HttpParams {
    let params = new HttpParams();
    // tslint:disable-next-line:forin
    for (const key in this.values) {
      params = params.set(key, this.values[key]);
    }
    return params;
  }

}
