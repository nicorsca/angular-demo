import {NicoCollection} from '@system/utilities/nico-collection';

export class PaginatedNicoCollection<T> extends NicoCollection<T> {
  public currentPage: number;
  public total: number;
  public nextPageUrl: string;
  public prevPageUrl: string;
  public perPage: number;
}
