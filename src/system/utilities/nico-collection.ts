import {NicoUtils} from './nico-utils';

export class NicoCollection<T> {

  protected items: Array<T>;

  protected listChangeListeners: any = {};

  constructor(items?: Array<T>) {
    this.items = items;
    if (items === undefined || items === null) {
      this.items = [];
    }
  }

  public setItems(items?: Array<T>): void {
    this.items = items;
    this.callListChangeListener();
  }

  /**
   * returns length of array
   */
  get length(): number {
    return this.items.length;
  }

  get ___is_nico_collection(): boolean {
    return true;
  }


  /**
   * Get new instance of the class.
   */
  public newInstance(): NicoCollection<T> {
    return new NicoCollection<T>();
  }

  /**
   * Set change listener
   */
  public addChangeListener(fn: () => {}, name?: string): void {
    if (!name) {
      name = NicoUtils.getRandomString(10);
    }
    this.listChangeListeners[name] = fn;
  }

  /**
   * Get item at given index
   * @return T
   */
  public getItem(index: number): T {
    return this.items[index];
  }

  /**
   * Remove item from given index
   */
  public removeFromIndex(index: number): void {
    this.items.splice(index, 1);
  }

  /**
   * Map the collection to new collection
   */
  public map(callbackFn: (value: T, index: number) => any): NicoCollection<any> {
    const ret = new NicoCollection<any>();
    this.items.forEach((item: T, index: number) => {
      ret.push(callbackFn(item, index));
    });
    return ret;
  }

  /**
   * Get index of the given item
   */
  public getIndex(item: T, compareProperty?: string): number {
    if (compareProperty) {
      for (let i = 0; i < this.items.length; i++) {
        if (this.items[i][compareProperty] === item[compareProperty]) {
          return i;
        }
      }
      return -1;
    } else {
      return this.items.indexOf(item);
    }
  }

  /**
   * Call list change listener
   */
  protected callListChangeListener(): void {
    for (const name in this.listChangeListeners) {
      if (this.listChangeListeners.hasOwnProperty(name)) {
        this.listChangeListeners[name]();
      }
    }
  }

  /**
   * add items to array
   */
  public add(item: T, index?: number): void {
    if (isNaN(index) || index < 0 || index > this.items.length) {
      index = this.items.length;
    }
    this.items.splice(index, 0, item);
    this.callListChangeListener();
  }

  /**
   * remove items form array
   */
  public remove(item: T, compareKey?: string): void {
    const oldCount = this.items.length;
    this.items.forEach((value, index) => {
      if (compareKey) {
        if (item[compareKey] === value[compareKey]) {
          this.items.splice(index, 1);
        }
      } else if (value === item) {
        this.items.splice(index, 1);
      }


    });
    if (oldCount !== this.items.length) {
      this.callListChangeListener();
    }
  }

  /**
   * merge to array
   */
  public merge(items?: Array<T>): NicoCollection<T> {
    this.items = this.items.concat(items);
    this.callListChangeListener();
    return this;
  }

  // tslint:disable-next-line:typedef
  [Symbol.iterator]() {
    return this.items.values();
  }

  /**
   * Empty
   */
  public empty(): NicoCollection<T> {
    this.items.length = 0;
    this.callListChangeListener();
    return this;
  }

  public all(): Array<T> {
    return this.items;
  }

  public count(): number {
    return this.items.length;
  }

  /**
   * Iterate through the items
   */
  public forEach(callbackFn: (value: T, index: number, array: T[]) => void): void {
    this.items.forEach(callbackFn);
  }

  /**
   * Push item to the collection
   */
  public push(item: T): void {
    this.items.push(item);
    this.callListChangeListener();
  }

  /**
   * Pop item
   */
  public pop(): T {
    const ret = this.items.pop();
    this.callListChangeListener();
    return ret;
  }

  /**
   * Get first item for the callback satisfied true value
   */
  public first(callbackFn: (obj: T) => boolean): T {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.items.length; i++) {
      if (callbackFn(this.items[i]) === true) {
        return this.items[i];
      }
    }
    return null;
  }

  public where(callbackFn: (obj: T) => boolean): NicoCollection<T> {
    const ret = this.newInstance();
    this.items.forEach(element => {
      if (callbackFn(element) === true) {
        ret.push(element);
      }
    });
    return ret;
  }

  /**
   * Compare given value with given operator
   * @returns boolean
   */
  private compareValue(value: any, operator: '=' | '<' | '>' | '<=' | '>=' | '!=', compareValue: any): boolean {
    switch (operator) {
      case '=':
        return value === compareValue;
      case '<=':
        return value <= compareValue;
      case '>=':
        return value >= compareValue;
      case '<':
        return value < compareValue;
      case '>':
        return value > compareValue;
      case '!=':
        return value !== compareValue;
      default:
        throw Error('Invalid comparison operator');
    }
  }

  /**
   * Find or search items in collection
   * @returns any
   */
  private findOrSearch(property: number | string, operator: '=' | '<' | '>' | '<=' | '>=' | '!=', compareValue: string | number, find: boolean): any {
    let item: any = null;
    if (find === false) {
      item = this.newInstance();
    }
    let truth = false;
    // @ts-ignore
    this.forEach((v: any) => {
      if (NicoUtils.isNullOrUndefined(property)) {
        truth = this.compareValue(v, operator, compareValue);
      } else {
        truth = this.compareValue(v[property], operator, compareValue);
      }
      if (truth === true) {
        if (find === true) {
          item = v;
          return true;
        } else {
          item.push(v);
        }
      }
    });
    return item;
  }

  /**
   * Sort the items
   */
  public sort(compareFn?: (a: T, b: T) => number): NicoCollection<T> {
    this.items = this.items.sort(compareFn);
    return this;
  }

  public tabularize(numCol: number): Array<Array<any>> {
    const result = [];
    let row = [];
    // tslint:disable-next-line:prefer-for-of
    for (let j = 0; j < this.items.length; j++) {
      row.push(this.items[j]);
      if (row.length === numCol) {
        result.push(row);
        row = [];
      }
    }
    return result;
  }

  public toJsonString(): string {
    return JSON.stringify(this.items);
  }

}
